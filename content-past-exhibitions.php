<?php
/**
 * @package gcmf
 */
?>
<?php 		
//Set Exhibition info variables

	//Start Date
	$start_timestamp = (!((get_post_meta($post->ID, 'wpcf-start-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-start-date',TRUE) : '' ;
	$start_date = ($start_timestamp == '') ? '' : date("F j, Y", $start_timestamp);
	$short_start_date = ($start_timestamp == '') ? '' : date("M j, Y", $start_timestamp);
	
	//End Date
	$end_timestamp = (!((get_post_meta($post->ID, 'wpcf-end-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-end-date',TRUE) : '' ;
	$end_date = ($end_timestamp == '') ? '' : ' &#8211; ' . date("F j, Y", $end_timestamp);
	$short_end_date = ($end_timestamp == '') ? '' : date("M j, Y", $end_timestamp);
	
?>
		<div class="col-sm-6 exhibition-gridview">
			<div class="multi-box-wrapper mb-has-heading">
				<article id="post-<?php the_ID(); ?>" <?php post_class('multi-box'); ?>>
					<div class="mb-image">			
						<?php if ( has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
							<?php if(!(get_post_meta($post->ID, 'wpcf-featured-slider-lg-img', TRUE) == '')) : ?>
							<img src="<?php echo get_post_meta($post->ID, 'wpcf-featured-slider-lg-img', TRUE) ?>" class="img-responsive" />
							<?php else: ?>
							<?php the_post_thumbnail('half-slider', array('class' => 'img-responsive')); ?>
							<?php endif; ?>
							<span class="countdown-overlay"></span>
							</a>
						<?php endif; ?>
					</div>
					<div class="mb-content-wrapper-outer">
						<div class="mb-content-wrapper-inner is-table">
							<div class="table-cell">
								<div class="mb-content">
									<p class="details"><?php echo $location ?></p>
									<p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>
								</div>
							</div>
						</div><!-- .mb-content-wrapper-inner -->
						<div class="container mb-details">
							<div class="row">
								<div class="col-sm-8">
								<?php $exhibition_date = $short_start_date . ' &#8211; ' .  $short_end_date;?>
									<p class="date details"><?php echo $exhibition_date; ?></p>
								</div>
								<div class="col-sm-4">
									<div class="no-gutter">								
										<a class="btn" href="<?php the_permalink(); ?>">View Details</a>
									</div>
								</div>
							</div>
						</div>
					</div><!-- .mb-content-wrapper-outer -->
				</article>
			</div>
			<?php
			if(!(get_post_meta($post->ID, 'wpcf-pressroom-slug', TRUE) == '') && !(get_post_meta($post->ID, 'wpcf-educational-resources-url', TRUE) == '')) {
				$link_divider = '<span class="jumplink-divider"></span>';
				$jl_class = ' multi';
			} else {
				$link_divider = '';
				$jl_class = '';
			}
			?>
			<div class="mb-jumplink-wrapper clearfix<?php echo $jl_class; ?>">
			
			<?php if(!(get_post_meta($post->ID, 'wpcf-pressroom-slug', TRUE) == '')) : ?>
				<a class="jumplink" href="<?php echo get_post_meta($post->ID, 'wpcf-pressroom-slug', TRUE); ?>">Pressroom Archives</a>
			<?php endif; ?>
			<?php echo $link_divider; ?>
			<?php if(!(get_post_meta($post->ID, 'wpcf-educational-resources-url', TRUE) == '')) : ?>
				<a class="jumplink" href="<?php echo get_post_meta($post->ID, 'wpcf-educational-resources-url', TRUE); ?>">Educational Resources</a>
			<?php endif; ?>
			</div>
			
		</div>
	<div class="col-md-12"> 
		<article id="post-<?php the_ID(); ?>" <?php post_class('exhibition-listview'); ?>>
		
			<div class="inner-content  clearfix">
				<div class="row">	
					<div class="col-md-2">
						<p class="past-exhibition-dates"><strong>Start</strong><br />
							<?php echo $short_start_date; ?>
						</p>
						<p class="past-exhibition-dates"><strong>End</strong><br />
							<?php echo $short_end_date; ?>
						</p>
					</div>
					<div class="col-md-3">
						<?php if ( has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
							<?php if(!(get_post_meta($post->ID, 'wpcf-featured-slider-lg-img', TRUE) == '')) : ?>
							<img src="<?php echo get_post_meta($post->ID, 'wpcf-featured-slider-lg-img', TRUE) ?>" class="img-responsive" />
							<?php else: ?>
							<?php the_post_thumbnail('half-slider', array('class' => 'img-responsive')); ?>
							<?php endif; ?>
							</a>
						<?php endif; ?>

					</div>
					<div class="col-md-4 past-exhibition-title">
						<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<?php echo types_render_field("wpcf-gallery-location", array("output" => "html"));?>
					</div>
					<div class="col-md-3 past-exhibition-details">
						<a href="<?php the_permalink(); ?>" class="btn">Exhibition Details</a>
						<div class="past-exhibition-jumplinks">
						<?php if(!(get_post_meta($post->ID, 'wpcf-pressroom-slug', TRUE) == '')) : ?>
							<a href="/pressroom/category/<?php echo get_post_meta($post->ID, 'wpcf-pressroom-slug', TRUE); ?>/" class="jumplink">Pressroom Archives</a><br />
						<?php endif; ?>
						<?php if(!(get_post_meta($post->ID, 'wpcf-educational-resources-url', TRUE) == '')) : ?>
							<a href="<?php echo get_post_meta($post->ID, 'wpcf-educational-resources-url', TRUE); ?>" class="jumplink">Educational Resources</a>
						<?php endif; ?>
						</div>
					</div>
				</div> <!-- .row -->
			</div>

		</article><!-- #post-## -->
	</div>
