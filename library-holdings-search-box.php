<style>
	.facetwp-facet-keyword_search:before
	{
		display:none;
	}
</style>

<script>
function fwp_redirect() {
    FWP.parse_facets();
    FWP.set_hash();
    var query = FWP.build_query_string();
    window.location.href = '/library/results/?' + query;
}
</script>


<div class='holdings-search'>
	<h3>Holdings Search</h3>
	<label>Collection:</label>
	<?php echo facetwp_display('facet', 'collection_dropdown'); ?>
	<label>Format:</label>
	<?php echo facetwp_display('facet', 'format'); ?>
	<label>Keywords:</label>
	<?php echo facetwp_display('facet', 'keyword_search'); ?>
	<button onclick="fwp_redirect()" class="btn-search">Start Search</button>
</div>

<div style="display:none;">
	<?php echo facetwp_display('template', 'default'); ?>
</div>

