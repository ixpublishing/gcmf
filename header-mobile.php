<?php
/**
 * The Header for the mobile home page of our theme.
 *
 * @package gcmf
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head();

switch_to_blog(1);
	if( of_get_option('ga_tracking') ) :
		echo of_get_option('ga_tracking');
	endif;
restore_current_blog();

?>
</head>
<body <?php body_class('home-mobile'); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	
	<?php switch_to_blog(1); ?>
	<?php if( of_get_option('alert_toggle') ) : ?>
		<div id="alertbar-area">

			<div id="alertbar">
				<div class="container alert">
					<i class="fa fa-exclamation-circle pull-left"></i>
					<?php echo of_get_option('alert_text') ?>
				</div>
			</div>
			<!-- <a class="alertbar_toggle" href="#"></a> -->
		</div>
	<?php endif; ?>
	<?php restore_current_blog(); ?>
	
	<header id="masthead" class="site-header" role="banner">
		
		<div class="container">
		
			<div id="preheader" class="clearfix">
				
				<ul id="header-cta" class="list-float pull-right">
					<li><a href="/membership/" class="cta-link1">Members</a><li>
					<li><a href="/support/ways-to-give/gcmf-annual-fund/" class="cta-link2">Donate</a></li>
				</ul>
		
				<ul id="info-bullets" class="list-float pull-right">
					<li class="info-bullet-1"><span>Free Admission</span>|</li>
					<li class="info-bullet-2"><span>Open 365</span>|</li>
					<li class="info-bullet-3"><span><a href="https://reservations.gcmf.museum/state/login.aspx">Log In</a></span></li>
				</ul>
			
			<?php //echo do_shortcode('[ob-header-links height="35" width="400" scrolling="no" class="outbound-iframe"]'); ?>
			</div>
		
			<div class="row">
		
			<div class="col-md-3 col-sm-5 col-xs-6 clearfix">
					<?php switch_to_blog(1); ?>
					<div class="site-branding">
						<h3 id="logo" class="site-title logos-gcmf-logo-color pull-left hide-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h3>
						<?php //Mobile Reduced Logo ?>
						<h3 id="mobile-logo" class="site-title logos-gcmf-logo-mobile pull-left hide-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h3>
					</div>
					<?php restore_current_blog(); ?>
				
				</div><!-- .col -->
				
				<div class="col-md-9 col-sm-7 col-xs-6 clearfix">
				
					<nav id="site-navigation" class="main-navigation" role="navigation">
						
						<div class="menu-toggle">
							<a href="#"><i class="fa fa-bars"></i></a>
						</div>
						
						<div class="sr-only skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'gcmf' ); ?>"><?php _e( 'Skip to content', 'gcmf' ); ?></a></div>
						
						<?php switch_to_blog(1); ?>
						
						<?php wp_nav_menu( array('theme_location' => 'primary', 'container' => false , 'walker' => new UL_Class_Walker()  ) ); ?>
						
						<div id="site-search-trigger">
							<a><i class="fa fa-search"></i></a>
						</div>

					</nav><!-- #site-navigation --><!-- #site-navigation -->
					
					<div id="site-search-lg-screen">	
						<?php global_site_search_form(); ?>
					</div>
					
					<?php restore_current_blog(); ?>
				
				</div><!-- .col-sm-9 -->
				
				<div id="site-search-sm-screen" class="col-xs-12">
					<?php global_site_search_form(); ?>
				</div>
				
			</div><!-- .row -->
			
		</div><!-- header .container -->
		
		</header><!-- #masthead -->