<?php
/**
 * The template for displaying Collections
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gcmf
 */

get_header(); ?>

<?php 		

?>		


<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/js/gcmf.js"></script-->
<style>
	.searchtemplate .facetwp-template
	{
		overflow:visible;
	}
	.searchtemplate p.excerpt
	{
		margin:.6em 0;
	}

	.item-info
	{
		float:right;
	}
	.search-main-wrapper .facetwp-facet {
		margin-bottom: 0px;
	}
	.searchwp-highlight.on
	{
		background:#ffa;
	}

	.search-options-row .facetwp-selections ul
	{
		padding-left:0;
	}
	.search-options-row .facetwp-selections li {
		font-size:0.9em;
	}

	.title-date
	{
		margin-left:0;
	}

	.format-icon
	{
		padding-left:23px;
		background-size:20px 20px;
		background-repeat:no-repeat;
	}
	.center {
		float: center;
		text-align: center;
	}

	.item-thumbnail img
	{
		max-width:200px;
		width:auto;
	}

	.related-item-image 
	{
		text-align: center;
	}
	.related-item-image img
	{
		max-height: 150px;
		width:auto;
	}
	
	.result_text {
		font-weight: 300; 
	}

	ul#search-tabs {
		list-style: none outside none;
		padding-left: 0;
	}

	li.search-tiem a:hover {
 		border-top: #00667a solid 7px;

	}

	li.search-item {
		position: relative;
		display: inline-block;
		background: #d7d7d7;
		margin-right: 5px;
	}


	li.search-item a {
	  color: #333333;
	  padding: 7px 20px 7px 20px;
	  display: block;
	  -webkit-transition: all 0.2s;
	  transition: all 0.2s;
	}

	#search-tabs {
	    font-size: 16px;
	    position: relative;
	    z-index: 99;
	}

		.digital-dl-list {
		list-style-type: none;
		padding: 0;
	}

	.digital-dl-list li {
		margin-bottom: 10px;
	}


</style>


<?php
	$hilite = false;
	try {
		$hilite = new SearchWP_Term_Highlight();
	} catch (\Exception $e) {
		
	}
	
?>
<div class=''>
<div id="content" class="container site-content">
<div id="landing-page">
	
		<div id="primary" class="col-sm-12 content-area">
		<main id="main" class="row site-main" role="main">
			<div class='col-sm-12 collection-featured'>
				<?php
					if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
					yoast_breadcrumb('<p id="breadcrumbs-collection">','</p>');
					}
				?>
				<div class="row">	
					<div class="col-sm-6 col-md-9 col-lg-6">
						<hr class="top">
						<a href="/library/collections"><span class="collection-small">COLLECTIONS</span></a>
				<h1 class="collection-featured">
					<?php $term_title = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
					echo $term_title->name; ?></h1>

					<p><?php include (get_stylesheet_directory() . "/share_this_include.php"); ?></p>

					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_desc = get_field("collection_description", $term);	?>
					<div class="tag-desc"><?php echo $tax_desc; ?></div>
					

					<?php

						$args = array(
							"taxonomy" => "collection",
							"show_count" => 1,
							"title_li" => "",
							"child_of" => $term->term_id,
							"echo" => 0,
							'hide_empty' => 0
						);

						//echo $term->term_id;
						if(isset($_REQUEST["alpha"]))
							$alpha = $_REQUEST["alpha"];
						else
							$alpha = false;

						$args['walker'] = new Post_Category_Walker( $post_id, $args['taxonomy'], $alpha );
						?>

							<?php
							$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							if ($term->parent == 0) { 
								$args['depth'] = "1";
								$child_cats = wp_list_categories($args);
								//wp_list_categories('taxonomy=collection&depth=1&show_count=0&title_li=&child_of=' . $term->term_id); 
							} else {
								//$args['child_of'] = $term->parent;
								$child_cats = wp_list_categories($args);
								//wp_list_categories('taxonomy=collection&show_count=0&title_li=&child_of=' . $term->parent); 
							}
							if(!stristr($child_cats, "no categories"))
								echo $child_cats;
						?>
						 <?php 
						 //$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
						 //echo $term->name; 
						 ?>
<div class="item-container">
						<?php 
						// Don't show files from subcategories; only this one.
						global $wp_query;
						//$current_cat = intval(  $term->term_id );//get_query_var('cat') );
						// echo "Cat: $current_cat";
						// $args = array_merge( $wp_query->query_vars, 
						// 	array( 'include_children' => 0 ) 
						// );
						// print_r($args);
						$unwanted_children = get_term_children($term->term_id, $args['taxonomy']);
			  			$unwanted_post_ids = get_objects_in_term($unwanted_children, $args['taxonomy']);

			  			query_posts( array_merge( array('posts_per_page' => -1, 'orderby'=> 'title', 'order' => 'ASC', 'post__not_in' => $unwanted_post_ids), $wp_query->query));

						//$child_q = new WP_Query( $args );

						while ( have_posts() ) : the_post();
							//Category of current post
							

							//print_r(get_query_var('cat'));
							?>
				<div class='item'>
					<? /*php the_post_thumbnail('thumbnail'); */?>
					<a class ='col-file-list' href="<?php the_permalink(); ?>" target="blank" rel="bookmark"><?php the_title(); ?></a>
				</div>

			<?php endwhile; // end of the loop. ?>
</div>
			<br>
					</div>
					<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="collection-landing">
						<?php $tax_img = get_field("collection_image", $term);	?>
						<?php if($tax_img): ?><img class="collection-image " src='<?php echo $tax_img['url']; ?>' width='230px'>					
					<?php endif; ?>
						<div class="collection-list-lp col-sm-12">
						<?php /* 
							Use separate page for results.
						*/ ?>
						<style>
							div.facetwp-checkbox, div.facetwp-checkbox.checked
							{
								background: none;
								margin:0;
								padding:0;
							}
						</style>
						
						<script>
						(function($) {
						    $(document).ready(function() {
						        //FWP.auto_refresh = false;
						        window.location.hash = "!/collection=" + <?php echo $term->term_id; ?>;
						    });
						   
						    $(document).on('facetwp-refresh', function() {
						    	//console.log("hello");
						        if ( FWP.loaded ) {
						        	//console.log("hello2");
						        	var hash = '!/' + FWP.serialize(FWP.facets);
						        	hash += "&collection=" + <?php echo $term->term_id; ?>;
						            //FWP.set_hash();
						            window.location.href = '/library/results/#' + hash;
						        }
						    });
						})(jQuery);
						</script>
					<h3>Collection Formats:</h3>
						<?php 
						echo "<div style='display:none;'>" . do_shortcode('[facetwp template="default"]') . "</div>";
						echo do_shortcode( '[facetwp facet="format"]' ); ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
					$tax_number = get_field("collection_number", $term);
					$tax_date = get_field("collection_date", $term);
					$tax_extent = get_field("collection_extent", $term);	
					?>	
					<?php if ($tax_number != '') { ?>					
					<div class="tag-number"><span class="result-type">Collection Number:  </span><?php echo $tax_number; ?></div>
					<?php } else { }; ?>
					
					<?php if ($tax_date != '') { ?>	
					<div class="tag-date"><span class="result-type">Date:  </span><?php echo $tax_date; ?></div>
					<?php } else { }; ?>
				
					<?php if ($tax_extent != '') { ?>	
					<div class="tag-len"><span class="result-type">Collection Size:  </span><?php echo $tax_extent; ?></div>
					<?php } else { }; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list = get_field("container_list", $term);	?>
					<?php if ($tax_container_list): ?>
					 <?php echo "<br><span class='download-link'><a href='$tax_container_list[url]' target='_blank'>&nbsp;Collection Guide</a></span><br>" ?>
					<?php endif; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list2 = get_field("container_list2", $term);	?>
					<?php if ($tax_container_list2): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list2[url]' target='_blank'>Collection Guide 2</a></span><br>" ?>
					<?php endif; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list3 = get_field("container_list3", $term);	?>
					<?php if ($tax_container_list3): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list3[url]' target='_blank'>Collection Guide 3</a></span><br>" ?>
					<?php endif; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list4 = get_field("container_list4", $term);	?>
					<?php if ($tax_container_list4): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list4[url]'' target='_blank''>Collection Guide 4</a></span><br>" ?>
					<?php endif; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list5 = get_field("container_list5", $term);	?>
					<?php if ($tax_container_list5): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list5[url]' target='_blank'>Collection Guide 5</a></span><br>" ?>
					<?php endif; ?>
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<?php $tax_container_list6 = get_field("container_list6", $term);	?>
					<?php if ($tax_container_list6): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list6[url]' target='_blank'>Collection Guide 6</a></span><br>" ?>
					<?php endif; ?>
					<?php $tax_container_list7 = get_field("container_list7", $term);	?>
					<?php if ($tax_container_list7): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list7[url]' target='_blank'>Collection Guide 7</a></span><br><br>" ?>
					<?php endif; ?>
					<?php $tax_container_list8 = get_field("container_list8", $term);	?>
					<?php if ($tax_container_list8): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list8[url]' target='_blank'>Collection Guide 7</a></span><br><br>" ?>
					<?php endif; ?>
					<?php $tax_container_list9 = get_field("container_list9", $term);	?>
					<?php if ($tax_container_list9): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list9[url]' target='_blank'>Collection Guide 7</a></span><br><br>" ?>
					<?php endif; ?>
					<?php $tax_container_list10 = get_field("container_list10", $term);	?>
					<?php if ($tax_container_list10): ?>
					 <?php echo "<span class='download-link'><a href='$tax_container_list10[url]' target='_blank'>Collection Guide 7</a></span><br><br>" ?>
					<?php endif; ?>
					</div><!--end colleciton-landing-lp -->
				</div><!--end image class-->
			</div><!--end image/collection-->
					<div class="col-md-3 col-lg-3">
						<?php get_sidebar(); ?>
					</div>
				</div>
				<?php include (get_stylesheet_directory() . "/share_this_include.php"); ?>
			</div>
			

		</main><!-- #main -->
	</div><!-- #primary -->
	
</div><!-- #content -->
</div>
</div>
<?php get_footer(); ?>