<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
		<div class="col-sm-4">
			
			<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				 echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '">' . the_post_thumbnail('full', array('class'=>'img-responsive') ) . '</a>';
				} else {
					echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '"><img src="' . get_template_directory_uri() . '/img/placeholders/-placeholder.png" class="img-responsive" alt="" /></a>';
				}
					
			?>
		</div><!-- .col-sm-4 -->
		
		<div class="col-sm-8">
			
			<header class="entry-header">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<div class="event-details">		
			<?php
				 
					if (!((get_post_meta($post->ID, 'wpcf-with', TRUE))=='')) {		 
						echo '<p class="event-speaker">with ' . get_post_meta($post->ID,'wpcf-with',TRUE) . '</p>';		 
					}
					
					$event_dates=get_post_meta($post->ID,'wpcf-date-and-time');
					if(array_filter($event_dates)) {
					  //array contains values
						foreach ($event_dates as $event_date) {
							echo  '<p class="event-date">' . $event_date . '</p>';
						}
					}

					if (!((get_post_meta($post->ID, 'wpcf-location', TRUE))=='')) {		 
						echo '<p class="event-location">' . get_post_meta($post->ID,'wpcf-location',TRUE) . '</p>';		 
					}
			?>	  
				</div>
			<?php	
			if ( is_category() || is_archive() ) {
				the_excerpt();
			} else {
				the_content();
			} 
			?>
				
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
	
		</div><!-- .col-sm-8 -->
	</div>
		
</article><!-- #post-## -->