<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package gcmf
 */
?>
<?php if ( has_nav_menu( 'secondary' ) ) : ?>
</div><!-- /#content-nav-wrap -->
<?php endif; ?>
	<?php if ( dynamic_sidebar('footer-message') ) : ?> <?php else : endif; ?>

	<?php switch_to_blog(1); ?>

	<footer id="mega-footer">
		<div class="container">
			<div id="mf-container" class="row">
				<div class="col-md-12 col-lg-12">
					<ul class="mf-menus list-float">
						<li class="plan-footer-menu plan">
							<p class="mf-headings">George C. Marshall</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_a', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">Research Library</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_b', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">The Museum</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_c', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">Educational Programs</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_d', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">The Foundation</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_e', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">News & Events</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_f', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
						<li class="plan-footer-menu plan">
							<p class="mf-headings">Support</p>
							<?php wp_nav_menu( array('theme_location' => 'footer_menu_g', 'container' => false, 'menu_class' => 'list-unstyled' ) ); ?>
						</li>
					</ul>
				</div>
				<!--<div class="col-md-4 col-lg-5">
				<p class="museum-info mf-headings">Free. Open 365. <span>Today Until 5pm.</span></p>
					<ul class="mf-menus list-float">
						<li class="museum-info-col info-col-1">Info Col 1</li>
						<li class="museum-info-col info-col-2">Info Col 2</li>
					</ul>
				</div>!-->
			</div>
		</div>

	</footer>

	<footer id="subfooter">

		<div class="container">

			<div class="row">


				<div class="col-md-12 footer-address">

					VMI Parade | P.O. Box 1600 | Lexington, VA 24450 | P: 540.463.7103 | E: <a class="footer-address" href="mailto:marshallfoundation@marshallfoundation.org">marshallfoundation@marshallfoundation.org</a>


				</div>



			</div>

		</div><!-- footer .container -->

	</footer><!-- footer#subfooter -->



	<?php restore_current_blog(); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
