<?php
/**
 * Template Name: Full Width No Title
 *
 * @package gcmf
 */

get_header(); ?>

		<div id="content" class="site-content container">
		<?php

			if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
		?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-blank' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
					
</div><!-- #content -->

<?php get_footer(); ?>
