<?php
/**
 * The Template for displaying all single posts.
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
<!--
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>
-->

	<p id="breadcrumbs">
		<span prefix="v: http://rdf.data-vocabulary.org/#">
			<span><a href="/newsroom/calendar/" >Event Calendar</a></span> » <span ><span class="breadcrumb_last" ><?php the_title(); ?></span></span>
		</span>
	</p>

	<div class="row">
	
		<div class="col-md-12">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
		
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php
		
					get_template_part( 'content', 'single-events' );
					 
					?>
				
					<?php gcmf_content_nav( 'nav-content' ); ?>
		
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>
		
				<?php endwhile; // end of the loop. ?>
		
				</main><!-- #main -->
			</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		


	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?> 