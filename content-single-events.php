<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	//Save custom fields to an array
	$cf = get_post_custom(); ?>
	<div class="row">
		<div class="col-md-4">

			<?php
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				  the_post_thumbnail('full', array('class'=>'img-responsive') );
				  echo '<p class="event-img-caption">' . get_post(get_post_thumbnail_id())->post_excerpt . '</p>';
				} else {
					echo '<img src="' . get_template_directory_uri() . '/img/placeholders/-placeholder.png" class="img-responsive" alt="" />';
				}
			?>

				<?php if ( !empty( $cf['wpcf-get-tickets-url'][0] ) ) : ?>
					<a href="<?php echo $cf['wpcf-get-tickets-url'][0]; ?>" class="btn orange">Get Tickets</a>
				<?php endif; ?>

				<div class="event-details">
					<?php if ( !empty( $cf['wpcf-cost'][0] ) ) : ?>
						<p><span>Cost: </span><?php echo $cf['wpcf-cost'][0]; ?></p>
					<?php endif; ?>
					<?php if ( !empty( $cf['wpcf-ages'][0] ) ) : ?>
						<p><span>Ages: </span><?php echo $cf['wpcf-ages'][0]; ?></p>
					<?php endif;?>
					<?php if ( !empty( $cf['wpcf-open-to'][0] ) ) : ?>
						<p><span>Open to: </span><?php echo $cf['wpcf-open-to'][0]; ?></p>
					<?php endif;?>
				</div>

		</div><!-- .col-md-4 -->

		<div class="col-md-8">

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

				<div class="event-details">
				<?php if ( !empty( $cf['wpcf-with'][0] ) ) : ?>
					<p class="event-speaker">with <?php echo $cf['wpcf-with'][0]; ?></p>
				<?php endif; ?>

					<p class="event-date">
					<?php
						$startdate = $cf['wpcf-start-date'][0];
						$enddate = $cf['wpcf-end-date'][0];

						if ($startdate == $enddate) {
							$printdate = date_i18n('F j, Y', $startdate);
						}
						else if($enddate == null){
							$printdate = date_i18n('F j, Y', $startdate);
						}
						else{
							$printdate = date_i18n('F j', $startdate) . " - " . date_i18n('F j, Y', $enddate);
						}

						echo $printdate;
					?>


					</p>

					<?php if ( !empty( $cf['wpcf-event-location'][0] ) ) : ?>
						<p class="event-location"><?php echo $cf['wpcf-event-location'][0]; ?></p>
					<?php endif; ?>

				</div>

			<div class="entry-content">
				<?php the_content(); ?>

			<?php if ( !empty( $cf['wpcf-sponsored-by'][0] ) || !empty( $cf['wpcf-registration-details'][0] ) || !empty( $cf['wpcf-cancellation-details'][0] ) ) : ?>
				<div class="event-details">
					<?php if ( !empty( $cf['wpcf-sponsored-by'][0] ) ) : ?>
						<h4>Sponsored by</h4>
						<p><?php echo $cf['wpcf-sponsored-by'][0]; ?></p>
					<?php endif; ?>

					<?php if ( !empty( $cf['wpcf-registration-details'][0] ) ) : ?>
						<h4>Registration Details</h4>
						<p><?php echo $cf['wpcf-registration-details'][0]; ?></p>
					<?php endif; ?>

					<?php if ( !empty( $cf['wpcf-cancellation-details'][0] ) ) : ?>
						<h4>Cancellation Details</h4>
						<p><?php echo $cf['wpcf-cancellation-details'][0]; ?></p>
					<?php endif; ?>
				</div>
			<?php endif; ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
				<?php include (get_stylesheet_directory() . "/share_this_include.php"); ?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->

		</div><!-- .col-md-8 -->
	</div>
</article><!-- #post-## -->
