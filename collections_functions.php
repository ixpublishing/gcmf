<?php
session_start();

define('WPV_LOGGING_STATUS', 'info');

//Add custom thumbnail size
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'search-thumb', 269, 269, true ); //(cropped)
}


function searchwp_custom_timeout()
{
    return 25;
}

add_filter( 'searchwp_timeout', 'searchwp_custom_timeout' );


/**
	When perform a search, create an ordered array of the IDs. 
	We'll use these to generate Next/Prev links on the details page.
**/
/*
function get_id($n)
{ return $n->ID; }

add_filter( 'wpv_filter_query_post_process', 'get_array_of_search_result_ids', 10, 2 ); 
function get_array_of_search_result_ids( $query, $view_settings ) 
{
    if ( !empty( $query->posts ) ) { // if the query found no posts
        $_SESSION["search_result_ids"] = array_map("get_id", $query->posts);
    }
    else
    {
    	$_SESSION["search_result_ids"] = null;
    }
    return $query;
}

*/

function add_custom_types_to_tax( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

    // Get all your post types
    $post_types = get_post_types();

    $query->set( 'post_type', $post_types );
    return $query;
  }
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );


function collection_search_scripts() 
{
	wp_enqueue_style( 'font_awesome', "//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" );

	if(get_current_blog_id() == 9)
	{
		wp_enqueue_style( 'collection_search_css', get_template_directory_uri() . '/css/collection-search.css', array('gcmf-style') );

        wp_enqueue_style("royalslider_css", "/wp-content/plugins/new-royalslider/lib/royalslider/royalslider.css");
        // <link rel='stylesheet' id='new-royalslider-core-css-css'  href='http://gcmf.rtssys-dev.com/wp-content/plugins/new-royalslider/lib/royalslider/royalslider.css?ver=3.1.9' type='text/css' media='all' />
		
		if(is_singular("art"))
		{
			wp_enqueue_style( 'collection_details_css', get_template_directory_uri() . '/css/collection-details.css', array('gcmf-style') );
		}
        elseif(is_page_template("page-collection-landing-page.php"))
        {
            wp_enqueue_style( 'collection_landing_page_css', get_template_directory_uri() . '/css/collection-landing-page.css', array('gcmf-style') );
        }
		
		//wp_enqueue_style( 'bootstrap_datepicker', "//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" );
		//wp_enqueue_script( 'bootstrap_datepicker_js', "//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js", array(), '1.0.0', true );

		//wp_enqueue_style( 'bootstrap_select', get_template_directory_uri() . '/css/jquery.selectBoxIt.css' );
		wp_enqueue_script( 'bootstrap_select_js', get_template_directory_uri() . '/js/jquery.selectBoxIt.min.js', array("main"), '1.0.0', true );

		//wp_enqueue_script( 'date_js', get_template_directory_uri() . '/js/date.js', array(), '1.0.0', true );
		wp_enqueue_script( 'elevatezoom', get_template_directory_uri() . '/js/jquery.elevateZoom-3.0.8.min.js', array(), '1.0.0', true );
		//wp_enqueue_script( 'zoomviewer', get_template_directory_uri() . '/js/jquery.zoom.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'cookies_js', get_template_directory_uri() . '/js/cookies.js', array(), '1.0.0', true );

		wp_enqueue_script( 'collection_search_js', get_template_directory_uri() . '/js/collection-search.js', array("bootstrap_select_js"), '1.0.0', true );


	}
}

add_action( 'wp_enqueue_scripts', 'collection_search_scripts' );

// add_filter('wpv_filter_query', 'tc_filter_get_order_arg', 101, 2); // If pass in URL sort param, use that to order by instead of default.
// function tc_filter_get_order_arg($query, $view_settings) {

// 	global $WP_Views;

// 	$view_settings['order'] = "ASC";

// 	return $query;
// }

// add_filter('wpv_view_settings', 'tc_order_by_default_settings', 11, 2);
// function tc_order_by_default_settings($view_settings) {

//     if (!isset($view_settings['orderby'])) {
//         $view_settings['orderby'] = 'post_date';
//     }
//     if (!isset($view_settings['order'])) {
//         $view_settings['order'] = 'ASC';
//     }
    
//     return $view_settings;
// }



//http://stackoverflow.com/questions/8893160/mysql-wordpress-slow-query-when-using-in-statements?rq=1
// function use_straight_join( $distinct_clause ) {

//     $distinct_clause = ( $use_straight_join ) ? 'STRAIGHT_JOIN' . $distinct_clause : $distinct_clause;

//     return $distinct_clause;
// }
// add_filter( 'posts_distinct', 'use_straight_join' );


/**
 * Allow to remove method for an hook when, it's a class method used and class don't have variable, but you know the class name :)
 */
function remove_filters_for_anonymous_class( $hook_name = '', $class_name ='', $method_name = '', $priority = 0 ) {
        global $wp_filter;
        
        // Take only filters on right hook name and priority
        if ( !isset($wp_filter[$hook_name][$priority]) || !is_array($wp_filter[$hook_name][$priority]) )
                return false;
        
        // Loop on filters registered
        foreach( (array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array ) {
                // Test if filter is an array ! (always for class/method)
                if ( isset($filter_array['function']) && is_array($filter_array['function']) ) {
                        // Test if object is a class, class and method is equal to param !
                        if ( is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && get_class($filter_array['function'][0]) == $class_name && $filter_array['function'][1] == $method_name ) {
                                unset($wp_filter[$hook_name][$priority][$unique_id]);
                        }
                }
                
        }
        
        return false;
}

/**
 * Allow to remove method for an hook when, it's a class method used and class don't have global for instanciation !
 */
function remove_filters_with_method_name( $hook_name = '', $method_name = '', $priority = 0 ) {
	global $wp_filter;

	// Take only filters on right hook name and priority
	if ( !isset($wp_filter[$hook_name][$priority]) || !is_array($wp_filter[$hook_name][$priority]) )
	        return false;

	// Loop on filters registered
	foreach( (array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array ) {
	        // Test if filter is an array ! (always for class/method)
	        if ( isset($filter_array['function']) && is_array($filter_array['function']) ) {
	                // Test if object is a class and method is equal to param !
	                if ( is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && $filter_array['function'][1] == $method_name ) {
	                        unset($wp_filter[$hook_name][$priority][$unique_id]);
	                }
	        }
	        
	}

	return false;
}


//remove_filters_with_method_name( 'posts_where', 'acf_field_relationship', 'posts_where', 10);



add_filter( 'acf/fields/relationship/result', 'acf_relationship_result', 10, 2);
function acf_relationship_result( $html, $post )
{
	$new = "";

	// Add Accession Number to Relationship field
	if (!((get_post_meta($post->ID, 'wpcf-accession-number', TRUE))==''))
	{
		$accession_num = get_post_meta($post->ID,'wpcf-accession-number',TRUE); 
	
		// new html
		$new = '<div style="margin:0 3px; color:#444; width:48px; float:left;">' . $accession_num . '</div>';
	}
 
    return $new . $html;
}


function acf_search_accession_num( $args, $field, $post )
{
	//print_r($args);
	//echo " \n ------ \n";
    // eg from https://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
    // $args['meta_query'] = array(
    // 	'relation' => 'OR',
    //     array(
    //         'key' => 'wpcf-accession-number',
    //         'value' => "85",  // . $args['like_title'] . "%",// 'blue',
    //         'compare' => 'LIKE'
    //     )
    // );

    $args['meta_query'] = array(
		    	'relation' => 'OR',
		        array(
		            'key' => 'wpcf-accession-number',
		            'value' => $args['like_title'],// . "%",// 'blue',
		            'compare' => 'LIKE'
		        )
		    );

	//unset($args['like_title']);

   	// $q = new WP_Query($args);
   	// echo $q->request;

    

    //var_dump($args);
    //print_r($args);
 
    return $args;
}
// acf/fields/relationship/result - filter for every field
add_filter('acf/fields/relationship/query', 'acf_search_accession_num', 10, 3);




function gcmf_art_posts_where( $where, &$wp_query )
	{
	    global $wpdb;
	    
	    if ( $title = $wp_query->get('like_title') )
	    {
	        $title_filter = " AND " . $wpdb->posts . ".post_title LIKE '%" . esc_sql( like_escape(  $title ) ) . "%'";

	        $where = str_ireplace($title_filter, "", $where);

	        $where .= " OR " . $wpdb->posts . ".post_title LIKE '%" . esc_sql( like_escape(  $title ) ) . "%'";

	    }
	    
	    return $where;
	}

add_filter( 'posts_where', 'gcmf_art_posts_where', 11, 2 );



//global $wp_filter;
//print_r($wp_filter);

function collections_search_widgets_init() {

	register_sidebar( array(
		'name' => 'Search sidebar',
		'id' => 'search_sidebar_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'collections_search_widgets_init' );



/* Edit FacetWP queries to order by featured first, then default. */
function add_featured_ordering_to_facetwp_query_args( $query_args, $class ) {

	$query_args['meta_query'] = array(
		'relation' => 'OR',
		array(
			'key' => 'featured',
			'compare' => 'EXISTS'//,
			//'value' => array(1,2,null)
		)
		,
		array(
			'key' => 'featured',
			'compare' => 'NOT EXISTS'
		)
	);
    return $query_args;
}

//add_filter( 'facetwp_query_args', 'add_featured_ordering_to_facetwp_query_args', 10, 2 );
function orderbyreplace($orderby) {
    return "mt1.meta_value, mt2.meta_value ASC"; /* str_replace('menu_order',
		'mt1.meta_value',
		$orderby);*/
}
//add_filter('posts_orderby','orderbyreplace');
//$loop = new WP_Query( $args );
//remove_filter('posts_orderby','orderbyreplace');


function my_facetwp_index_row( $params ) {
    global $wpdb;

    // print_r($params);
    // exit();
 
    if ( 'collection' == $params['facet_name'] || 'mobile_collection' == $params['facet_name'] ) {

    	$fields = wpcf_admin_fields_get_fields();

        $values = $params['facet_display_value'];
        foreach ( $values as $key=> $val ) {
        	$cb_field = $fields['collection']['data']['options'][$key];
        	// $wpcf_cb_

        	//wpcf-fields-checkboxes-option-2e767835f41eb0be01b2c6d7eebbd750-1

            $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}facetwp_index
                (post_id, facet_name, facet_source, facet_value, facet_display_value) VALUES (%d, %s, %s, %s, %s)",
                $params['post_id'],
                $params['facet_name'],
                $params['facet_source'],
                md5($key),
                $cb_field['title']// . " ----- " . print_r($key, true)
            ) );
        }
        // skip the default indexing query
        return false;
    }elseif ( 'categories' == $params['facet_name'] || 'mobile_categories' == $params['facet_name'] ) {
    	$fields = wpcf_admin_fields_get_fields();

        $values = $params['facet_display_value'];
        foreach ( $values as $key=>$val ) {
        	$cb_field = $fields['object-category']['data']['options'][$key];
            $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}facetwp_index
                (post_id, facet_name, facet_source, facet_value, facet_display_value) VALUES (%d, %s, %s, %s, %s)",
                $params['post_id'],
                $params['facet_name'],
                $params['facet_source'],
                md5($key),
                $cb_field['title']
                // $val,
                // $val
            ) );
        }
        // skip the default indexing query
        return false;
    }elseif ( 'period' == $params['facet_name'] || 'onoff_view' == $params['facet_name'] || 'era' == $params['facet_name'] ) {

    	$facet_name = $params['facet_name'];
    	if($facet_name == "onoff_view") $facet_name = "status";

    	// $fields = wpcf_admin_fields_get_fields();
    	// $wpcf_field = $fields['object-'. $facet_name];
    	 $wpcf_field_name = str_ireplace("cf/wpcf-", "", $params["facet_source"]);
    	 $wpcf_field = wpcf_admin_fields_get_field($wpcf_field_name);


        //$values = $params['facet_display_value'];
        foreach ( $wpcf_field['data']['options'] as $field ) {
        	if($field['value'] == $params['facet_value'])
        		$period = $field['title'];
        }
    	//$cb_field = $fields['object-period']['data']['options'][$key];
        $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}facetwp_index
            (post_id, facet_name, facet_source, facet_value, facet_display_value) VALUES (%d, %s, %s, %s, %s)",
            $params['post_id'],
            $params['facet_name'],
            $params['facet_source'],
            md5($params['facet_value']),
            $period //. "---" . $wpcf_field_name . "**" .print_r($params, true)// . $cb_field['title']
        ) );
        // skip the default indexing query
        return false;
    }
    return $params;
}
 
 //function my_init_function() {
    add_filter( 'facetwp_index_row', 'my_facetwp_index_row' );
 //}


function gcmf_facet_result_count( $output, $params ) {
    $output = $params['total'] . " Artworks found (Showing " . $params['lower'] . '-' . $params['upper'] . ')';
    //$output =  $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    return $output;
}

add_filter( 'facetwp_result_count', 'gcmf_facet_result_count', 10, 2 );


function implode_r($glue,$arr){
        $ret_str = "";
        foreach($arr as $a){
                $ret_str .= (is_array($a)) ? implode_r($glue,$a) :strval($a) . $glue ;
        }
        if(strrpos($ret_str,$glue)!=strlen($glue))
            $ret_str = substr($ret_str,0,-(strlen($glue)));
        return $ret_str;
}

function facetwp_get_result_ids( $output, $class ) {

    $_SESSION["search_result_ids"] = $class->get_filtered_post_ids();

    //$cur_url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    $_SESSION["search_result_params"] = "#/";
    foreach($class->facets as $facet)
    {
    	if(!empty($facet["selected_values"]))
    	{
    		$_SESSION["search_result_params"] .= $facet["facet_name"] . "=" . implode(",", $facet["selected_values"]) . "&";
    	}
    }
    //$_SESSION["search_result_params"] = print_r($class->facets, true);
    return $output;
}

//add_filter( 'facetwp_pre_filtered_post_ids', 'my_facetwp_pre_filtered_post_ids', 10, 2 );
add_filter( 'facetwp_template_html', 'facetwp_get_result_ids', 10, 2 );




// function custom_columns( $column, $post_id ) {
//   //switch ( $column ) {
//   //  case "column-meta":
//       $col_meta = get_post_meta( $post_id, 'wpcf-accession-number', true);
//       echo '<a href="' . $col_meta . '">' . $col_meta. '</a>';
//   //    break;
//   //}
// }
//add_action( "manage_posts_custom_column", "custom_columns", 10, 2 );

// Make these columns sortable
function sortable_columns($columns) {
  $columns['column-meta'] = 'accession_num';
  return $columns;
}

add_filter( "manage_edit-art_sortable_columns", "sortable_columns" );

add_action( 'pre_get_posts', 'accession_num_orderby' );
function accession_num_orderby( $query ) {
	if( ! is_admin() )
		return;

	$orderby = $query->get( 'orderby');

	if( 'accession_num' == $orderby ) {
		$query->set('meta_key','wpcf-accession-number');
		$query->set('orderby','meta_value_num');
	}
}



//// FROM: http://wordpress.stackexchange.com/questions/16637/how-to-filter-post-listing-in-wp-dashboard-posts-listing-using-a-custom-field/16641#16641
/*
Plugin Name: Admin Filter BY Custom Fields
Plugin URI: http://en.bainternet.info
Description: Filter posts or pages in admin by custom fields (post meta)
Version: 1.0
Author: Bainternet
Author URI: http://en.bainternet.info
*/

add_filter( 'parse_query', 'ba_admin_posts_filter' );
add_action( 'restrict_manage_posts', 'ba_admin_posts_filter_restrict_manage_posts', 100 );

function ba_admin_posts_filter( $query )
{
    global $pagenow;
    if ( is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
        $query->query_vars['meta_key'] = $_GET['ADMIN_FILTER_FIELD_NAME'];
    if (isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '')
        $query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
    }
}

function ba_admin_posts_filter_restrict_manage_posts()
{
    global $wpdb, $post_type, $current_screen;

    if($current_screen->post_type == 'art')
    {
	    if(is_array($post_type))
	    		$post_type = $current_screen->post_type;

	    $sql = 'SELECT DISTINCT meta_key FROM '.$wpdb->postmeta.' ORDER BY 1';
	    $fields = $wpdb->get_results($sql, ARRAY_N);

	    
		?>
		<script>
			jQuery("input[name=post_type]").val("<?php echo $post_type; ?>");
		</script>
		<select name="ADMIN_FILTER_FIELD_NAME">
		<option value=""><?php _e('Filter By Custom Fields', 'baapf'); ?></option>
		<?php
		    $current = isset($_GET['ADMIN_FILTER_FIELD_NAME'])? $_GET['ADMIN_FILTER_FIELD_NAME']:'';
		    $current_v = isset($_GET['ADMIN_FILTER_FIELD_VALUE'])? $_GET['ADMIN_FILTER_FIELD_VALUE']:'';
		    foreach ($fields as $field) {
		        if (substr($field[0],0,1) != "_" && $field[0] == 'wpcf-accession-number'){
		        printf
		            (
		                '<option value="%s"%s>%s</option>',
		                $field[0],
		                $field[0] == $current? ' selected="selected"':'',
		                "Accession #"//$field[0]
		            );
		        }
		    }
		?>
		</select> 
		<?php _e('Value:', 'baapf'); ?><input type="TEXT" name="ADMIN_FILTER_FIELD_VALUE" value="<?php echo $current_v; ?>" />
		<?php
	}
}