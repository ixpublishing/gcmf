<?php
/**
 * The template for displaying search forms in gcmf
 *
 * @package gcmf
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="sr-only"><?php _ex( 'Search for:', 'label', 'gcmf' ); ?></span>
		<input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Enter keywords &hellip;', 'placeholder', 'gcmf' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'gcmf' ); ?>">
	</label>
	<input type="submit" class="search-submit btn utility-icons-search-white" value="">
</form>
