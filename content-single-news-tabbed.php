<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
		<div class="col-md-4">
		
			<?php
			
			//$taxonomy_filter = array('partner-category', 'program-category');
			//$tax_terms_filtered = wp_get_object_terms( $post->ID, $taxonomy_filter );
			//$parent_term_id = $tax_terms_filtered[0]->parent;
			//$parent_taxonomy = $tax_terms_filtered[0]->taxonomy;
			//$parent_term = get_term_by('id', $parent_term_id , $parent_taxonomy );
			
			?>
			
			<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				  the_post_thumbnail('full', array('class'=>'img-responsive') );
				} else {
					echo '<img src="' . get_template_directory_uri() . '/img/placeholders/-placeholder.png" class="img-responsive" alt="" />';
				}
					
			?>
			
				<?php 


				$article_cats = get_the_terms( $post->ID, 'news-category');
				$article_cats_list = array();

				foreach($article_cats as $article_cat) {
					$article_cats_list[] = '<a href="' . get_term_link($article_cat) . '">' . $article_cat->name . '</a>';
				}?>
			
		</div><!-- .col-md-4 -->
		
		<div class="col-md-8">
		
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<p class="entry-subheading"><?php echo types_render_field( "subheading", array( ) ) ?></p>
				<p class="entry-date"><?php echo get_the_date(); ?></p>
			</header><!-- .entry-header -->

			<div class="pressroom-tab-holder shortcode-tabs">
			
				<div class="tab-hold tabs-wrapper">
				
					<ul id="tabs" class="tabset tabs">
						<li><a href="#tab1">Story</a></li>
						<li><a href="#tab2">Images</a></li>
					</ul>
					<div class="tab-box tabs-container">
					
						<div id="tab1" class="tab tab_content">

							<div class="entry-content">
								<?php the_content(); ?>
								<?php
									wp_link_pages( array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
										'after'  => '</div>',
									) );
								?>
							</div><!-- .entry-content -->

							<footer class="entry-meta">
								<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
							</footer><!-- .entry-meta -->
						</div>
						
						<div id="tab2" class="tab tab_content">
							<?php echo types_render_field("media-gallery", array( )) ?>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .col-md-8 -->
	</div>
</article><!-- #post-## -->
