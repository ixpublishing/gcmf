<?php $current_blog = get_blog_details(1); ?>
<div class="global-search-form-wrapper">
	<form class="global-search-form form-control" role="search" action="<?php echo esc_url( trailingslashit( $current_blog->path . global_site_search_get_search_base() ) ) ?>">
		<label>
			<span class="sr-only"><?php _ex( 'Search for:', 'label', 'gcmf' ); ?></span>
			<input type="text" name="phrase" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Enter keywords&hellip;', 'placeholder', 'gcmf' ); ?>" value="<?php echo esc_attr( stripslashes( global_site_search_get_phrase() ) ) ?>" title="<?php _ex( 'Search for:', 'label', 'gcmf' ); ?>">
		</label>
		
		<button type="submit" class="search-submit btn utility-icons-search-white">
        </button>
	</form>
</div>


				