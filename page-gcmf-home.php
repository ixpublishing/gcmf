<?php
/**
 * Template Name: GCMF Home Page
 *
 *
 * @package gcmf
 */

get_header('no-hero'); ?>

<div id="sticky-hero">
  	<div id="hero" class="full-hero">
		<?php echo get_new_royalslider(1); ?>
		<div class="container nav-bullets gcmf-hero-slider-skin"></div>
	</div>
		<div id="content" class="site-content container">
				<?php

					
						if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-no-title' ); ?>

						<div class="row">
							<div class="col-md-6">
								<h2 class="section-header">Explore the Digital Archives &#187;</span>
									<?php	
											switch_to_blog(7); //switch to pressroom to get articles
										// Get Related Posts
										$cat = get_category_by_slug($atts['slug']);
										$args = array(
										'post_type' => 'archive',
										'category' => $cat->term_id,
										'posts_per_page' => $posts
										);
										

									$related_news  = get_posts( $args );
										$html = '';
										global $post;
										$html .= '<ul class="list-unstyled entry-list">';
										foreach ($related_news as $post) : setup_postdata( $post ); {
											//print_r($post);
											$html .= '<li class="entry-list-item">';
											$html .= '<div class="row">';
											$html .= '<div class="col-xs-2 entry-list-thumb"><a href="' . get_permalink() . '">';
											$html .= ( '' != get_the_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail') : '<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/thumbnail-default.png" alt="" />';
											$html .= '</a></div>';
											$html .= '<div class="col-xs-10 entry-list-content"><p class="entry-date">'. get_the_date() . '</p><p class="entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
											$html .= '<p class="excerpt">' . the_excerpt_max_charlength(120) . ' <a href="'. get_permalink() .'" class="jumplink">Read more</a></p>';
											$html .= '</div>';
											$html .= '</div>';
											$html .= '</li>';
										}
										endforeach;
										$html .= '</ul>';
										wp_reset_postdata();
										restore_current_blog(); //switch back to main site
										return $html;
									
									?>
							</div>

							<div class="col-md-6">

								<h2 class="section-header">Upcoming Events &#187;</h2>
									<ul>
										<?php
										    global $switched;
										    switch_to_blog(7); //switched to blog id 2
										 
										    // Get latest Post
										    $latest_posts = get_posts('category=-7&numberposts=2&orderby=post_name&order=DSC');
										    $cnt =0;
										?> 
									    <ul>
									    <?php foreach($latest_posts as $post) : setup_postdata($post);?>
									        <li>
									            <a href="<?php echo get_page_link($post->ID); ?>" title="<?php echo $post->post_title; ?>"><?php echo $post->post_title; ?></a>
									        </li>                                
									    <?php endforeach ; ?>
									 
									<?php restore_current_blog(); //switched back to main site ?>
									</ul>
								</div>
						</div> <!--End Row-->

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
					
</div><!-- #content -->

<?php get_footer(); ?>