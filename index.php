<?php
/**
 * The main template file.
 *
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div class="row">
	
		<div class="col-md-9">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<h1><?php wp_title(''); ?></h1>
					<div class="article">
			
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<article class="post excerpt">
					<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow" id="featured-thumbnail">
					<div class="col-sm-3 col-xs-12 entry-list-thumb">
						<?php 
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							  the_post_thumbnail('thumbnail');
							} 
						?>
					</div>
					<div class="col-xs-12 col-sm-9 entry-list-content">
						<div class="featured-cat"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></div>
					<header>						
						<h2 class="title">
							<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h2>
						<div class="post-info"><span class="theauthor"><?php the_author_posts_link(); ?></span> | <span class="thetime"><?php the_time( get_option( 'date_format' ) ); ?></span></div>
							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->
						</div>
					</header><!--.header-->
					</a>
				</article>
			<?php endwhile; else: ?>
				<div class="no-results">
					<h5><?php _e('No results found. We apologize for any inconvenience, please hit back on your browser or use the search form below.', 'gcmf'); ?></h5>
					<?php get_search_form(); ?>
				</div><!--noResults-->
			<?php endif; ?>
			<!--Start Pagination-->
			<?php if ( isset($mts_options['mts_pagenavigation']) && $mts_options['mts_pagenavigation'] == '1' ) { ?>
				<?php  $additional_loop = 0; global $additional_loop; mts_pagination($additional_loop['max_num_pages']); ?>           
			<?php } { ?>
			
			<?php } wp_reset_query(); ?>
			<!--End Pagination-->			
			</main><!-- #main -->
	</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">

		<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>