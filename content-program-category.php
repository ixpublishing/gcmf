<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	
	$taxonomy_filter = 'program-category';
	$tax_terms_filtered = wp_get_object_terms( $post->ID, $taxonomy_filter );
	$parent_term_id = $tax_terms_filtered[0]->parent;
	$parent_taxonomy = $tax_terms_filtered[0]->taxonomy;
	$parent_term = get_term_by('id', $parent_term_id , $parent_taxonomy );
	
	?>	
			
			<?php /*
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				 echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '">' . the_post_thumbnail('full', array('class'=>'img-responsive') ) . '</a>';
				} else {
					echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '"><img src="' . get_template_directory_uri() . '/img/placeholders/' . $parent_term->slug . '-placeholder.png" class="img-responsive" alt="" /></a>';
				}
					
			*/?>
		
			<?php //get information about all taxonomies associated with post
			/*$post = get_post($post->ID);
			$post_type = $post->post_type;
			$taxonomies = get_object_taxonomies($post_type);//array of post taxonomies
			//get_the_term_list( $post->ID, $taxonomies );
			$tax_terms = get_the_terms($post->ID, $taxonomies);
			print_r($tax_terms);
			if ( has_term('', 'faculty-member', $post->ID) ) {
			
				$faculty_terms = get_the_terms($post->ID, 'faculty-member');
				
				foreach($faculty_terms as $faculty_term) {
					$term_link = get_term_link ($faculty_term, 'faculty-member');
					echo '<a href="' . $term_link . '">' . $faculty_term->name . '</a>';
				}	
				//print_r($faculty_terms);
			}*/
			?>
			
			<header class="entry-header">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			</header><!-- .entry-header -->

			<div class="entry-content">
			
			<?php
			if ( is_category() || is_archive() ) {
				the_excerpt();
			} else {
				the_content();
			} 
			?>
			<a class="btn orange" href="/statewide/request-program/" title="<?php the_title(); ?>">Request Program</a>
				
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
	
	<?php $args = array(
			//default to current post
			'post' => 0,
			'before' => '<p class="related-items">',
			//this is the default
			'sep' => '<br />',
			'after' => '</p>',
			//this is the default
			'template' => '%s: %l'
			);
			echo the_taxonomies($args);
			//echo get_the_term_list( $post->ID, 'program-category', 'Related Categories: ', ', ', '' );
			

$taxonomy = 'program-category'; //Choose the taxonomy
$terms = get_the_terms( $post->ID, $taxonomy ); //Get all the terms
//print_r($terms);
foreach($terms as $term) {
	//echo $term->parent;
}
		?>
				
</article><!-- #post-## -->
