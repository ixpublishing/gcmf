<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
		<div class="col-md-4">
		
			<?php
			
			$taxonomy_filter = 'partner-category';
			$tax_terms_filtered = wp_get_object_terms( $post->ID, $taxonomy_filter );
			$parent_term_id = $tax_terms_filtered[0]->parent;
			$parent_taxonomy = $tax_terms_filtered[0]->taxonomy;
			$parent_term = get_term_by('id', $parent_term_id , $parent_taxonomy );
			
			?>
			
			<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				  the_post_thumbnail('full', array('class'=>'img-responsive') );
				} else {
					echo '<img src="' . get_template_directory_uri() . '/img/placeholders/' . $parent_term->slug . '-placeholder.png" class="img-responsive" alt="" />';
				}
					
			?>
		</div><!-- .col-md-4 -->
		
		<div class="col-md-8">
		
			<?php //get information about all taxonomies associated with post
			/*$post = get_post($post->ID);
			$post_type = $post->post_type;
			$taxonomies = get_object_taxonomies($post_type);//array of post taxonomies
			echo get_the_term_list( $post->ID, $taxonomies );
			$tax_terms = get_the_terms($post->ID, $taxonomies);
			print_r($tax_terms);
			if ( has_term('', 'faculty-member', $post->ID) ) {
			
				$faculty_terms = get_the_terms($post->ID, 'faculty-member');
				
				foreach($faculty_terms as $faculty_term) {
					$term_link = get_term_link ($faculty_term, 'faculty-member');
					echo '<a href="' . $term_link . '">' . $faculty_term->name . '</a>';
				}
				//print_r($faculty_terms);
			}*/
			?>
			
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header><!-- .entry-header -->
			<?php
			$address_1 = types_render_field("partner-address-1", array());
			$address_2 = types_render_field("partner-address-2", array());
			$city = types_render_field("partner-city", array());
			$state = types_render_field("partner-state", array());
			$zip = types_render_field("partner-zip", array());
			$phone = types_render_field("partner-phone", array());
			$email = types_render_field("partner-email", array());
			$url = types_render_field("partner-website-url", array("output"=>"raw"));
			$url_text = preg_replace('#^https?://#', '', $url);
			?>

			<div class="entry-content">
				<p class="partner-details">
				<?php if(!($address_1)=='' ) { echo $address_1 . '<br />'; }
					  if(!($address_2)=='' ) { echo $address_2 . '<br />'; }
					  if(!($city)=='' ) { echo $city; }
					  if(!($state)=='' ) { echo ',' . $state;}
					  if(!($zip)=='' ) { echo ' ' . $zip . '<br />'; }
					  if(!($phone)=='' ) { echo $phone . '<br />'; }
					  if(!($email)=='' ) { echo $email . '<br />'; }
					  if(!($url)=='' ) { echo '<a href="'. $url .'" title="'. $url .'">' . $url_text . '</a>'; }
					  ?>
				</p>
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
	
		</div><!-- .col-md-8 -->
</article><!-- #post-## -->
