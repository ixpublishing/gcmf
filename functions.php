<?php
/**
 * gcmf functions and definitions
 *
 * @package gcmf
 */

add_filter( 'searchwp_omit_document_processing', '__return_true' );
//add_filter( 'searchwp_debug', '__return_true' );

function searchwp_custom_timeout()
{
    return 25;
}
add_filter( 'searchwp_timeout', 'searchwp_custom_timeout' );


// Throttle searchwp indexer
//https://gist.github.com/jchristopher/10730056
// pause the indexer for 1s in between passes
function my_searchwp_indexer_throttle() {
	return 1;
}
 
add_filter( 'searchwp_indexer_throttle', 'my_searchwp_indexer_throttle' );
 
 
// only process 3 posts per indexer pass (instead of the default of 10)
function my_searchwp_index_chunk_size() {
	return 3;
}
 
add_filter( 'searchwp_index_chunk_size', 'my_searchwp_index_chunk_size' );
 
 
// only process 200 terms at a time
function my_searchwp_process_term_limit() {
	return 200;
}
 
add_filter( 'searchwp_process_term_limit', 'my_searchwp_process_term_limit' );
/** end **/



class Post_Category_Walker extends Walker_Category {

	private $term_ids = array();

	private $letter_filter = null;

	function __construct( $post_id, $taxonomy, $letter_filter = false)  {
		// fetch the list of term ids for the given post
		$this->term_ids = wp_get_post_terms( $post_id, $taxonomy, 'fields=ids' );

		if($letter_filter)
		{
			$this->letter_filter = $letter_filter;
		}
	}

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		if($this->letter_filter && substr($element->name, 0, 1) != $this->letter_filter)
		{
			//we're filtering alphabetically, and this doesn't match.
			return;
		}

		$id = $element->term_id;

		//$child_count = $element->category_count; // count($children_elements[ $id ]);
		$tax_link = get_term_link(intval($element->term_id), $element->taxonomy);
		$tax_desc = term_description(intval($element->term_id), $element->taxonomy);
		$tax_digit = get_field('collection_digitized', $element);
		if ($tax_digit) {
			$tax_digit = '<span class="format-icon format-digitalarchives"> &#160;</span>';
			} else {
			$tax_digit = ''; }			
		//$tax_img = get_field("collection_image", $element);
		//$tax_img_url = $tax_img['url'];

		//if (false == $tax_img) {
		//$tax_img_url = 'http://placehold.it/170x240/046598';
			//}

		//has_post_thumbnail(intval($element->term_id), $element->taxonomy);

		 $output .= 
		 sprintf( "<div class='collection-box'><div class='row'>\n<div class='col-sm-12 collection-box-content'><div class='inner-content'><h3><a href='%s'>%s</a> %s </h3> %s <a href='%s'>About this Collection &#187;</a> &nbsp;|&nbsp; <a href='/library/results/#!/collection=%s' target='_blank'>Search the Collection &#187;</a> </div></div>\n</div><hr></hr></div>",
               
            $tax_link,
            $element->name,
            $tax_digit,
            //$child_count,
            $tax_desc,
            $tax_link,
            $id

            //$tax_img_url

        );
        $output .= "<!-- " .print_r($element, true) . "-->";
	}
}


//ini_set('display_errors',1); 
//error_reporting(E_ALL);
global $blog_id;
if($blog_id == '1') {
	add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
	add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );
	add_filter( 'the_content', 'remove_thumbnail_dimensions', 10 );
	function remove_thumbnail_dimensions( $html ) {
		$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
		return $html;
	}
}


function gcmf_facetwp_index_row( $params, $class ) {
    if ( in_array($params['facet_name'], array("subject", "format", "format_dropdown") )) {

		$fields = wpcf_admin_fields_get_fields();

        $values = $params['facet_display_value'];
		if(!is_array($values)):
			$wpcf_field_name = str_ireplace("cf/wpcf-", "", $params["facet_source"]);
		        $wpcf_field = wpcf_admin_fields_get_field($wpcf_field_name);
			foreach ( $wpcf_field['data']['options'] as $field ) {
	        		if($field['value'] == $params['facet_value'])
	        			$cb_field = $field['title'];
	        	}

			$params['facet_display_value'] = print_r($cb_field, true);// $cb_field['title'];//get_the_title($params['post_id']) .  print_r($fields, true);
			$class->insert($params);
			return false;

		else:
          foreach ( $values as $key=> $val ) {

				$cb_field = $fields[$params['facet_name']]['data']['options'][$key];
				$params['facet_value'] = $cb_field['set_value']; //md5($key);
				$params['facet_display_value'] = $cb_field['title'];
				$class->insert($params);
			}
	  return false;
	endif;
    }
    return $params;
}
add_filter( 'facetwp_index_row', 'gcmf_facetwp_index_row', 10, 2 );

/*
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * This code allows the theme to work without errors if the Options Framework plugin has been disabled.
 */
if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	$optionsframework_settings = get_option('optionsframework');
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}

/*
 * Allow shortcodes inside CF7
 */
add_filter( 'wpcf7_form_elements', 'do_shortcode' );

/*
 * Options framework allow script tags
 */
add_action('admin_init','optionscheck_change_santiziation', 100);
function optionscheck_change_santiziation() {
    remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
    add_filter( 'of_sanitize_textarea', 'custom_sanitize_textarea' );
}
function custom_sanitize_textarea($input) {
    global $allowedposttags;
      $custom_allowedtags["script"] = array();
      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
      $output = wp_kses( $input, $custom_allowedtags);
    return $output;
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'gcmf_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function gcmf_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on gcmf, use a find and replace
	 * to change 'gcmf' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'gcmf', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	
	add_image_size( 'nav-thumbnail', 100, 70, true ); // Hard Crop Mode
	
	add_image_size( 'pressroom-thumbnail', 75, 75, true ); // Hard Crop Mode	
	
	add_image_size( 'related-image', 150, 150); // Soft Crop Mode
	
	add_image_size( 'content-box', 400, 257, true ); // Hard Crop Mode
	
	add_image_size( 'cta-mb-image', 270, 200, true ); // Hard Crop Mode

	add_image_size( 'collection-list-image', 170, 240, true ); // Hard Crop Mode
	
	add_image_size( 'listview-img', 290, 125, true ); // Hard Crop Mode
	
	add_image_size( 'half-slider', 555, 276, true ); // Hard Crop Mode
	
	add_image_size( 'quarter-slider', 263, 276, true ); // Hard Crop Mode
	
	add_image_size( 'one-third-slider', 360, 276, true ); // Hard Crop Mode
	
	add_image_size( 'section-slider', 700, 450, true ); // Hard Crop Mode

	add_image_size( 'brand-box', 265, 270, true ); // Hard Crop Mode

	add_image_size( 'feature-box-main', 255, 435, true ); // Hard Crop Mode

	add_image_size( 'feature-box', 255, 208, true ); // Hard Crop Mode

	add_filter( 'image_size_names_choose', 'gcmf_custom_sizes' );

	function gcmf_custom_sizes( $sizes ) {
		return array_merge( $sizes, array(
			'half-slider' => __('Featured Slider Lg (555x276)', 'gcmf'),
			'quarter-slider' => __('Featured Slider Sm (263x276)', 'gcmf'),
			'section-slider' => __('Exhibition/Section Slider (700x450)', 'gcmf'),
			'content-box' => __('Content Box Image (400x257)', 'gcmf'),
			'cta-mb-image' => __('Call to Action Box Image', 'gcmf'),
			'collection-list-image' => __('Collection Box Image (170x240)', 'gcmf'),
			'nav-thumbnail' => __('Image for Previous and Next Links', 'gcmf'),
			'brand-box' => __('Brand Box Image', 'gcmf'),
			'feature-box-main' => __('Feature Box Image', 'gcmf'),
			'feature-box' => __('Feature Box Small Image', 'gcmf')
		) );
	}

	/**
	 * This theme uses wp_nav_menu() in multiple locations.
	 */
	 
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'gcmf' ),
		'secondary' => __( 'Secondary Navigation', 'gcmf' ),
		'top_menu' => __( 'Top Menu', 'gcmf' ),
		'footer_menu_a' => __( 'Footer Menu A', 'gcmf' ),
		'footer_menu_b' => __( 'Footer Menu B', 'gcmf' ),
		'footer_menu_c' => __( 'Footer Menu C', 'gcmf' ),
		'footer_menu_d' => __( 'Footer Menu D', 'gcmf' ),
		'footer_menu_e' => __( 'Footer Menu E', 'gcmf' ),
		'footer_menu_f' => __( 'Footer Menu F', 'gcmf' ),
		'footer_menu_g' => __( 'Footer Menu G', 'gcmf' ),
	) );

/*
remove secondary navigation from main site (Home Page)
*/
add_action( 'admin_init', 'multisite_nav_unregister' );
 
function multisite_nav_unregister(){
 
	if ( is_main_site() ) {
	unregister_nav_menu( 'secondary' );
	}
	
	if ( !is_main_site() ) {
	unregister_nav_menu( 'primary' );
	unregister_nav_menu( 'top_menu' );
	unregister_nav_menu( 'footer_menu_a' );
	unregister_nav_menu( 'footer_menu_b' );
	unregister_nav_menu( 'footer_menu_c' );
	unregister_nav_menu( 'footer_menu_d' );
	unregister_nav_menu( 'footer_menu_e' );
	unregister_nav_menu( 'footer_menu_f' );
	unregister_nav_menu( 'footer_menu_g' );
	}
 
}
	
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

}
endif; // gcmf_setup
add_action( 'after_setup_theme', 'gcmf_setup' );

/**
 * Register widgetized areas and update sidebar with default widgets
 */
function gcmf_widgets_init() {
	global $blog_id;
	
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'gcmf' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Message', 'gcmf' ),
		'id'            => 'footer-message',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Email Sign-up Heading', 'gcmf' ),
		'id'            => 'signup-text',
		'before_widget' => '<div class="title">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	
		register_sidebar( array(
			'name'          => __( 'Alert', 'gcmf' ),
			'id'            => 'alert',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
		) );
	
	
	
}
add_action( 'widgets_init', 'gcmf_widgets_init' );



//Enqueue scripts and styles
function gcmf_scripts() {
		
	wp_enqueue_style( 'gcmf-style', get_stylesheet_directory_uri() . '/style.css' );
		
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '123', true );
	
	wp_enqueue_script( 'gcmf-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
		
	wp_enqueue_script( 'magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ) );
	
	wp_enqueue_script( 'add-to-any', 'http://static.addtoany.com/menu/page.js', array(), '1.0', true );
	
	wp_enqueue_script( 'pin-images', get_template_directory_uri() . '/js/pinimages.min.js', array() );
	
	wp_enqueue_script( 'placeholders', get_template_directory_uri() . '/js/placeholders.jquery.min.js', array( 'jquery' ) );
	
	if (is_page()) {
		wp_enqueue_script( 'match-height', get_template_directory_uri() . '/js/jquery.matchHeight.js', array( 'jquery' ) );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}	
}
add_action( 'wp_enqueue_scripts', 'gcmf_scripts' );


function add_first_and_last($output) {
  
	$output= preg_replace('/menu-item /', 'first-menu-item menu-item ', $output, 1);
	$output=substr_replace($output, "last-menu-item menu-item", strripos($output, "menu-item"), strlen("menu-item"));

  return $output;
}
add_filter('wp_nav_menu', 'add_first_and_last');

add_filter('wp_nav_menu_items','add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {
    if( $args->menu == 'Main Navigation' ) {
        return $items."<li class='menu-header-search'><form action='http://example.com/' id='searchform' method='get'><input type='text' name='s' id='s' placeholder='Search'></form></li>";
		
	} else {

    return $items;
	
	}
}

//Mobile Detect
if(!class_exists('Mobile_Detect')){
  require_once get_template_directory() . '/inc/Mobile_Detect.php';
}

// Shortcodes
include_once('shortcodes.php');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


class UL_Class_Walker extends Walker_Nav_Menu {
  function start_lvl(&$output, $depth) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"sub-menu-level-".$depth." list-unstyled\">\n";
  }
  
  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        $id_field = $this->db_fields['id'];
        if ( !empty( $children_elements[ $element->$id_field ] ) ) {
            $element->classes[] = 'has-children';
        }
        Walker_Nav_Menu::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

add_filter( 'wp_nav_menu_objects', 'add_menu_class', 10, 2 );
	function add_menu_class( $sorted_menu_items, $args ) {
	if ($args->theme_location == "primary") {
		$count = 0;
		foreach ($sorted_menu_items as $i => $menu ) {
			//print_r($menu);
			 if ($menu->menu_item_parent == 0) {
				$sorted_menu_items[$i]->classes[] = 'primary-nav-parent-'.strtolower(str_replace(' ', '-', $count));
				$count++;
			 }
		}
		
	}
	return $sorted_menu_items;
}

//Add body class to multisites
add_filter('body_class', 'multisite_body_classes');

function multisite_body_classes($classes) {
        $id = get_current_blog_id();
        $slug = strtolower(str_replace(' ', '-', trim(get_bloginfo('name'))));
        $classes[] = $slug;
        $classes[] = 'site-id-'.$id;
        return $classes;
}

//Change post_class() echo to return for use in shortcodes
function shortcode_post_class( $class = '', $post_id = null ) {
	// Separates classes with a single space, collates classes for post DIV
	$classes = 'class="' . join( ' ', get_post_class( $class, $post_id ) ) . '"';
	return $classes;
}

// Set Default Link for Media items to 'None'
function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

//Add Royalslider custom skins
add_filter('new_royalslider_skins', 'gcmf_new_royalslider_add_custom_skin', 10, 2); 
	function gcmf_new_royalslider_add_custom_skin($skins) {
	 $skins['gcmf-section-slider-skin'] = array(
	           'label' => 'GCMF Section Slider Skin',
	           //'path' => get_stylesheet_directory_uri() . '/some-path/my-custom-skin.css'  // get_stylesheet_directory_uri returns path to your theme folder
	      );
		  $skins['gcmf-hero-slider-skin'] = array(
	           'label' => 'GCMF Hero Slider Skin',
	           //'path' => get_stylesheet_directory_uri() . '/some-path/my-custom-skin.css'  // get_stylesheet_directory_uri returns path to your theme folder
	      );
		  
	      return $skins;
}

//Pagination
if ( ! function_exists( 'paginate' ) ) :
	function paginate( $location ) {
		global $wp_query;

		$big = 999999999; // need an unlikely integer
		$html = '';
		$html .= '<div class="pagination '. $location .'">';
		$html .= paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'show_all' => true,
			'prev_text' => '&#171;',
			'next_text' => '&#187;',
			'type' => 'list'
		) );
		$html .= '</div>';
		
		return $html;
	}
endif;

function global_site_search_pagination( $mainlink = '' ) {
	global $network_query, $current_site;
	if ( absint( $network_query->max_num_pages ) <= 1 ) {
		return '';
	}

	if ( empty( $mainlink ) ) {
		$mainlink = $current_site->path . global_site_search_get_search_base() . '/' . urlencode( global_site_search_get_phrase() );	  	 	 	 				 	 	  
	}

	return paginate_links( array(
		'base'      => trailingslashit( $mainlink ) . '%_%',
			'format' => '?paged=%#%',
		'total'     => $network_query->max_num_pages,
		'current'   => !empty( $network_query->query_vars['paged'] ) ? $network_query->query_vars['paged'] : 1,
			'show_all' => true,
			'prev_text' => '&#171;',
			'next_text' => '&#187;',
			'type' => 'list'
	) );
	
	return $html;
}


//Force Subcategories to use parent category archive template
function new_subcategory_hierarchy() { 
    $category = get_queried_object();
 
    $parent_id = $category->category_parent;
 
    $templates = array();
     
    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );
 
        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
 
        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}
 
add_filter( 'category_template', 'new_subcategory_hierarchy' );

//add_filter( 'img_caption_shortcode', 'gcmf_img_caption', 10, 3 );

function gcmf_img_caption( $out, $attr, $content ) {
     extract( shortcode_atts( array(
          'id'      => '',
          'align'   => 'alignnone',
          'width'   => '',
          'caption' => '',
		  'desc' => 1
     ), $attr ) );

     if ( 1 > (int) $width || empty( $caption ) ) {
          return $content;
     }

     if ( $id )
	{
		$id = 'id="' . esc_attr( $id ) . '" ';
		$html_desc = '';
			
		$img  = get_post( preg_replace( '/[^0-9]/', '', $id ) );
		$html_desc = trim( $img->post_content );
		
		if ( '' !== $desc )
		{
			$html_desc = '<p class="wp-desc-text">' . $html_desc . '</p>';
		}

	}
     return '<div ' . $id . 'class="wp-caption ' . esc_attr( $align ) . '">' . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p>' . $html_desc . '</div>';
}

function wp_get_attachment( $attachment_id ) {
	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'title' => $attachment->post_title
	);
}

// Enqueue Scripts/Styles for our Lightbox
function gcmf_add_lightbox() {
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/inc/lightbox/js/jquery.fancybox.pack.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/inc/lightbox/js/lightbox.js', array( 'fancybox' ), false, true );
    wp_enqueue_style( 'lightbox-style', get_template_directory_uri() . '/inc/lightbox/css/jquery.fancybox.css' );
}
add_action( 'wp_enqueue_scripts', 'gcmf_add_lightbox' );





//Output button text based on Admission Type taxonomy


//Remove CPT slug
/**
 * Remove the slug from published post permalinks. Only affect our CPT though.
 */
function vipx_remove_cpt_slug( $post_link, $post, $leavename ) {
 
    if ( ! in_array( $post->post_type, array( 'exhibitions', 'news', 'programs' ) ) 
    	|| 'publish' != $post->post_status )
        return $post_link;
 
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
 
    return $post_link;
}
//add_filter( 'post_type_link', 'vipx_remove_cpt_slug', 10, 3 );

/**
 * Some hackery to have WordPress match postname to any of our public 
 * post types. All of our public post types can have /post-name/ as 
 * the slug, so they better be unique across all posts. Typically core 
 * only accounts for posts and pages where the slug is /post-name/
 */
function vipx_parse_request_tricksy( $query ) {
 
    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;
 
    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query )
        || ! isset( $query->query['page'] ) )
        return;
 
    // 'name' will be set if post permalinks are just post_name, 
    // otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) )
        $query->set( 'post_type', array( 'post', 'documents', 'photographs', 'page', 'posters', 'holding', 'maps', 'microfilm', 'digital-archive', 'oral-histories', 'video', 'audio' ) );
}
add_action( 'pre_get_posts', 'vipx_parse_request_tricksy' );

	 
//Dynamically change colors for style sheets 
function adjustBrightness($hex, $steps) {
    // Steps should be between -255 and 255. Negative = darker, positive = lighter
    $steps = max(-255, min(255, $steps));

    // Format the hex color string
    $hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }

    // Get decimal values
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));

    // Adjust number of steps and keep it inside 0 to 255
    $r = max(0,min(255,$r + ($r * ($steps / 255))));
    $g = max(0,min(255,$g + ($g * ($steps / 255))));  
    $b = max(0,min(255,$b + ($b * ($steps / 255))));

    $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
    $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
    $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

    return '#'.$r_hex.$g_hex.$b_hex;
}

//Truncate excerpt
function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			return trim(mb_substr( $subex, 0, $excut )) . '...';
		} else {
			return trim($subex) . '...';
		}
		return '[...]';
	} else {
		return $excerpt;
	}
}

//Add custom login screen 

function custom_login_css() {
//echo '&lt;link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/custom-login.css" /&gt;';
}
add_action('login_head', 'custom_login_css');

//Enqueue custom admin area style sheet
function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

function custom_login_stylesheet() { 
	wp_register_style( 'custom_wp_login_css', get_stylesheet_directory_uri() . '/css/custom-login.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_login_css' );
}
add_action( 'login_enqueue_scripts', 'custom_login_stylesheet' );


function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return implode(",", $rgb); // returns the rgb values separated by commas
   //return $rgb; // returns an array with the rgb values
}

/**
 * html5_shiv function.
 * 
 * @access public
 * @return void
 */
function html5_shiv() {
        ?>
        <!-- IE Fix for HTML5 Tags -->
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php
}
if(!is_admin()) {
        add_action('wp_head','html5_shiv');
}

//Sort post order

function post_sort_asc( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_category( array('upcoming-exhibitions', 'traveling-exhibitions') ) ) {
        $query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts', 'post_sort_asc', 1 );

function post_sort_by_end_date( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_category('past-exhibitions') ) {
        $query->set( 'orderby', 'meta_value_num' );
        $query->set( 'meta_key', 'wpcf-end-date' );
        $query->set( 'order', 'DESC' );
        return;
    }
}
add_action( 'pre_get_posts', 'post_sort_by_end_date', 1 );




//Add custom next/prev post links for sort order in Past Exhibitions section
function custom_coin_sort() {

	if(in_category('past-exhibitions')) {

		add_filter('get_previous_post_join', 'rt_post_join');
		add_filter('get_next_post_join', 'rt_post_join');
		add_filter('get_previous_post_where', 'rt_prev_post_where');
		add_filter('get_next_post_where', 'rt_next_post_where');
		add_filter('get_previous_post_sort', 'rt_prev_post_sort');
		add_filter('get_next_post_sort', 'rt_next_post_sort');
	
	function rt_post_join($join, $isc, $ec) {
		global $wpdb;

		$join = " INNER JOIN $wpdb->postmeta AS pm ON pm.post_id = p.ID JOIN wp_cov_7_term_relationships tr on tr.object_id = p.id
       JOIN wp_cov_7_term_taxonomy tt on tt.term_taxonomy_id = tr.term_taxonomy_id
       JOIN wp_cov_7_terms t on t.term_id = tt.term_id";
		return $join;
	}
	
	function rt_prev_post_where($w) {
		global $wpdb, $post;

		$prd = get_post_meta($post->ID, 'wpcf-end-date', true);
		$w = $wpdb->prepare(" WHERE pm.meta_key = 'wpcf-end-date' AND pm.meta_value < '$prd' AND t.slug = 'past-exhibitions'");
		return $w;
	}
	
	function rt_next_post_where($w) {
		global $wpdb, $post;

		$prd = get_post_meta($post->ID, 'wpcf-end-date', true);
		$w = $wpdb->prepare(" WHERE pm.meta_key = 'wpcf-end-date' AND pm.meta_value > '$prd' AND t.slug = 'past-exhibitions'");
		return $w;
	}
		
		function rt_prev_post_sort($o) {
				$o = "ORDER BY pm.meta_value DESC LIMIT 1";
			return $o;
		}
		function rt_next_post_sort($o) {
				$o = "ORDER BY pm.meta_value ASC LIMIT 1";
			return $o;
		}
	}
}

	add_action( 'wp', 'custom_coin_sort', 1 );

function shortcode_include_partial($atts){
	extract(shortcode_atts(array(
	  'partial' => ''
	), $atts));

	ob_start( );
	include( $partial . '.php' );
	$output .= ob_get_clean( );

	return $output;
}
add_shortcode('include_partial', 'shortcode_include_partial');

// function facetwp_get_result_ids( $output, $class ) {

//     $_SESSION["search_result_ids"] = $class->get_filtered_post_ids();

//     $_SESSION["search_result_params"] = "#/";
//     foreach($class->facets as $facet)
//     {
//     	if(!empty($facet["selected_values"]))
//     	{
//     		$_SESSION["search_result_params"] .= $facet["name"] . "=" . implode(",", $facet["selected_values"]) . "&";
//     	}
//     }
//     return $output;
// }
// add_filter( 'facetwp_template_html', 'facetwp_get_result_ids', 10, 2 );

// REMOVE WOOCOMMERCE PRODUCT INFO

global $blog_id;

if ($blog_id == 25) {

	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

}