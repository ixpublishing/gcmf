<?php
/**
 * The template for displaying WooCommerce Pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package gcmf
 */

get_header('woo'); ?>

<div id="content" class="site-content container">
				<?php

					
						if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>

	<div class="row">
	
		<div class="col-md-9">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php woocommerce_content(); ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">
		
			<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>
