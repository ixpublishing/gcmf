<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
		<div class="col-md-4">
		
			<?php
			
			$taxonomy_filter = array('partner-category', 'program-category');
			$tax_terms_filtered = wp_get_object_terms( $post->ID, $taxonomy_filter );
			$parent_term_id = $tax_terms_filtered[0]->parent;
			$parent_taxonomy = $tax_terms_filtered[0]->taxonomy;
			$parent_term = get_term_by('id', $parent_term_id , $parent_taxonomy );
			
			?>
			
			<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				  the_post_thumbnail('full', array('class'=>'img-responsive') );
				} else {
					echo '<img src="' . get_template_directory_uri() . '/img/placeholders/' . $parent_term->slug . '-placeholder.png" class="img-responsive" alt="" />';
				}
					
			?>
			
		</div><!-- .col-md-4 -->
		
		<div class="col-md-8">
		
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
				<a class="btn orange" href="/statewide/request-program/" title="<?php the_title(); ?>">Request Program</a>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
					<?php $args = array(
						//default to current post
						'post' => 0,
						'before' => '<p class="related-items">',
						//this is the default
						'sep' => '<br />',
						'after' => '</p>',
						//this is the default
						'template' => '%s: %l'
						);
						echo the_taxonomies($args);
						//echo get_the_term_list( $post->ID, 'program-category', 'Related Categories: ', ', ', '' );

					//$t = get_the_taxonomies(); print_r($t); 
					?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
	
		</div><!-- .col-md-8 -->
</article><!-- #post-## -->
