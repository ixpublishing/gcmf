<?php
/**
 * The template for displaying pages without a title.
 *
 * Template Name: Default - No Title
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
				<?php

					
						if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>

	<div class="row">
	
		<div class="col-md-9">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-no-title' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">
		
			<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>
