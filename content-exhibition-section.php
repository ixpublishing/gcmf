<?php
/**
 * @package gcmf
 */
?>

<?php 		
//Set Exhibition info variables
	//Location(s)
	$location = types_render_field("gallery-location", array());
	
	//Now
	$now = time();

	//Start Date
	$start_timestamp = (!((get_post_meta($post->ID, 'wpcf-start-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-start-date',TRUE) : '' ;
	$short_start_date = ($start_timestamp == '') ? '' : date("M j, Y", $start_timestamp);
	$xshort_start_date = ($start_timestamp == '') ? '' : date("m/d/y", $start_timestamp);

	//End Date
	$end_timestamp = (!((get_post_meta($post->ID, 'wpcf-end-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-end-date',TRUE) : '' ;
	$short_end_date = ($end_timestamp == '') ? '' : ' &#8211; ' . date("M j, Y", $end_timestamp);
	$xshort_end_date = ($end_timestamp == '') ? '' : ' &#8211; ' . date("m/d/y", $end_timestamp);
	
	//Get all Event Type terms associated with the post to output as classes for isotope filtering
	$terms = get_the_terms( $post->ID, 'event-type' );
	if ( $terms && ! is_wp_error( $terms ) ) : 
	$event_type_list = array();
	foreach ( $terms as $term ) {
		$event_type_list[] = $term->slug;
	}
	$event_types = join( " ", $event_type_list );
	endif;
?>
	<div class="col-md-4 col-sm-6 one-third isotope-item <?php echo $event_types;?>">
		<div class="multi-box-wrapper mb-has-heading">
			<article id="post-<?php the_ID(); ?>" <?php post_class('multi-box'); ?>>
				<div class="mb-image">			
					<?php if ( has_post_thumbnail()) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
						<?php the_post_thumbnail('content-box', array('class' => 'img-responsive')); ?>
						<span class="countdown-overlay"></span>
						</a>
					<?php endif; ?>
				</div>
				<div class="mb-content-wrapper-outer">
					<div class="mb-content-wrapper-inner is-table">
						<div class="table-cell">
							<div class="mb-content">
								<p class="details"><?php echo $location ?></p>
								<p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>
							</div>
						</div>
					</div><!-- .mb-content-wrapper-inner -->
					<div class="container mb-details">
						<div class="row">
							<div class="col-sm-8 col-xs-12">
							<?php
							$exhibition_date = (!($short_end_date == '')) ? ($short_start_date .  $short_end_date) : 'Ongoing'; 
							$xshort_exhibition_date  = (!($short_end_date == '')) ? ($xshort_start_date .  $xshort_end_date) : 'Ongoing';
							?>
								<p class="date details"><?php echo $exhibition_date; ?></p>
							</div>
							<div class="mobile-info col-xs-6">
								<a href="<?php the_permalink(); ?>">View Details</a>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="no-gutter">								
									<?php $btn = admission_button();
										$ticketed_class = ( types_render_field('ticketed-event', array()) == 1 ) ? ' check-schedule' : '' ;
									?>
									<?php if( ($now > $end_timestamp) && ($exhibition_date !== 'Ongoing') ) {
										$btn['button-text'] = "Just Ended";
										$btn['ticket-url'] = get_permalink();
									}?>
									<a class="btn<?php echo $ticketed_class; ?>" href="<?php echo $btn['ticket-url']; ?>"><?php echo $btn['button-text-short']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- .mb-content-wrapper-outer -->
			</article>
		</div>
	</div>