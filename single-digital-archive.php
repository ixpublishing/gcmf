<?php
/**
 * The Template for displaying all single posts.
 *
 * @package gcmf
 */

get_header(); ?>
<!-- <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.cycle/2.99/jquery.cycle.min.js"></script> -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/gcmf.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />

<link rel="stylesheet" href="<?php echo plugins_url() ?>/facetwp/assets/css/front.css" />


<?php
	$hilite = false;
	try {
		$hilite = new SearchWP_Term_Highlight();
	} catch (\Exception $e) {

	}

?>
<div class=''>
<div id="content" class="container site-content">


<div id="landing-page">
	<div class="row">
		<div id="primary" class="col-sm-12 content-area">
		<main id="main" class="row site-main" role="main">
			<div class='col-sm-12'>
				<h1>ONLINE CATALOG SEARCH</h1>
			</div>

			<div class="search-landing-top">
				<div class='col-lg-12'>

					<?php require("inc/_search_result_next_prev.php"); ?>


				</div>
			</div>
			<?php while ( have_posts() ) : the_post();
			//Category of current post
			$category = get_the_category();

			//Get array of all categories
			$allcats = get_all_category_ids();

			//Get the name and ID of the current category (excluding Featured - Home)
			foreach( $category as $cat ) {
				if ( ($cat->category_parent == 0) && ($cat->cat_name != '_Featured - Home') ) {
					$cat_name = $cat->name;
					$cat_id = $cat->term_id;
				}
			}
			//Remove current category ID from array for next/prev post links
			$cat_rm = array_search($cat_id,$allcats);
			unset($allcats[$cat_rm]);
			?>

			<div class='col-sm-12 clearfix'>
				<div class='row'>
				<div class='col-md-9'>
					<div class='clearfix'>
						<div class="item-thumbnail pull-left col-md-4 col-lg-4">
							<?php
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							  the_post_thumbnail();
							} else
							{
								?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/GCMF-placeholder.jpg" />
								<?php
							}
							?>
						</div>
						<div class="col-md-8">
							<div class='rowx'>
								<div >
									<?php
									$format = types_render_field( "format", array( "separator" => ", " ) );
									$format_url = preg_replace("#[[:punct:]]#", "", str_replace(" ", "-", strtolower($format)));
									echo ($format) ? "<span class='format-icon format-".$format_url."'>$format</span><br>" : "" ?>

									<h2><!--a href="<?php the_permalink(); ?>"--><?php echo the_title(); ?><!--/a--><?php echo !empty($item_date) ? (", " . $item_date) : "" ?></h2>
									<div class='year'><?php echo types_render_field("year"); ?></div>
									<?php if (!((get_post_meta($post->ID, 'wpcf-date-range', TRUE))=='')) : ?><span class="result-text"><?php echo types_render_field("date-range");  ?></span><br>
									<?php endif; ?>
								   <?php echo ($download_link = types_render_field("pdf-attachment", array("output" => "raw"))) ? "<span class='download-link'><a href='$download_link'>Download</a></span><br>" : "" ?>
								</div>

									<?php if (!((get_post_meta($post->ID, 'wpcf-creator', TRUE))=='')) : ?>
									<div><span class="result-type">Author:</span> 
									<?php echo types_render_field("creator"); ?></div>
									<?php endif; ?>
									<?php if (!((get_post_meta($post->ID, 'wpcf-publisher', TRUE))=='')) : ?>
										<div><span class="result-type">Publisher:</span>
										<?php echo types_render_field("publisher", array()); ?>
										</div>
									<?php endif; ?>

									<?php if (!((get_post_meta($post->ID, 'wpcf-date', TRUE))=='')) : ?>
										<div><span class="result-type">Date:</span>
										<?php echo types_render_field("date", array()); ?>
										</div>
									<?php endif; ?>

							</div>

							<hr>
							<div >
								<?php if (!((get_post_meta($post->ID, 'wpcf-physical-description', TRUE))=='')) : ?>
									<div><span class="result-type">Physical Description:</span>
								<?php echo types_render_field("physical-description", array()); ?>
									</div>
										<?php endif; ?>

							</div>

							<div>
							<?php if (types_render_field("subject")!='') : ?>
								<div>
										<span class="result-type">Subject:</span>

										<?php echo types_render_field("subject", array('separator'=>", ")); ?>
										<br>
										<?php echo types_render_field("subject-text", array()); ?>
									</div>
										<?php endif; ?>

							</div>
							<div>
							<?php $col_titles = get_the_terms($post->ID, 'collection'); ?>
							<?php if (!empty($col_titles)) { ?>
							<span class="result-type">Collection: </span><?php echo the_terms($post->ID, 'collection'); ?></span>
							<br>
							<br>
							<?php } ?>
							</div>
							<div class="tags">
								<?php
								$posttags = get_the_tags();
								if ($posttags) {
								  foreach($posttags as $tag) {

								  	echo '<a href="/library/results/#!/keyword_search=' . $tag->name . '">' . $tag->name .'</a>, ';

								  }
								}
								?>
							</div>
							<br>

						</div>
					</div>

					<br>
					<div class='clearfix '>
						<div class="s-tab-holder col-lg-12">
						<div class="search-tabs-wrapper" >
							<ul class="s-tabs" class="s-tabs">
								<?php if( $content = $post->post_content ): ?>
								<li><a href="#s-tabs1">Summary</a></li>
								<?php endif; ?>
								<?php
									$related_items = get_field('related_items');
									?>
								<?php if( $related_items ): ?>
								<li><a href="#s-tabs2">Related</a></li>
								<?php endif; ?>
							</ul>
							<div class="s-tab-box s-tabs-container">
								 <div id="s-tabs1" class="s-tab s-tab-content">
						            <?php the_content(); ?>
						        </div>
						        <div id="s-tabs2" class="s-tab s-tab-content">
						        	<div id="related-items-carousel" class="carousel slide">

									<div class="carousel-inner">

										<h4>Related Items</h4>
										<?php if(!empty($related_media)): ?><h2>Related Items</h2><li><a href="#tab2"></a></li></div><?php endif; ?>

										<?php foreach( $related_items as $related_item ): ?>
											<div class='item col-md-3'>
												<a href="<?php echo get_permalink( $related_item->ID ); ?>">
													<div class='related-item-image'>
													<?php
													if ( has_post_thumbnail($related_item->ID) ) { // check if the post has a Post Thumbnail assigned to it.
													  echo get_the_post_thumbnail($related_item->ID);
													} else
													{
														?>
														<img class='clearfix' src="http://placehold.it/100x150/046598" />
														<?php
													}
													?>
													</div>
													<div class='related-item-title'>
														<?php echo get_the_title( $related_item->ID ); ?>
													</div>

												</a>
												<div class='related-item-creator'>
													<?php echo get_post_meta($related_item->ID, 'wpcf-creator', true);//types_render_field("creator", array("post_id"=>$related_item->ID)) ?>
												</div>
											</div>
										<?php endforeach; ?>

									</div>

									<!-- Controls -->
									<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left"></span>
									</a>
									<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
						<?php endwhile; // end of the loop. ?>
						        </div>
							</div>
						</div>
					</div>
					<?php include (get_stylesheet_directory() . "/share_this_include.php"); ?>
					</div>

				</div>

				<div class='search-highlight col-sm-3 row'>
					<?php $file_attach = types_render_field("file-attachment", array("output" => "raw"));
							$file_attach2 = types_render_field("file2-attachment", array("output" => "raw"));
							$file_attach3 = types_render_field("file3-attachment", array("output" => "raw"));
							$file_attach4 = types_render_field("file4-attachment", array("output" => "raw"));
							$file_attach5 = types_render_field("file5-attachment", array("output" => "raw"));
					?>
					<?php
						$related_digital_archives = get_field('related_digital_archives');
						?>
						<?php if ($file_attach !=='' || $file_attach2 !=='' || $file_attach3 !=='' || $file_attach4 !=='' || $file_attach5 !=='' || (!empty($related_digital_archives)) )  { ?>
						<h3>Digital Downloads</h3>
						<?php foreach( $related_digital_archives as $related_digital_archive ): ?>
						<a href="<?php echo get_permalink( $related_digital_archive->ID ); ?>" target="_blank"><?php echo get_the_title( $related_digital_archive->ID ); ?>&#187<br>
						<?php endforeach; ?>
						<!--span class="result-text"><?php /* echo types_render_field("file-attachment", array('separator'=>", ")); */?></span>-->
						<span class="result-text"><a href="<?php echo $file_attach ?>" target="_blank"><?php echo types_render_field("file-title"); ?></a></span><br>
						<span class="result-text"><a href="<?php echo $file_attach2 ?>" target="_blank"><?php echo types_render_field("file2-title"); ?></a></span><br>
						<span class="result-text"><a href="<?php echo $file_attach3 ?>" target="_blank"><?php echo types_render_field("file3-title"); ?></a></span><br>
						<span class="result-text"><a href="<?php echo $file_attach4 ?>" target="_blank"><?php echo types_render_field("file4-title"); ?></a></span><br>
						<span class="result-text"><a href="<?php echo $file_attach5 ?>" target="_blank"><?php echo types_render_field("file5-title"); ?></a></span>
						<br>
						<?php } ?>
						<?php $col_titles = get_the_terms($post->ID, 'collection'); ?>
						<?php if (!empty($col_titles)) { ?>
					<h3>Collection</h3>
					<?php echo the_terms($post->ID, 'collection'); ?>
					<br>
					<?php } ?>
					<a href="/library/services/"><p class="center"><input type="submit" value="Request Library Services &#187" class="btn btn-services "></p></a>
					<?php if (types_render_field("location")): ?>
						<span class="result-type">Location: </span><span class="result-text"><?php echo ($loc = types_render_field("location")) ? $loc . "<br>" : ""; // echo ($location = get_post_meta($post->ID,'wpcf-location',true)) ? "$location" : "" ?></span>
					<?php endif; ?>
					<?php if (!((get_post_meta($post->ID, 'wpcf-call-number', TRUE))=='')) : ?>
						<span class="result-type">Call No. </span><span class="result-text"><?php echo types_render_field("call-number");  ?></span>
						<br>
					<?php endif; ?>
					<?php if (!((get_post_meta($post->ID, 'wpcf-holding-rights', TRUE))=='')) : ?><br>
						<span class="result-type">Holding Rights: </span><span class="result-text"><?php echo types_render_field("holding-rights");  ?></span><br>
					<?php endif; ?>
					<?php if (!((get_post_meta($post->ID, 'wpcf-id', TRUE))=='')) : ?>
						<span class="result-type">Holding ID: </span><span class="result-text"><?php echo types_render_field("id");  ?></span><br>
					<?php endif; ?>
					<?php if (!((get_post_meta($post->ID, 'wpcf-rights', TRUE))=='')) : ?><br>
						<span class="result-type">Rights: </span><span class="result-text"><?php echo types_render_field("rights");  ?></span><br>
					<?php endif; ?>
					<?php if (types_render_field("lang")!='') : ?><br>
						<span class="result-type">Language: </span><span class="result-text-language"><?php echo types_render_field("lang", array('separator'=>", ")); ?></span><br>
					<?php endif; ?>
				</div>
				</div>
			</div>
			<hr>
				<div class='col-lg-12'>
					<?php require("inc/_search_result_next_prev.php"); ?>
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	</div> <!-- .row -->


</div><!-- #content -->
</div>
</div>
<?php get_footer(); ?>
