<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'gcmf'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	global $blog_id;
	
	$site_colors = array(
		'' => __('Default', 'gcmf'),
		'#0064a2' => __('Marshall Blue', 'gcmf'),
		'#00aee6' => __('Marshall Light Blue', 'gcmf'),
		'#1a808e' => __('Marshall Green', 'gcmf'),
		'#5a5a5a' => __('Marshall Gray', 'gcmf'),
	);

	$options = array();

	$options[] = array(
		'name' => __('Basic Settings', 'gcmf'),
		'type' => 'heading');

	if ( !($blog_id == 1) ) {
	
	$options[] = array(
		'name' => __('Section Color', 'gcmf'),
		'desc' => __('Select the accent color for this area of the site.', 'gcmf'),
		'id' => 'section_color',
		'std' => '',
		'type' => 'select',
		'options' => $site_colors );

	$options[] = array(
		'name' => __('Priority Link Text', 'gcmf'),
		'desc' => __('Enter text for the main link that appears at the right hand side of the section navigation.', 'gcmf'),
		'id' => 'priority_link_text',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'name' => __('Priority Link URL', 'gcmf'),
		'desc' => __('Enter URL for Priority Link. For site links, use relative paths (/section_name/page/), for external links, use full url (http://www.sitename.com/path/).', 'gcmf'),
		'id' => 'priority_link_url',
		'std' => '',
		'type' => 'text');
	} else {
		

	$options[] = array(
		'name' => __('Facebook URL', 'gcmf'),
		'id' => 'url_facebook',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Twitter URL', 'gcmf'),
		'id' => 'url_twitter',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('YouTube URL', 'gcmf'),
		'id' => 'url_youtube',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('GooglePlus URL', 'gcmf'),
		'id' => 'url_gplus',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Instagram URL', 'gcmf'),
		'id' => 'url_instagram',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Flickr URL', 'gcmf'),
		'id' => 'url_flickr',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Pinterest URL', 'gcmf'),
		'id' => 'url_pinterest',
		'type' => 'text');
		

		$options[] = array(
		'name' => __('LinkedIn URL', 'gcmf'),
		'id' => 'url_linkedin',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('RSS URL', 'gcmf'),
		'id' => 'url_rss',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('iTunes URL', 'gcmf'),
		'id' => 'url_itunes',
		'type' => 'text');
	}

	return $options;
}