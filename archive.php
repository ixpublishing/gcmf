<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div class="row">

		<div class="col-md-9">

			<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
						//Compile list of CPTs
						$args = array(
						   'public'   => true,
						   '_builtin' => false
						);
						$custom_post_types = get_post_types( $args );

						if ( is_category() ) :
							single_cat_title();

						//Use CPT list to get archive title
						elseif ( is_post_type_archive( $custom_post_types ) ) :
							post_type_archive_title();

						//Use CPT list to get archive title
						elseif ( is_tax() ) :
							single_term_title();

						else :
							_e( 'Archives', 'gcmf' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					//$term_description = term_description();
					//if ( ! empty( $term_description ) ) :
						//printf( '<div class="taxonomy-description">%s</div>', $term_description );
					//endif;
				?>
			</header><!-- .page-header -->
			<?php echo paginate('top'); ?>

			<?php global $blog_id; ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					 if ( $blog_id == 3 ) :
							get_template_part( 'content', 'news' );

					else:

					get_template_part( 'content', get_post_format() );
					endif;
				?>

			<?php endwhile; ?>



		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		<?php echo paginate('bottom'); ?>
		</main><!-- #main -->
	</section><!-- #primary -->

		</div><!-- .col-md-9 -->


		<div class="col-md-3">

			<?php get_sidebar(); ?>

		</div><!-- .col-md-3 -->

	</div> <!-- .row -->

</div><!-- #content -->

<?php get_footer(); ?>
