<?php
/**
 * The template for redirecting sites to an alternative home page.
 *
 * Template Name: Redirect
 *
 * @package gcmf
 */
$location = get_site_url() . '/at-the-museum/'; 
wp_redirect($location);
exit;

?>