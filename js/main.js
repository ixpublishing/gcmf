jQuery(document).ready(function($) {
	var alertFallback = false;
	if (typeof console === "undefined" || typeof console.log === "undefined") {
		console = {};
		if (alertFallback) {
			console.log = function(msg) {
				alert(msg);
			};
		} else {
			console.log = function() {};
		}
	}

	
	//Set Height of priority link for vertical centering
	var addAlert = function(){
		var stickyHero, masthead,
			alertBarHeight,
			headerTop,
			win;
	
		if($('#alertbar-area').length > 0) {
			stickyHero = $('#sticky-hero');
			masthead = $('#masthead');
			//remove any css that has been dynamically set so it doesn't get repeated in calculations
			stickyHero.removeAttr('style');
			
			alertBarHeight = $('#alertbar-area').outerHeight();
			headerTop = stickyHero.css('margin-top');
			win = $(window).width();
			if ( win >= 768 ) {
				masthead.not('.home-mobile #masthead').css('top', alertBarHeight);
				stickyHero.css('margin-top', parseInt(headerTop) + alertBarHeight);
			} else {
				masthead.removeAttr('style');
				stickyHero.css('margin-top', parseInt(headerTop) );
			}
		}
	};

	// run our function on load
	addAlert();

	// and run it again every time window is resized
	$(window).on('resize', function() {
		 addAlert();
	});

	//JS Sniffer
	document.documentElement.className = document.documentElement.className.replace("no-js","js");

	var mhContainer = $('.entry-content .mb-inner-content, .entry-content .inner-content').not('.featured-slider .inner-content');
	if (mhContainer.length>0) {
		mhContainer.matchHeight();
	}
	
	// Tabs
	//When page loads...
	$('.tabs-wrapper').each(function() {
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("active").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	});
	
	//On Click Event
	$("ul.tabs li").click(function(e) {
		$(this).parents('.tabs-wrapper').find("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(this).parents('.tabs-wrapper').find(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content

		 var activate = $(this).parents('.tabs-wrapper').find(activeTab);
		 activate.fadeIn(); //Fade in the active ID content
		 
		 //See if tabs contain a royalslider
		 var refreshSlider = activate.find('.royalSlider');
		 if(refreshSlider.length) {
			$(refreshSlider).royalSlider('updateSliderSize', true);
		 }
		
		e.preventDefault();
	});
	
	$("ul.tabs li a").click(function(e) {
		e.preventDefault();
	});

	$('.tabset').each(function() {
		var menuWidth = 100;
	    var menuItems = $(this).find('li').size();
	    var itemWidth = (menuWidth/menuItems);

	    $(this).css({'width': menuWidth +'%'});
	    $(this).find('li').css({'width': itemWidth +'%'});
	});
	
	
	 setTimeout(function() {
		var slider = $('#new-royalslider-1');
		var container = $('.nav-bullets');
		slider.find('.rsNav').appendTo(container);
	}, 1);
	
	$('.featured-slider').each(function(){
		var nav = $(this).find('.rsNav');
		if ($(this).data('royalSlider').numSlides <= 1) {
			nav.hide();
		}
	});


	/*
	// TC - 5-20 - Are we using this?
	//Open tab from an external link based on URL hash value (exclude collections search pages)
    if(document.location.href.indexOf("collections/results") < 0) {
    	try
    	{
			setTimeout(function() {
				  if (location.hash) {
				  	window.scrollTo(0, 0);
					var hash = location.hash;
					$('a[href='+hash+']').parent().siblings().removeClass('active').end().addClass('active').trigger( 'click' );
				  }
			}, 1);
		} catch(ex) {}
	}
	*/
	
	//ISOTOPE
	
	// cache container
	var $container = $('#isotope-container'),
		win = $(window).width();
	;
	// initialize isotope
	
	if ( ($container.length > 0) && (win >= 768) ) {
	
		$container.isotope({
		  // options...
		  layoutMode: 'sloppyMasonry',
		  itemSelector : '.isotope-item'
		});
	
	}

	// filter items when filter link is clicked
	$('.isotope-filter a, .sort-filter a').click(function(){
	  $(this).parent().siblings().removeClass('active').end().addClass('active');
	  var selector = $(this).attr('data-filter');
	  $container.isotope({ filter: selector });
	  return false;
	  
	});
	
	function isotope_mobile_check() {
		var win,
			$container = $('#isotope-container');
		win = $(window).width();
		if ( ($container.length > 0) && ($container.hasClass('isotope') ) ) {
			if( win < 768){
				$container.isotope('destroy');
			} else {
				$container.isotope();
			}
		}
	
	}

		// run our function on load
	isotope_mobile_check();

	// and run it again every time window is resized
	$(window).on('resize', function() {
		 isotope_mobile_check();
	});

	
/*
//Create submenu as a "child" in full width container to display as mega-menu
	$('#menu-primary-navigation .sub-menu-level-0').each(function( i ) {
		$(this).appendTo('#hero')
		.wrap("<div id='primary-nav-sub-menu-" + i + "' class='primary-nav-sub-menu'><div class='container'></div></div>")
		.addClass('list-inline');
	});
	
//Open "child" menu when hovering over main navigation parent	
	$('[class*="primary-nav-parent"] a').hover(
		function(){
		var i = $(this).parent().attr('class').match(/\d/);
		$('#primary-nav-sub-menu-' + i).stop(true, true).fadeIn(200);
		},
		function () {
			var i = $(this).parent().attr('class').match(/\d/);
			$('#primary-nav-sub-menu-' + i).stop(true, true).delay(2000).fadeOut(200);
		}
	);
//Keep submenus active while hovering over "child"
	$('[id*="primary-nav-sub-menu"]').hover(
		function(){
		$(this).stop(true, true).show();
		},
		function(){
		$(this).stop(true, true).fadeOut(200);
		}
		
	);*/
	/*
	$( ".pl-prev" ).hover(function() {
		$( '.pl-prev-thumb' ).stop( true, true ).fadeIn(200);
		}, function() {
		$( '.pl-prev-thumb' ).stop( true, true ).fadeOut(200);
		});
	
	$( ".pl-next" ).hover(function() {
		$( '.pl-next-thumb' ).stop( true, true ).fadeIn(200);
		}, function() {
		$( '.pl-next-thumb' ).stop( true, true ).fadeOut(200);
		});
	*/


	// Search Tabs
	//When page loads...
	$('.search-tabs-wrapper').each(function() {
		$(this).find(".s-tab-content").hide(); //Hide all content
		$(this).find("ul.s-tabs li:first").addClass("active").show(); //Activate first tab
		$(this).find(".s-tab-content:first").show(); //Show first tab content
	});
	
	//On Click Event
	$("ul.s-tabs li").click(function(e) {
		$(this).parents('.search-tabs-wrapper').find("ul.s-tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(this).parents('.search-tabs-wrapper').find(".s-tab-content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content

		var activate = $(this).parents('.search-tabs-wrapper').find(activeTab);
		activate.fadeIn(); //Fade in the active ID content
	});
	
	$("ul.s-tabs li a").click(function(e) {
		e.preventDefault();
	});


	//Add active class to exhibitions dropdown filters
	$(".exhibition-year-sort-filter .has-children ul li a").each(function() {   
		if (this.href === window.location.href) {
			$(this).parent().addClass("active");
		}
	});
	
	$('.dropdown a').click(function(e){
		$('.dropdown-trigger > a').text($(this).text());
	});

	//Set Height of priority link for vertical centering
	var priority_link_height = function(){
		var section_nav_height =  $('#menu-secondary-navigation').outerHeight();
		$('div.priority-link').css({ 'height': section_nav_height });
	};

	// run our function on load
	priority_link_height();

	// and run it again every time window is resized
	$(window).on('resize', function() {
		 priority_link_height();
	});
	
	//Truncate text
	function shorten(text, maxLength) {
    var ret = text;
    if (ret.length > maxLength) {
        ret = ret.substr(0,maxLength-3) + "...";
    }
		return ret;
	}
	
	function shorten_box_text(query) {	
		$(query).each(function(){
			var $this = $(this);
			var t = $this.text();
			var str = shorten(t,45);
			$this.text(str);
		});	
	}
	
	if ( document.body.clientWidth < 975 ) {
        shorten_box_text('.col-sm-4 .inner-content a, .col-sm-3 .inner-content a');
	}
	
	//Add priority link as first element of mobile dropdown menu
	var cta = $('.priority-link').html();
	if(cta){
		$('#mobile-secondary-navigation').prepend('<li class="mobile-cta">' + cta + '</li>');
	}
	
	//Toggle dropdown mobile secondary navigation
	$('#mobile-menu .secondary-menu-toggle').on('click', function() {
		$(this).toggleClass('open');
	});
	
	//Toggle alert message visibility
	$('.alertbar_toggle').on('click', function(){
		$('#alertbar').slideToggle();
		$(this).toggleClass('open');
	});
	
	//Function to bind to window resize events
	 $(window).resize(function() {
        if(this.resizeTO) { clearTimeout(this.resizeTO); }
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 250);
    });
	
	
	$('#site-search-trigger').on('click', function(){
		var searchWrapper = $('#masthead .global-search-form-wrapper');
		searchWrapper.toggleClass('expanded');
		var win = $(window).width();
		$(this).toggleClass('focused');
		if (win >= 768) {
			$('#site-navigation').toggleClass('fade');
		}
	});
	
});