// if(window.location.href.indexOf("collections") < 0)
// {
// 	jQuery('#logo, h1, h2, h3, a, li, span').hover(function()
// {
// 	    jQuery(this).css("position", "absolute").css('top', (Math.random() * (jQuery(window).height() - 111)) + 'px').css('left', (Math.random()*(jQuery(window).width() - 127)) + 'px');
// 	});	
// }


//
(function($) {

	$(document).ready(function() {
		var slider = $('.collection-slider').royalSlider ({
			autoScaleSlider:true,
			autoScaleSliderWidth:665,
			autoScaleSliderHeight:400,
			// imageScalePadding:0,
			// slidesSpacing:0,
			 // imgWidth:700,
			 // imageScaleMode:'fit',
			 //  imgHeight:400,
			  navigateByClick: false,
			loop:true,
			globalCaption:true,
			controlNavigation:'thumbnails',
		}).data('royalSlider');

		try
		{
			slider.ev.on('rsAfterSlideChange', function(event) {
				// triggers after slide change
				// console.log("Slide change");
				// Update the Download link to point to this asset.
				$("#asset-download-link").attr("href", event.target.currSlide.content.find("img").attr("src"));
			});
		} catch(ex) {}

	});
	
    $('.collection-search-main .btn-search').click(function() { FWP.refresh(); })

    // If the Mobile search is clicked, need to see what category and collections were selected and filter on those, as well as pass in the keyword.
    $("#mobile-search-options input.btn-search").click(function() {
    	var keywords 		= $("#mobile-search-options input[type=text]").val();
    	var category_id 	= $("#mobile-search-options .category-dropdown").val();
    	var collection_id 	= $("#mobile-search-options .collection-dropdown").val();

    	var search_url = "";// "/collections/results/#/";
    	if(keywords != '')
    	{
    		search_url += "&keyword_search=" + keywords;
    	}

    	if(category_id)
    	{
    		//FWP.facets.categories = [category_id];
    		search_url += "&categories=" + category_id;
    	}

    	if(collection_id)
    	{
    		search_url += "&collection=" + collection_id;
    	}

    	// // console.log(search_url);
    	// // console.log(FWP);

    	window.location.hash = search_url;
    	location.reload();
    });

	$("#show-sidebar").click(function() {
		//$("#search-sidebar").toggle();
		if($("#search-sidebar.visible").length > 0) {
			// console.log("hide");
			$("#search-sidebar").removeClass("visible");//.css({marginLeft: '-25%' }) //"slide", { direction: "right"}, 500);//, 1000);
			$(this).html("<i class='fa fa-arrow-circle-o-right'></i> Show Advanced Search");
		}else{
			$("#search-sidebar").addClass("visible");//.css({marginLeft: '0' }) //"slide", { direction: "right"}, 500);//, 1000);
			$(this).html("<i class='fa fa-arrow-circle-o-left'></i> Hide Advanced Search");
		}
		return false;
	});
	
	// Zoom details images.
	$('.collections-slider .ss-image .focus-item > img, .collections-slider .media .image-box > img').elevateZoom({
		zoomType	: "lens",
		lensShape	: "round",
		lensSize	: 300
	}); 
	//.zoom();
	//$('#ex1').zoom();


	//jQuery(document).ready(function($) {
	// collection search scripts

	$(".collection-search-main")
		.prepend("<input type='hidden' name='wpv_column_sort_id' value='wpcf-artist' />")
		.prepend("<input type='hidden' name='wpv_column_orderby' value='date' />")
		.prepend("<input type='hidden' name='wpv_column_sort_dir' value='ASC' />");

		//.prepend("<input type='hidden' name='wpv_column_orderby' value='meta_value' />")

	// Init grid size from cookie
	var gridSize = "large";
	if(docCookies.hasItem("gridSize"))
	{
		gridSize = docCookies.getItem("gridSize");
	}

	// Check to see if we're in the bootstrap XS
    var on_bootstrap_xs = $(".hidden-xs").first().is(":hidden");
    var on_bootstrap_sm = false;
    if(!on_bootstrap_xs)
    {
    	on_bootstrap_sm = $(".hidden-sm").first().is(":hidden");
    }

    if(on_bootstrap_xs || on_bootstrap_sm)
    {
    	// console.log("on_bootstrap_xs or -sm");
    	docCookies.setItem("gridSize", "small");
    	gridSize = "small";
    }
    else
    {

    }

	$(".collection-search-main").first().prepend("<input type='hidden' name='grid_size' value='"+ gridSize +"' />");
	$("#content").removeClass("small-grid large-grid").addClass(gridSize + "-grid");


	// Generate placeholder text
	$(".field-wrapper > input[type=text]").each(function() {
		var label = $(this).siblings("label").text();

		$(this).attr("placeholder", "TYPE " + label.toUpperCase());
	});


	$(".date-range-search").each(function(ix, el) 
	{
		// from a min and a max, need to create a control with one date field and a menu that lets you select
		// before, after, exactly.

		// hide existing inputs
		$(el).find("input").hide();

		// Add my menu element
		$(el).append("<select class='date-range-when'><option selected value=''>WHEN</option><option value='BEFORE'>Before</option><option value='AFTER'>After</option><option value='EXACTLY'>Exactly</option></select>");
			//<div class='dropdown'><div class='default'>WHEN</div><ol><li>Before</li><li>After</li><li>Exactly</li></ol>");

		// Are there param values? Get 'em, populate the new input and menu.
		// They're re-pop'd automatically into the inputs, so we can get them from there.
		var minField = $(el).find("[name=object-date_min]").first().val();
		var maxField = $(el).find("[name=object-date_max]").first().val();
		var curVal = minField || maxField || "";
		//// console.log(minField, maxField);
		//// console.log(minField && !maxField ? "AFTER" : maxField && !minField ? "BEFORE" : "EXACTLY");

		//// console.log($(el).find('.date-range-when').first());

		//debugger
		$(el).find('.date-range-when').first().val(minField && !maxField ? "AFTER" : maxField && !minField ? "BEFORE" : "EXACTLY");

		// Add new input element
		$(el).append("<input type='text' placeholder='TYPE YEAR' class='search-range-date-field' value='"+ curVal +"'  />");
	});

	$(".dropdown li").click(function() {
		$(this).siblings("li").removeClass("selected");

		$(this).addClass("selected");
	});


	$(".date-range-when").change(function() 
	{
		// when the after/before/exactly selection changes, need to update the text fields..
		var when = $(this).val();

		if($.trim(when) == "")
		{
			when = "EXACTLY";
		}

		var inputDate = $(this).siblings(".search-range-date-field").val();

		updateDateRangeField(this, inputDate, when);
	});

	$(".search-range-date-field").on("keyup", function() 
	{
		// Get value, update the sibling inputs based on the menu option and the input value
		var inputDate = $(this).val();

		// I can parse hand-entered date to a date object, but looks like these will be stored just as a YYYY string
		// inputDate = Date.parse(inputDate);
		//// console.log(inputDate);

		var when = $(this).siblings(".date-range-when").val();

		if($.trim(when) == "")
		{
			when = "EXACTLY";
		}

		//// console.log("W:"+when+"/W");

		updateDateRangeField(this, inputDate, when);
	});

	function updateDateRangeField(el, inputDate, when)
	{
		var minField = $(el).siblings("[name=object-date_min]").first();
		var maxField = $(el).siblings("[name=object-date_max]").first();

		switch(when.toUpperCase()) {
			case "BEFORE":
			//// console.log("BEFORE");
			minField.val(null);
			maxField.val( inputDate );
			break;
			case "AFTER":
			//// console.log("AFTER");
			maxField.val(null);
			minField.val( inputDate );
			break;
			case "EXACTLY":
			//// console.log("EXACTLY");
			minField.val( inputDate );
			maxField.val( inputDate );
			break;
		}
	}

	// Create tab functionality
	$(".search-tab-section").click(function()
	{
		var section = $($(this).attr("href"));

		if(section.is(":hidden"))
		{
			// hide all sections
			//$("#collection-filter-menu").hide();

			$(".search-tab-section").removeClass("expanded");
			$(this).addClass("expanded");

			// show selected section
			section.slideDown('fast');
		}
		else
		{
			$(this).removeClass("expanded");
			section.slideUp('fast');
		}

		return false;
	});


	$(".multi-select").each(function(ix, el) 
	{
		// Wrap .form-items
		$(el).find(".form-item").wrapAll("<div class='multi-select-wrapper' />");
	});


	$(".multi-select > label").click(function() {
		if(!$(this).parents(".multi-select").hasClass("expanded"))
		{

			$(".multi-select-wrapper").hide();
			$(".multi-select").removeClass("expanded");

			//// console.log("label click");

		

			$(this).parents(".multi-select")
				.addClass("expanded")
				.find(".multi-select-wrapper")
				.show();
		}
		else
		{
			//$(this).hide();
			$(".multi-select-wrapper").hide();
			$(".multi-select").removeClass("expanded");
		}		
	});


	$(".wpv-filter-form").on("click", "input[type='reset']", function(e){
	  e.preventDefault();
	  $(".wpv-filter-form")[0].reset();  

	  $(".multi-select .form-item-checkbox input:checkbox").each(function(ix, el) {
		$(this).triggerHandler('change');
		})
	});


	$(".wpv-filter-form input").blur(function() {
		//updateCurrentSearchTerms();
	});

	// When click an item in a multi select, update, the count shown above
	$(".multi-select .form-item-checkbox input:checkbox").change(function(ix, el) 
	{
		var parent = $(this).parents(".form-item-checkbox");

		if($(this).is(":checked"))
			$(parent).addClass("checked");
		else
		{
			$(parent).removeClass("checked");
		}

		//Update count
		var multiSel = $(this).parents(".multi-select");
		updateSelCount(multiSel);
		//updateCurrentSearchTerms();
	});

	
	$(".wpv-filter-form .multi-select").each(function(ix, el) 
	{
		var multiSel = $(el);
		updateSelCount(multiSel);
	});
	
	function updateSelCount(multiSel)
	{
		
		var numChecked = multiSel.find(":checked").length;
		if(multiSel.find(".count").length > 0)
			multiSel.find(".count").html(numChecked);
		else
		{
			multiSel.find("> label").append("<div class='count'>"+ numChecked +"</div>");
		}

		if(numChecked == 0) multiSel.find(".count").remove();

		// Make sure filter menu is showing if any are checked
		if(numChecked > 0)
		{
			$("#collection-filter-menu").show();
		}
		
	}


	// Click on a selection item
    $(document).on('click', '.facetwp-selections li', function() {
        var $this = $(this);
        var facet_name = $this.attr('data-facet');
        var facet_value = $this.attr('data-value');
        var facet_type = $('.facetwp-facet-' + facet_name).attr('data-type');
        // console.log(facet_type);
        if ('search' == facet_type) {
        	// console.log(facet_name);
            $('.facetwp-facet-' + facet_name).find('input[value="'+ facet_value +'"]').val("");
            $(".collection-search-main input[type=submit].btn-search").click();
        }
       	else if('number_range' == facet_type)
       	{
       		$('.facetwp-facet-' + facet_name).find('input[value="'+ facet_value +'"]').val("").blur();
            //$(".collection-search-main input[type=submit].btn-search").click();
       	}

        
    });

	function updateCurrentSearchTerms()
	{
		// Need to get the input values -- keywords and date range
		var selections_display = $(".facetwp-selections"); //("<div class='facetwp-selections'><ul></ul></div>").insertAfter(
		
		if($(selections_display).find("> ul").length <= 0)
		{
			$("<ul></ul>").appendTo(selections_display);
		}
		selections_display = $(selections_display).find("> ul");

		// keywords
		$(".collection-search-main .facetwp-facet-keyword_search input[type=text]").each(function(ix, el) {
			if($(this).val() != "")
			{
				$(selections_display).find("li[data-facet='keyword_search']").remove();
				
				// Add input value to selections
				$(selections_display).append('<li data-facet="keyword_search" data-value="'+ $(this).val() +'"><span>Keywords: '+ $(this).val() +'</span><span class="facetwp-remove-selection"></span></li>');
			}
		});

		// date range
		$(".collection-search-main .facetwp-facet-date_range input[type=text]").each(function(ix, el) {
			// console.log("date range");
			if($(this).val() != "")
			{
				$(selections_display).find("li[data-facet='date_range']").remove();

				if($(this).hasClass("facetwp-number-min"))
				{
					// Add input value to selections
					$(selections_display).append(
						'<li data-facet="date_range" data-value="'+ $(this).val() +'">' +
							'<span>Min: '+ $(this).val() +'</span>						  ' +
							'<span class="facetwp-remove-selection"></span>				  ' +
						'</li>');
				} else {
					// Add input value to selections
					$(selections_display).append(
						'<li data-facet="date_range" data-value="'+ $(this).val() +'">' +
							'<span>Max: '+ $(this).val() +'</span>						  ' +
							'<span class="facetwp-remove-selection"></span>				  ' +
						'</li>');
				}
			} 
		});
		
	}

	updateCurrentSearchTerms();


	$("article").on("click", ".remove-search-term", function() 
	{
		var origEl = $("#" + $(this).attr("data-original-el"));

		$(origEl).not(':button, :submit, :reset')
			.removeAttr('checked')
			.removeAttr('selected')
			.not(':checkbox, :radio, select')
			.val('');

		$(origEl).blur().change();
		//updateCurrentSearchTerms();
		$(".wpv-filter-form").first().submit();

	});
	

	// Inelegant string parsing to make search result titles show better
	// Have to do this on mouseenter, since loading the results with ajax
	$(".entry-content").on("mouseenter", ".collection-search-result", function() 
	{
		$(".item-details .artist:not(.formatted)").each(function(ix, el)
		{
			// Trim title to end after first parenthesis
			var first_paren = $(el).html().indexOf(")");
			if(first_paren >= 0)
			{
				$(el).html(
					$(el).html().substr(0, 
						first_paren + 1
					)
				);

				$(el).html($(el).html().replace(/\(/g, "<span class='paren-start'>(").replace(/\)/g, ")</span>")).addClass("formatted");

			}
		});
	});
	

	$(".field-wrapper select").addClass("selectpicker");
	$('.selectpicker').selectBoxIt(
		{
			showFirstOption: false
		});

	// When facetwp is about to refresh, make sure sorting param is picked up.
	$(document).on('facetwp-refresh', function() {
		// Hide sidebar
		//$("#search-sidebar").hide();

		// Apply sorting params
		FWP_HTTP.get['orderby'] = "date"; //"meta_value";
		FWP_HTTP.get['meta_key'] = "wpcf-artist";
		FWP_HTTP.get['order'] = "ASC";

		var custom_orderby = $(".collection-search-main input[name='wpv_column_orderby']").first();
		if(custom_orderby.val())
		{
			FWP_HTTP.get['orderby'] = custom_orderby.val();
		}

		var custom_sort = $(".collection-search-main input[name='wpv_column_sort_id']").first();
		if(custom_sort.val())
		{
			FWP_HTTP.get['meta_key'] = custom_sort.val();
		}

		var custom_sort_dir = $(".collection-search-main input[name='wpv_column_sort_dir']").first();
		if(custom_sort_dir)
		{
			FWP_HTTP.get['order'] = custom_sort_dir.val();
		}
		//FWP.jqXHR.complete(function() {// console.log("complete");});

		// console.log(FWP);
	});


	$(".collection-search-main").on("change", "select.sort-menu", function() 
	{
		//// console.log(this);
		var selOpt = $(this).find("option:selected").first();
		if($(selOpt).attr("data-order"))
		{
			$(".collection-search-main input[name='wpv_column_sort_dir']").val($(selOpt).attr("data-order"));
		}

		$(".collection-search-main input[name='wpv_column_sort_id']").val($(selOpt).attr("data-sort-id"));

		$(".collection-search-main input[name='wpv_column_orderby']").val($(selOpt).attr("data-orderby"));
		//// console.log($(this).val());
		//(".wpv-filter-form").submit();

		FWP.refresh();
	});

	$(".collection-search-main").on("change", "select.grid-menu", function()
	{
		var gridSize = "large";

		if($(this).val().indexOf("4") || on_bootstrap_xs)
		{
			gridSize = "small";
			docCookies.setItem("gridSize", "small");
		}
		else
		{
			gridSize = "large";
			docCookies.setItem("gridSize", "large");
		}

		$(".collection-search-main input[name='grid_size']").val(gridSize);

		$("#content").removeClass("small-grid large-grid").addClass(gridSize + "-grid");
	});

	$(document).on('facetwp-loaded', function() {
		// Store current facets in cookie
		var date = new Date();
		var facets = window.location.hash;
		date.setTime(date.getTime()+(24*60*60*1000));


    	//If on mobile 
    	if(on_bootstrap_xs && FWP.facets.categories)
    	{
    		$("#mobile-search-options .category-dropdown").val(FWP.facets.categories[0]);
    		$("#mobile-search-options .collection-dropdown").val(FWP.facets.collection[0]);
    		$("#mobile-search-options input[type=text]").val(FWP.facets.keyword_search);
    	}

		//docCookies.setItem("selectedFacetHtml", facetSelectedHtml, null, "/"); 

		docCookies.setItem("facet_data", facets, date, "/"); //.cookie = "facetdata="+facets+"; expires="+date.toGMTString()+"; path=/";

		// console.log("Cookie!", docCookies.getItem("facet_data"));

		//debugger
		$(".facetwp-facet-date_range input").each(function() {
			$(this).attr("placeholder", "Year");
		});

		$(".field-wrapper select").addClass("selectpicker");
		$('.selectpicker').selectBoxIt(
		{
			showFirstOption: false
		});


		// Show More/Less link for Artists
		$(".collections .facetwp-type-checkboxes.facetwp-facet-artist").each(function(ix, el) {
			// instead of limiting height, only show 10 items, then show More link
			var child_cbs = $(this).find("> .facetwp-checkbox");

			if(child_cbs.length > 10)
			{
				var child_cbs_to_show = child_cbs.slice(0,9);
				var child_cbs_to_hide = child_cbs.slice(10);
				
				$(child_cbs_to_hide).wrapAll("<div class='extra-cbs hidden'></div>");

				var toggle_cb_link = $("<a href='#'>Show All</a>").appendTo(this);
				toggle_cb_link.on("click", function() {
					if($(this).text() == "Show Less")
					{
						$(this).text("Show All").parent(".facetwp-type-checkboxes").find(".extra-cbs").addClass("hidden").removeClass("visible");
					}
					else
					{
						$(this).text("Show Less").parent(".facetwp-type-checkboxes").find(".extra-cbs").removeClass("hidden").addClass("visible");
					}
					
					return false;
				});
			}
		});

		updateCurrentSearchTerms();

		var facetSelectedHtml = $(".facetwp-selections").html();
		docCookies.setItem("selectedFacetHtml", facetSelectedHtml, null, "/"); 

		// console.log("facetSelectedHtml", facetSelectedHtml);
	});

})(jQuery);










