<?php global $current_site, $members_directory_base ?>

<?php if ( network_have_posts() ) : ?>
	<div class="pagination"><?php echo global_site_search_pagination() ?></div>
	
	<div class="global-search-results">
	<?php global_site_search_form() ?>

			<?php $tic_toc = 'toc'; ?>
			<?php $substr = function_exists( 'mb_substr' ) ? 'mb_substr' : 'substr'; ?>
			<div class="global-search-recordset">
			<?php while ( network_have_posts() ) : network_the_post(); ?>
			
				<div class="global-search-record">
						<a href="<?php echo network_get_permalink() ?>">
							<strong><?php network_the_title() ?></strong>
						</a><br>
						<?php $the_content = preg_replace( "~(?:\[/?)[^/\]]+/?\]~s", '', network_get_the_content() ); ?>
						<?php $stripped_content = strip_shortcodes($the_content); ?>
						<?php echo $substr( strip_tags( $stripped_content ), 0, 250 ) ?>&hellip;<a class="more-link" href="<?php echo network_get_permalink() ?>"><?php _e( 'More', 'globalsitesearch' ) ?></a>
				</div>
			<?php endwhile; ?>
			</div>
			<div class="pagination"><?php echo global_site_search_pagination() ?></div>
<?php else : ?>
	<p style="text-align:center"><?php _e( 'Nothing found for search term(s).', 'globalsitesearch' ) ?></p>

	
<?php endif; ?>
</div>