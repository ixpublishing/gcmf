<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package gcmf
 */
//ini_set('display_errors',1);
//error_reporting(E_ALL);

//Define section colors
if( of_get_option('section_color') ) {
	$section_color = of_get_option('section_color');
	$steps = -38;
	$section_color_dark = adjustBrightness($section_color, $steps);
}

?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6299692/711644/css/fonts.css" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if( of_get_option('section_color') ) { ?>
<style>
	.section-link a, .pagination ul.page-numbers .current, .pagination a:hover, ul.isotope-filter a:hover, a.more-link, .exhibition-listview h3 a:hover, .multi-box .mb-inner-content a, .multi-box .mb-content a:hover, .profile-tile h3, .profile-tile h3 a, .widget_nav_menu a:hover, #nav-content a, .section-slider .btn, .content-box .heading, header.profile .pt-name,.res-result h2, .fancybox-skin h2, .res-result h3, .fancybox-skin h3, .tabs.exhibition-alt-tabs li a { color: <?php echo $section_color ?> !important; }

	.section-link a:hover, a.more-link:hover, .cta-box h2, .content-slide a:hover, .section-slider .btn:hover, .profile-tile h3 a:hover, .multi-box .mb-inner-content a:hover, .tabs.exhibition-alt-tabs li a:hover { color: <?php echo $section_color_dark ?> !important; }

	.section-slider.exhibitions-slider { background-color:#fff !important; }

	.section-slider .btn, .tabs.exhibition-alt-tabs li.active a { background-color: #fff !important; }
	.section-slider.exhibitions-slider .btn, .multi-box .mb-content a.btn { color: #fff !important; }
	.section-slider .btn:hover { color: #fff !important; background-color: <?php echo $section_color_dark ?> !important;  }
	nav#section-nav #menu-secondary-navigation li:hover ul li a:hover, nav#section-nav #menu-secondary-navigation li ul li:hover a { background-color: #5d5653 !important;}
	nav#section-nav li.current-menu-item .current-menu-item {background-color:transparent !important;}

	.section-slider .social-icons li, .multi-box .countdown-overlay, #sticky-hero,.section-slider, .multi-box header, .multi-box footer, .multi-box-wrapper header a, .multi-box-wrapper footer a, .mb-details .btn, .content-box .btn, .multi-box .btn.default, .section-slider.exhibitions-slider .btn, a.postlink:hover, .gcmf-section-slider-skin .rsBullet.rsNavSelected span, .past-exhibition-details .btn, #hero { background-color: <?php echo $section_color ?> !important; }

	.content-box .btn:hover, .multi-box .btn.default:hover, .multi-box-wrapper header a:hover, .multi-box-wrapper footer a:hover, .mb-details .btn:hover, .priority-link a:hover, .section-slider.exhibitions-slider .btn:hover, .priority-link a:hover, .past-exhibition-details .btn:hover, .section-slider .social-icons li:hover, nav#section-nav li.current-menu-parent, nav#section-nav li.current-exhibitions-ancestor, nav#section-nav li.current-category-ancestor { background-color: <?php echo $section_color_dark ?> !important; }

	a.postlink:hover { border-color: <?php echo $section_color ?> !important; }
	.content-panel, header.profile, .tax-program-category article.programs, .post-type-archive-news article.news, .content-box, .res-result-wrapper { border-top:solid 8px <?php echo $section_color ?> !important;}
	ul.isotope-filter .active, .tabs li.active:after, .tabs.exhibition-alt-tabs li:hover, .tabs.exhibition-alt-tabs li.active, .multi-box-wrapper { border-top-color: <?php echo $section_color ?> !important;}

</style>


<?php } ?>
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52351861-1', 'auto');
  ga('send', 'pageview');

</script>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>

	<div id="masthead-wrap">

		<header id="masthead" class="site-header" role="banner">

		<div class="container">

			<div class="row brand-header">

				<div class="col-md-4 col-xs-6 col-sm-5 clearfix">
					<?php switch_to_blog(1); ?>
					<div class="site-branding">
						<h3 id="logo" class="site-title logos-gcmf-logo-color pull-left hide-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h3>
					</div>
					<?php restore_current_blog(); ?>

				</div><!-- .col -->

				<div class="col-md-4 col-xs-6 col-sm-5 clearfix">
					<div class="pull-right">
					<?php switch_to_blog(1); ?>
						<?php wp_nav_menu( array('theme_location' => 'top_menu', 'container' =>  'info-bullets' ) ); ?>
					<?php restore_current_blog(); ?>
					</div>
				</div>
				<div id="search-top">
				<div class="col-md-4 col-xs-12 col-sm-12 clearfix">

						<aside id="search" class="widget widget_search">
							<?php global_site_search_form(); ?>
						</aside>
					</div>

				</div>


				<div class="col-md-8 col-xs-6 col-sm-2 clearfix">

<ul class="social-icons list-float">

						<?php if( of_get_option('url_linkedin') ) : ?>
							<li class="social-icons-linkedin-icon"><a target="_blank" href="<?php echo of_get_option('url_linkedin') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_flickr') ) : ?>
							<li class="social-icons-flickr-icon"><a target="_blank" href="<?php echo of_get_option('url_flickr') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_instagram') ) : ?>
							<li class="social-icons-instagram-icon"><a target="_blank" href="<?php echo of_get_option('url_instagram') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_pinterest') ) : ?>
							<li class="social-icons-pinterest-icon"><a target="_blank" href="<?php echo of_get_option('url_pinterest') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_itunes') ) : ?>
							<li class="social-icons-itunes-icon"><a target="_blank" href="<?php echo of_get_option('url_itunes') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_youtube') ) : ?>
							<li class="social-icons-youtube-icon"><a target="_blank" href="<?php echo of_get_option('url_youtube') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_gplus') ) : ?>
							<li class="social-icons-gplus-icon"><a target="_blank" href="<?php echo of_get_option('url_gplus') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_facebook') ) : ?>
							<li class="social-icons-fb-icon"><a target="_blank" href="<?php echo of_get_option('url_facebook') ?>"></a></li>
						<?php endif; ?>

						<?php if( of_get_option('url_twitter') ) : ?>
							<li class="social-icons-twitter-icon"><a target="_blank" href="<?php echo of_get_option('url_twitter') ?>"></a></li>
						<?php endif; ?>

					</ul>

					<div id="menu-bars">
					<nav id="site-navigation" class="main-navigation" role="navigation">

							<div class="menu-toggle">
								<a href="#"><i class="fa fa-bars"></i></a>
							</div>

							<div class="sr-only skip-link">
								<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'gcmf' ); ?>"><?php _e( 'Skip to content', 'gcmf' ); ?></a>
							</div>

							<?php switch_to_blog(1); ?>

							<?php wp_nav_menu( array('theme_location' => 'primary', 'container' => false , 'walker' => new UL_Class_Walker()  ) ); ?>

							<?php restore_current_blog(); ?>
							<span class="spacer"></span>

						</nav><!-- #site-navigation -->
					</div>

				</div>

			</div><!-- /.row -->
		</div><!-- header .container -->
				</header><!-- #masthead -->

				<div class="navigation">

					<div class="container">

					<nav id="site-navigation" class="main-navigation" role="navigation">

						<div class="menu-toggle">
							<a href="#"><i class="fa fa-bars"></i></a>
						</div>

						<div class="sr-only skip-link">
							<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'gcmf' ); ?>"><?php _e( 'Skip to content', 'gcmf' ); ?></a>
						</div>

						<?php switch_to_blog(1); ?>

						<?php wp_nav_menu( array('theme_location' => 'primary', 'container' => false , 'walker' => new UL_Class_Walker()  ) ); ?>

						<?php restore_current_blog(); ?>
						<span class="spacer"></span>

					</nav><!-- #site-navigation -->

					</div><!-- /container -->

				</div>

		</div><!-- #masthead-wrap -->

	<?php if ( is_page() && has_post_thumbnail() ) {

			$image_id = get_post_thumbnail_id();

	} else {

		$post_id = get_option('page_on_front');
		$image_id = get_post_thumbnail_id( $post_id );
	}
		$image_url = wp_get_attachment_image_src($image_id,'full', true);?>

	<?php if ( has_nav_menu( 'secondary' ) ) { ?>
	<div id="content-nav-wrap">
		<nav id="section-nav">
			<div class="container">
				<div class="row">
					<?php
					//Desktop Menu
					wp_nav_menu( array('theme_location' => 'secondary', 'menu_class' => 'list-inline desktop-menu', 'container' => false, 'walker' => new UL_Class_Walker() ) );?>
					<div class="col-sm-5 tablet">
					<ul id="mobile-menu" class="list-unstyled">
						<li class="has-children"><a href="#">Go to...</a>
						<?php
						//Mobile Menu
						wp_nav_menu( array('theme_location' => 'secondary', 'menu_id' =>'mobile-secondary-navigation', 'menu_class' => 'list-unstyled mobile-menu dropdown', 'container' => false, 'depth'=>'1', 'walker' => new UL_Class_Walker() ) );
						?>
						</li>
					</ul>
				</div>
				</div>
			</div><!-- #section-nav .container -->
		</nav>

<?php } ?>
