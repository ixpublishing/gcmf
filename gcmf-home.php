<?php
/**
 * Template Name: GCMF Home
 *
 *
 * @package gcmf
 */

get_header('no-hero'); ?>

<div id="sticky-hero">
  	<div id="hero" class="full-hero">
  		<div class="container">
  			<div class="row">
		<?php echo get_new_royalslider(1); ?>
			<div class="container nav-bullets gcmf-hero-slider-skin"></div>
			</div>
		</div>
	</div>
		<div id="content" class="site-content container">
				<?php

					
						if ( function_exists('yoast_breadcrumb') && !(is_front_page()) ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-no-title' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
					
</div><!-- #content -->

<?php get_footer(); ?>