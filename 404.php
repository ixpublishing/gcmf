<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Sorry, that page can&rsquo;t be found.', 'gcmf' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location.', 'gcmf' ); ?></p>

					<?php global_site_search_form() ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
					
</div><!-- #content -->

<?php get_footer(); ?>