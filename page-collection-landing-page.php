<?php
/**
 * Template Name: Collection Landing Page
 *
 * @package gcmf
 */
// ini_set('display_errors',1); 
// error_reporting(E_ALL);

$SHOW_PAGE_TITLE = true;

get_header(); ?>

<!--[if lt IE 9]>
<style>
	#content .works-grid .collection-item:hover .item-details
	{
		background:url(<?php echo get_template_directory_uri(); ?>/img/misc/000000-0.8.png) !important;
	}
</style>
<![endif]-->

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div class="row">
	
		<div class="col-md-9">

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'single' ); ?>
				
					
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>


				<?php endwhile; // end of the loop. ?>

				<script>
					jQuery(document).ready(function($) {
						$(".az-char").click(function() {
							// letter clicked! kick off the filter.

							$("#collection-list").load("?alpha=" + $(this).text() + " #collection-list", 
								function() {
									init_paging();
								});
							window.location.hash = "alpha=" + $(this).text();
							$(".az-char").removeClass("selected");
							$(this).addClass("selected");

						});
					});
				</script>

				<div class="collection">

					<?php	
					$taxonomy = 'collection';
					 
					// save the terms that have posts in an array as a transient
					if ( false === ( $alphabet = get_transient( 'col_archive_alphabet' ) ) ) {
					    // It wasn't there, so regenerate the data and save the transient
					    $terms = get_terms($taxonomy);
					 
					    $alphabet = array();
					    if($terms){
					        foreach ($terms as $term){
					            $alphabet[] = $term->slug;
					        }
					    }
					     set_transient( 'col_archive_alphabet', $alphabet );
					}
					 
					?>

					<div id="archive-menu" class="menu">
					 
					    <ul id="alphabet-menu">
					 	<a href="">&#171;</a>
					    <?php 
					     
					    foreach(range('a', 'z') as $i) :              
					 
					        $current = ($i == get_query_var($taxonomy)) ? "current-menu-item" : "menu-item";              
					        if (in_array( $i, $alphabet )){ 
					            printf( '<li class="az-char %s"><a href="%s">%s</a></li>', $current, get_term_link( $i, $taxonomy ), strtoupper($i) );
					        } else { 
					            printf( '<li class="az-char %s">%s</li>', $current, strtoupper($i) );
					        } 
					 
					    endforeach; 
					     
					    ?>
					    </ul>
					 
					</div>

					<div id="collection-list">

						<?php 
						/** 
						* Collections list template
						**/
						//require "inc/_collection_list.php"; 

						$args = array(
							"taxonomy" => "collection",
							"show_count" => 1,
							"hide_empty" => 0,
							"title_li" => "",
							"child_of" => $term->term_id
						);

						//echo $term->term_id;
						if(isset($_REQUEST["alpha"]))
							$alpha = $_REQUEST["alpha"];
						else
							$alpha = false;

						$args['walker'] = new Post_Category_Walker( $post_id, $args['taxonomy'], $alpha );
						?>

							<?php
							$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							if ($term->parent == 0) { 
								$args['depth'] = "1";
								wp_list_categories($args);
								//wp_list_categories('taxonomy=collection&depth=1&show_count=0&title_li=&child_of=' . $term->term_id); 
							} else {
								$args['child_of'] = $term->parent;
								wp_list_categories($args);
								//wp_list_categories('taxonomy=collection&show_count=0&title_li=&child_of=' . $term->parent); 
							}
						?>

						<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; 
						 ?>


					</div>

					<script>
						var cur_page = 0;
						var page_size = 10;
							

							
						function show_page(page_num)
						{
							var collection = jQuery("#collection-list .collection-box");
							var num_pages = collection.length / page_size;

							console.log("showing: " +  page_num + " of " + num_pages);

							collection.each(function(ix, el) {
								if(ix < collection.length - (num_pages-page_num)*page_size ||
									ix >= (collection.length - (num_pages-page_num)*page_size) + page_size)
								{
									jQuery(el).hide();
								}
								else
								{
									jQuery(el).show();
								}
							});
						}
						function init_paging()
						{
							console.log("init");
							jQuery(".pager").first().text("");

							var results = jQuery("#collection-list .collection-box");
							console.log("res: ", results.length);
							if(results.length > page_size)
							{
								var num_pages = results.length / page_size;
								console.log("numpages: ", num_pages);
								show_page(0);	

								for(var i = 0; i < num_pages; i++)
								{
									jQuery(".pager").first().append("<a class='collection-page' onclick='show_page("+ (i) +")'>" + (i+1) + "</a> ");
								}
							}
						}
						jQuery(function($) {
							
							init_paging();

						});
					</script>
					<div class='pager'>
						
					</div>
					<?php //get_template_part( 'content', 'collection-list' ); ?>
				</div>
					
			</main><!-- #main -->
		</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">

		<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>