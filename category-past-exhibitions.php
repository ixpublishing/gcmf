<?php
/**
 * The template for displaying Program Category Taxonomy Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gcmf
 */

get_header(); ?>
<?php
//CONDITIONAL CHECK FOR CHILD PAGE
  $is_child = cat_is_ancestor_of(4, $cat );?>

<?php if ( !is_paged() && !($is_child) ) { //top content check for paged ?>

<div id="content-top">
	<?php //if ( function_exists('yoast_breadcrumb') ) {
	//yoast_breadcrumb('<div class="container"><p id="breadcrumbs">','</p></div>');
	//} ?>

	 <?php
	 //Get past exhibition with the most recent end date.
	 $args = array(
    'posts_per_page' => 1,
    'category' => 4,
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'meta_key' => 'wpcf-end-date'
    );
    $last_exhibition = get_posts($args);
	foreach ( $last_exhibition as $post ) :
	setup_postdata( $post );

	//Set Exhibition info variables
	//Start Date
	$start_timestamp = (!((get_post_meta($post->ID, 'wpcf-start-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-start-date',TRUE) : '' ;
	$start_date = ($start_timestamp == '') ? '' : date("F j, Y", $start_timestamp);
	$short_start_date = ($start_timestamp == '') ? '' : date("M j, Y", $start_timestamp);

	//End Date
	$end_timestamp = (!((get_post_meta($post->ID, 'wpcf-end-date', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-end-date',TRUE) : '' ;
	$end_date = ($end_timestamp == '') ? '' : ' &#8211; ' . date("F j, Y", $end_timestamp);
	$short_end_date =  ($end_timestamp == '') ? '' : date("M j, Y", $end_timestamp);

	//Location(s)
	$location = types_render_field("gallery-location", array());

	//Slider Images
	$slider_image_1 = (!((get_post_meta($post->ID, 'wpcf-slider-image-1', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-slider-image-1',TRUE) : '' ;
	$slider_image_2 = (!((get_post_meta($post->ID, 'wpcf-slider-image-2', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-slider-image-2',TRUE) : '' ;
	$slider_image_3 = (!((get_post_meta($post->ID, 'wpcf-slider-image-3', TRUE))=='')) ? get_post_meta($post->ID,'wpcf-slider-image-3',TRUE) : '' ;
	?>
		<div class="container">
			<div class="row">
				<div class="section-slider exhibitions-slider clearfix">
					<div class="col-md-6 ss-content">
						<h3>Last Exhibition</h3>
						<h2><?php the_title(); ?></h2>
						<p class="exhibition-date"><?php echo $start_date . $end_date; ?></p>
						<?php if(!empty($location)) echo '<div class="wpcf-field-gallery-location">' . $location . '</div>'; ?>
						<a class="btn" href="<?php the_permalink(); ?>">Exhibiton Details</a>
					</div>
					<div class="col-md-6 ss-image">
						<div class="no-gutter">

						<?php
						//See if there are any slides, if not, show the featured image
						if( ($slider_image_1 == '') && ($slider_image_2 == '') && ($slider_image_3 == '') ) {
							//$img_meta = get_post(get_post_thumbnail_id());print_r($t);
							$thumbnail_args = array(
								'class'	=> "img-responsive",
								//'alt'	=> trim(strip_tags( $last_exhibition[0]->post_title )),
								//'title'	=> trim(strip_tags( $last_exhibition[0]->post_title ))
							);
							echo get_the_post_thumbnail($post->ID, 'full', $thumbnail_args);
						} else {
						?>
							<div class="exhibition-slider gcmf-section-slider-skin">
							<?php if(!($slider_image_1 == '')) : ?>
							<img class="rsImg" src="<?php echo $slider_image_1; ?>" alt="<?php echo $slider_caption_1; ?>" />
							<?php endif; ?>
							<?php if(!($slider_image_2 == '')) : ?>
							<img class="rsImg" src="<?php echo $slider_image_2; ?>" alt="<?php echo $slider_caption_2; ?>" />
							<?php endif; ?>
							<?php if(!($slider_image_3 == '')) : ?>
							<img class="rsImg" src="<?php echo $slider_image_3; ?>" alt="<?php echo $slider_caption_3; ?>" />
							<?php endif; ?>
							</div>
							<?php } ?>
						</div>
					</div><!-- .ss-image -->
						<ul class="social-icons list-unstyled">
							<li class="social-icons-pinterest-icon-alt"><a href="#"></a></li>
							<li class="social-icons-twitter-icon-alt"><a href="#"></a></li>
							<li class="social-icons-fb-icon-alt"><a href="#"></a></li>
						</ul>
				</div><!-- .section-slider -->
			</div><!-- .row -->

		</div><!-- .container -->

	</div>
<?php //reset latest exhibition query
 endforeach;
 wp_reset_postdata(); ?>

<?php }; //top content end check for paged ?>

 <div id="filter">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<header>
					<h1>
					<?php single_cat_title('Browse '); echo $is_child ? ' Exhibitions' : ''; ?>
					</h1>
				</header><!-- .page-header -->
			</div>
			<div class="col-sm-4">
				<ul class="exhibition-year-sort-filter list-unstyled dd-menu-basic">
					<li class="has-children"><a href="#">Browse By Year</a>
					<?php
					//Get all Exhibition Years
					$args = array(
						'post_type' => 'exhibitions',
						'parent'                   => 4,
						'orderby'                  => 'name',
						'order'                    => 'DESC',
						'hide_empty'               => 0
					);

					$years = get_categories( $args );

						 $count = count($years);
						 if ( $count > 0 ){
							 echo '<ul class="list-unstyled">';
							 foreach ( $years as $year ) {
							 $link = get_category_link(get_cat_id($year->cat_name));
							   echo '<li><a href="' . esc_url( $link ) . '" rel="tag">' . $year->name . '</a></li>';

							 }
							 echo "</ul>";
						 }
					?>
					</li>
				</ul>
			</div>
		</div>
	</div><!-- .row -->
</div>

<div id="content" class="site-content container">

			<section id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<div class="row">
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							//get_template_part( 'content', 'past-exhibitions' );
							include(locate_template('content-past-exhibitions.php'));
						?>
			<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>
		<?php echo paginate('bottom'); ?>
		</div>
		</main><!-- #main -->
	</section><!-- #primary -->

</div><!-- #content -->

<?php get_footer(); ?>
