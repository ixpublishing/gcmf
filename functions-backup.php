<?php 

function gcmf_facetwp_index_row( $params, $class ) {
    if ( 'subject' == $params['facet_name'] ||  'format' == $params['facet_name'] ) {

	$fields = wpcf_admin_fields_get_fields();

        $values = $params['facet_display_value'];
	if(!is_array($values)):
		$wpcf_field_name = str_ireplace("cf/wpcf-", "", $params["facet_source"]);
	        $wpcf_field = wpcf_admin_fields_get_field($wpcf_field_name);
		foreach ( $wpcf_field['data']['options'] as $field ) {
        		if($field['value'] == $params['facet_value'])
        			$cb_field = $field['title'];
        	}

		$params['facet_display_value'] = print_r($cb_field, true);// $cb_field['title'];//get_the_title($params['post_id']) .  print_r($fields, true);
		$class->insert($params);
		return false;

	else:
          foreach ( $values as $key=> $val ) {

		$cb_field = $fields[$params['facet_name']]['data']['options'][$key];
		$params['facet_value'] = $cb_field['set_value']; //md5($key);
		$params['facet_display_value'] = $cb_field['title'];
		$class->insert($params);
	  }
	  return false;
	endif;
    }
    return $params;
}
add_filter( 'facetwp_index_row', 'gcmf_facetwp_index_row', 10, 2 );

////Related News

add_action('init', 'remove_parent_theme_shortcodes');

function remove_parent_theme_shortcodes() {

remove_shortcode('related_news', 'shortcode_related_news');

// add the same shortcode in child theme with our own function
// note to self: think of a better example function name
add_shortcode('related_news', 'gcmf_shortcode_related_news');
}

function gcmf_shortcode_related_news( $atts, $content = null) {
 	extract( shortcode_atts( array (
     'posts' => -1,
    ), $atts ) );

	switch_to_blog(3); //switch to pressroom to get articles
	// Get Related Posts
	$cat = get_category_by_slug($atts['slug']);
	$args = array(
	'post_type' => 'news',
	'category' => $cat->term_id,
	'posts_per_page' => $posts
	);
	
	$related_news  = get_posts( $args );
	$html = '';
	global $post;
	$html .= '<ul class="list-unstyled entry-list">';
	foreach ($related_news as $post) : setup_postdata( $post ); {
		//print_r($post);
		$html .= '<li class="entry-list-item">';
		$html .= '<div class="row">';
		$html .= '<div class="col-xs-2 entry-list-thumb"><a href="' . get_permalink() . '">';
		$html .= ( '' != get_the_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail') : '<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/thumbnail-default.png" alt="" />';
		$html .= '</a></div>';
		$html .= '<div class="col-xs-10 entry-list-content"><p class="entry-date">'. get_the_date() . '</p><p class="entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
		$html .= '<p class="excerpt">' . the_excerpt_max_charlength(120) . ' <a href="'. get_permalink() .'" class="jumplink">Read more</a></p>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</li>';
	}
	endforeach;
	$html .= '</ul>';
	wp_reset_postdata();
	restore_current_blog(); //switch back to main site
	return $html;
}

?>






