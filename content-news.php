<?php
/**
 * @package gcmf
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
		<div class="col-sm-4">
		
			<?php
			$taxonomy_filter = 'news-category';
			$tax_terms_filtered = wp_get_object_terms( $post->ID, $taxonomy_filter );
			$parent_term_id = $tax_terms_filtered[0]->parent;
			$parent_taxonomy = $tax_terms_filtered[0]->taxonomy;
			$parent_term = get_term_by('id', $parent_term_id , $parent_taxonomy );
			
			?>
			
			<?php 
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				 echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '">' . the_post_thumbnail('full', array('class'=>'img-responsive') ) . '</a>';
				} else {
					echo '<a href="' . get_permalink() .'" title="' . get_the_title() . '"><img src="' . get_template_directory_uri() . '/img/placeholders/-placeholder.png" class="img-responsive" alt="" /></a>';
				}
					
			?>
		</div><!-- .col-sm-4 -->
		
		<div class="col-sm-8">
			
			<header class="entry-header">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			</header><!-- .entry-header -->

			<div class="entry-content">
			<p class="entry-date"><?php echo get_the_date(); ?> <!-- Author: --> <?php //the_author(); ?></p>
			<?php
			if ( is_category() || is_archive() ) {
				the_excerpt();
			} else {
				the_content();
			} 
			?>
				
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">

				<?php edit_post_link( __( 'Edit', 'gcmf' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
	
		</div><!-- .col-sm-8 -->
	</div>
	
	<?php 
	


$related_exhibitions = get_the_terms( $post->ID, 'category' );
$related_exhibitions_list = array();

$article_cats = get_the_terms( $post->ID, 'news-category');
$article_cats_list = array();

foreach($related_exhibitions as $related_exhibition) {
	$url = get_field('related_exhibition_url', 'category_'.$related_exhibition->term_id);
	$related_exhibitions_list[] = '<a href="'. $url .'">' . $related_exhibition->name . '</a>';
}

foreach($article_cats as $article_cat) {
	$article_cats_list[] = '<a href="' . get_term_link($article_cat) . '">' . $article_cat->name . '</a>';
}?>

<?php if(!empty($article_cats_list)) : ?>
	<p class="related-items">
		<?php echo 'Article Categories: ' . wp_sprintf('%l', $article_cats_list); ?>
	</p>
<?php endif; ?>

<?php if(!empty($related_exhibitions_list)) : ?>
	<p class="related-items last">
		<?php echo 'Related Exhibitions: ' . wp_sprintf('%l', $related_exhibitions_list); ?>
	</p>
<?php endif; ?>

				
</article><!-- #post-## -->
