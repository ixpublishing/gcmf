<?php
/**
 * The template for displaying Program Category Taxonomy Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gcmf
 */

get_header();?>

<div id="filter">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<header>
					<h1>
					<?php single_cat_title(); ?>
					</h1>
				</header><!-- .page-header -->
			</div>
			<div class="col-sm-3">
			<?php echo exhibition_sort_menu_dropdown(); ?>
			</div>
		</div><!-- .row -->
	</div>
</div>

<div id="content" class="site-content container">

<?php //echo exhibition_sort_menu(); ?> 

			<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
				<div class="row">
					<div id="isotope-container">
				<?php while ( have_posts() ) : the_post(); ?>
			
							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', 'exhibition-section' );
								//include(locate_template('content-at-the-museum.php'));
							?>
				<?php endwhile; ?>
					</div>
				</div><!-- .row -->
			<?php else : ?>

				<?php get_template_part( 'no-results', 'archive' ); ?>

			<?php endif; ?>
		</main><!-- #main -->
	</section><!-- #primary -->
					
</div><!-- #content -->

<?php get_footer(); ?>
