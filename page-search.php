<?php
/**
 * Template Name: GCMF Facet Search
 *
 * @package gcmf
 */

get_header(); ?>
 <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/gcmf.js"></script>

<div id='search-wrapper' class='container'>
	<!--
	<?php
	// $fields = wpcf_admin_fields_get_field('object-era');//wpcf_admin_fields_get_fields();
	//print_r($fields);
	?>
	-->
	<!--?php echo do_shortcode('[wpv-form-view name="Collections: Keyword Search" target_id="1783"]'); ?-->

	<!-- 
	<hr/>
	CF:
	<?php //print_r(types_render_field("artist", array( "id" => "2961" )));

	//print_r(get_post_custom_values('wpcf-collection', 2961));

	// types_render_field( 'collection', array( "id" => 2163 ) ); ?>
	-->

<div id="content" class="searchtemplate site-content <?php echo $_COOKIE['gridSize'] . "-grid" ?>">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">


				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php if(!is_front_page()) : ?>
						<div>
							<header class="hidden-xs entry-header">
								<h1 class="entry-title"><?php the_title(); ?></h1>
							</header><!-- .entry-header -->
							<?php endif; ?>

							<div class='search-content'>
								<?php the_content(); ?>
							</div>
						</div>
						
						<div id="search-sidebar">
							<div id='sidebar-contents'>
								<h2>Filter By</h2>
								<h3>Format:</h3>
								<?php echo do_shortcode( '[facetwp facet="format"]' ); ?>
								<h3>Collection:</h3>
								<?php echo do_shortcode( '[facetwp facet="collection"]' ); ?>
								<h3>Subject:</h3>
								<?php echo do_shortcode( '[facetwp facet="subject"]' ); ?>
								<h3>Author:</h3>
								<?php echo do_shortcode( '[facetwp facet="author"]' ); ?>

							</div>
						</div>

						<div class="entry-content" >
						
							<div class='search-main-wrapper clearfix '>

									<div class='row  col-md-12'>
									<div class='col-md-10 toggle-highlight'>
										<label for="toggle-highlight">Highlight</label> 
										<input id='toggle-highlight' type="checkbox" />
										
										<label for="digital-only">Digital Only</label>
										<input id="digital-only" type='checkbox' />
									</div>	
									<div class='col-md-2'>
											<?php echo do_shortcode("[facetwp sort='true']"); ?>
									</div>


								</div>

							        <div class='row search-input'>
							        	<div class='col-sm-12 col-md-12 col-lg-4'>
							        		<?php echo do_shortcode("[facetwp facet='keyword_search']"); ?>
							        	</div>	
										<div class='field-wrapper date-range col-md-8 col-sm-8 col-lg-5'>
											<label class='primary'>Year:</label>
											<?php echo do_shortcode("[facetwp facet='date_range']"); ?>
										</div>
										<div class='col-md-4 col-sm-4 col-lg-3'>
								       			<a id='reset-btn' href="/library/results">Reset</a>
												<input type='submit' value="Search" class='btn btn-search' />
							       		</div>
									</div>
								
								<div class="row col-lg-12">

										<?php echo do_shortcode("[facetwp selections='true']"); ?>

										<?php echo do_shortcode("[facetwp facet='has_digital_attachment']"); ?>
								</div>
								<div class='row col-lg-12 col-md-12 search-options-row '>
										<div class="col-sm-6 numbered-results">
											<?php echo do_shortcode( '[facetwp counts="true"]' . '&nbsp;Result(s)' ); ?>
										</div>
										<div class='col-lg-6 col-md-6 col-sm-12 pull-right'>
											<?php echo do_shortcode( '[facetwp pager="true"]' ); ?>
										</div>
										
									</div>
								</div>

							<div class='clearfix'>
								
									<?php echo do_shortcode( '[facetwp template="default"]' ); ?>
									
								<?php
									wp_link_pages( array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'gcmf' ),
										'after'  => '</div>',
									) );
								?>
							</div>

							<div class='row col-lg-12 col-md-12 search-options-row-bottom '>
										<div class="col-sm-6 numbered-results">
											<?php echo do_shortcode( '[facetwp counts="true"]' . '&nbsp;Result(s)' ); ?>
										</div>
										<div class='col-lg-6 col-md-6 col-sm-12 pull-right'>
											<?php echo do_shortcode( '[facetwp pager="true"]' ); ?>
										</div>
										
									</div>
						</div><!-- .entry-content -->
						<?php edit_post_link( __( 'Edit', 'gcmf' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
					</article><!-- #post-## -->

				<?php endwhile; // end of the loop. ?>

				<?php //print_r($_SESSION["search_result_ids"]); ?>

			</main><!-- #main -->
		</div><!-- #primary -->
				
	</div><!-- #content -->
</div>

<?php get_footer(); ?>
