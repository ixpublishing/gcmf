<?php
/**
 * The template for displaying Faculty Member pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div class="row">
	
		<div class="col-md-9">

			<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
			//get terms of current query
			$faculty_member = get_queried_object();?>			
				
				<header class="profile clearfix">
				<div class="row">
					<div class="col-sm-3 pt-img">
					<?php $fac_image = get_field('faculty_image', 'faculty-member_' . $faculty_member->term_id  );
 
					if( !empty($fac_image) ): ?>
					 
						<img src="<?php echo $fac_image['url']; ?>" alt="<?php echo $fac_image['alt']; ?>" class="img-responsive" />
					 
					<?php else: ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/placeholders/headshot.png" class="img-responsive" />
					<?php endif; ?>
					</div><!-- .col-md-4 -->
					<div class="col-sm-9 pt-content">
						<div class="inner-content">
							<h2 class="pt-name">
								<?php echo $faculty_member->name; ?>
							</h2>
				
						
						<p class="pt-title"><?php the_field('faculty_title', 'faculty-member_' . $faculty_member->term_id ); ?></p>
						<?php if ( ! empty( $faculty_member->description ) ) :
						printf( '<p class="page-description">%s</p>', $faculty_member->description );
						endif;
						?>
						</div>
					</div><!-- .col-md-9 -->
				</div><!-- .row -->
				</header><!-- .page-header -->
			<section id="related-programs">
			<h2>Current Programs with <?php echo $faculty_member->name; ?></h2>
			<?php if ( have_posts() ) :  /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post();?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'faculty-member' );
				?>

			<?php endwhile; ?>

		<?php else : ?>
			<p>There are currently no programs for <?php echo $faculty_member->name; ?>.</p>
			<?php //get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>
		</section><!--/#related-programs -->
		</main><!-- #main -->
	</section><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">
		
			<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>
