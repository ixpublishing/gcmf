<?php

// Validate facets

foreach ( $this->facets as $facet ) {

	//print_r($facet);

    if (  $facet['name'] == "keyword_search" ) {

        $keywords = $facet['selected_values'];

        break;

    }

}

//echo "<!-- " . print_r($this->facets, true) . "-->";

?>

<?php

	$hilite = false;

	try {

		$hilite = new SearchWP_Term_Highlight();

	} catch (\Exception $e) {

	}

?>

<?php 

//$highlighter = false;

/*$highlighter = false;

//if( class_exists( 'SearchWP_Term_Highlight' ) ) {

    //$highlighter = new SearchWP_Term_Highlight();

//}*/

?>

<?php while ($query->have_posts()) : ?>

<?php $query->the_post(); ?>
		<?php if (types_render_field("available-copy")!='Yes') : ?>

<div class="row">

	<div class="title-date col-sm-9 col-xs-12">

	    <div class="results-title">

			<?php

				// highlight the title

				$title = get_the_title();

				if( $hilite ) {

					$title = $hilite->apply_highlight( $title, $keywords );

				}

				$item_year = get_post_meta($post->ID,'wpcf-year',true);

				if($item_md = types_render_field("date", array("format" => "F j, ")))

				{

					$item_date = $item_md . $item_year;

				}

				else {

					$item_date = $item_year;

				}

			?>

			<a href="<?php the_permalink(); ?>"><?php echo $title; ?></a><?php echo !empty($item_date) ? (", " . $item_date) : "" ?>

			<br>
			<?php if (!((get_post_meta($post->ID, 'wpcf-creator', TRUE))=='')) : ?>

			<span class="result-type">Author:</span> <?php echo ($creator = get_post_meta($post->ID,'wpcf-creator',true)) ? "$creator" : "" ?>

			<br>

			<?php endif; ?>


			<?php if (!((get_post_meta($post->ID, 'wpcf-publisher', TRUE))=='')) : ?>

			<span class="result-type">Publisher:</span> <?php echo ($publisher = get_post_meta($post->ID,'wpcf-publisher',true)) ? "$publisher" : "" ?>

			<br>

			<?php endif; ?>

			<?php if (!((get_post_meta($post->ID, 'wpcf-call-number', TRUE))=='')) : ?>

			<span class="result-type">Call Number:</span> <?php echo ($callnumber = get_post_meta($post->ID,'wpcf-call-number',true)) ? "$callnumber" : "" ?>

			<?php endif; ?>

			<?php $col_titles = get_the_terms($post->ID, 'collection'); ?>
			<?php if (!empty($col_titles)) { ?>
			<span class="result-type">Collection: </span><span class="results-collection"><?php echo the_terms($post->ID, 'collection'); ?></span>
			<?php } ?>

		</div>

				<p class='excerpt'>

		<?php

		$excerpt = get_the_excerpt();

		if( $hilite ) {

			$excerpt = $hilite->apply_highlight( $excerpt, $keywords );

		}

		echo $excerpt;

		?>

		</p>

	</div>

		<div class='item-info col-sm-3 col-xs-12'>

	   <?php

		$format = types_render_field( "format", array( "separator" => ", " ) );

		$format_url = preg_replace("#[[:punct:]]#", "", str_replace(" ", "-", strtolower($format)));

		echo ($format) ? "<p class='result-format'><span class='format-icon format-".$format_url."'>$format</span></p>" : "" ?>

	    <?php echo ($download_link = types_render_field("pdf-attachment", array("output" => "raw"))) ? "<span class='download-link'><a href='$download_link' target='_blank'>Download</a></span><br>" : "" ?>

		<?php echo do_shortcode('[types field="audio-file"  loop="on"][/types]'); ?>

		<p class="result-file"><span class="partial-result"><a href="<?php echo types_render_field("file-attachment", array("output" => "raw")); ?>" target='_blank'><?php echo types_render_field("file-title", array()); ?></a></span></p>

	</div>

</div>

<div class="row">

	<div class='col-sm-12'>

		<div class="tags">
			<?php
			$posttags = get_the_tags();
			if ($posttags) {
			  foreach($posttags as $tag) {
			  	?>
			  	Tags: <a onclick="window.location='/library/results/#!/keyword_search=<?php echo $tag->name; ?>'; window.location.reload(); return false;" href="#"><?php echo $tag->name ?></a>,
			  	<?php
			    //echo '<a onclick="window.location=" href="/library/results/#!/keyword_search=' . $tag->name . '">' . $tag->name .'</a> ';
			  }
			}
			?>
		</div>

	</div>

</div>

<hr>
<?php endif; ?>
<?php endwhile; ?>
