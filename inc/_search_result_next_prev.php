<?php

/**
Next and previous links based on results array from SESSION.
**/
// 1. find my index
$curIx = array_search(get_the_id(), $_SESSION["search_result_ids"]);

$result_ids = $_SESSION["search_result_ids"];

if($curIx > 0 && $result_ids[$curIx - 1]) {
	$prev_post = get_post($result_ids[$curIx - 1]);
}
else {
	$prev_post = null;
}

if( count($result_ids) > $curIx && $result_ids[$curIx + 1]) {
	$next_post = get_post($result_ids[$curIx + 1]);
}
else {
	$next_post = null;// get_next_post();
}
?>

<?php
if (!empty( $prev_post )): ?>
<div class='col-lg-2 col-sm-2 col-md-2'>
	<a href="<?php echo get_permalink( $prev_post->ID ); ?>" title="<?php echo $prev_post->post_title; ?>" class="search-prev"><img src="<?php bloginfo('template_directory'); ?>/img/utility-icons/arrow_back.png"> Previous</a>
</div>
<?php
// if ( has_post_thumbnail($prev_post->ID)) {
//   echo '<div class="pl-prev-thumb hidden-sm hidden-xs"><a href="' . get_permalink( $prev_post->ID ) . '" title="' . esc_attr( $prev_post->post_title ) . '">';
//   echo get_the_post_thumbnail($prev_post->ID, 'nav-thumbnail');
//   echo '</a></div>';
//}		
?>
<?php endif; ?>
<?php
if (!empty( $next_post )): ?>
	<div class='col-lg-2 col-sm-2 col-md-2 pull-right'>
		<a href="<?php echo get_permalink( $next_post->ID ); ?>" title="<?php echo $next_post->post_title; ?>" class="search-next">Next <img src="<?php bloginfo('template_directory'); ?>/img/utility-icons/arrow_fwd.png"></a>
	</div>
<?php
// if ( has_post_thumbnail($next_post->ID)) {
//   echo '<div class="pl-next-thumb hidden-sm hidden-xs"><a href="' . get_permalink( $next_post->ID ) . '" title="' . esc_attr( $next_post->post_title ) . '">';
//   echo get_the_post_thumbnail($next_post->ID, 'nav-thumbnail');
//   echo '</a></div>';
//}		
?>
<?php endif;?>


<div class='col-lg-2 col-sm-2 col-md-2'>
	<a href='/library/results/<?php echo $_SESSION["search_result_params"]; ?>'>Return to Search</a>
</div>
<div class='col-lg-6'>
	<?php //TODO: add a responsive class (pull-xs-left, pull-md-right, etc) ?>
	<div class="facetwp-selections">
		<div class='hidden-xs text-right'><?php 
		echo stripslashes($_COOKIE['selectedFacetHtml']);
		?></div>
		<div class='visible-xs text-left'><?php 
		echo stripslashes($_COOKIE['selectedFacetHtml']);
		?></div>
	</div>
	
</div>