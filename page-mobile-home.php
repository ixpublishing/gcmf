<?php
/**
 * The template for mobile version of home page.
 *
 * Template Name: Home - Mobile
 *
 * @package gcmf
 */

get_header('mobile');?>
<div id="home-mobile-content">
	<?php the_content();?>
</div>
<?php
get_footer();
?>