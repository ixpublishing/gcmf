<?php
/**
 * The Template for displaying tags
 *
 * @package gcmf
 */

get_header(); ?>

<div id="content" class="site-content container">
	<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>

	<div class="row">
	
		<div class="col-md-9">

			<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">

							<?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' '; 
  }
}
?>

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Tag Archives: %s', 'twentythirteen' ), single_tag_title( '', false ) ); ?></h1>

				<?php if ( tag_description() ) : // Show an optional tag description ?>
				<div class="archive-meta"><?php echo tag_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

				</main><!-- #main -->
	</div><!-- #primary -->
		
		</div><!-- .col-md-9 -->
		
		<div class="col-md-3">

		<?php get_sidebar(); ?>
			
		</div><!-- .col-md-3 -->

	</div> <!-- .row -->
					
</div><!-- #content -->

<?php get_footer(); ?>