<?php

function test_sc($atts){
   extract(shortcode_atts(array(
      'text' => ''
   ), $atts));

   $return_string = 'I entered ' . $text;
   return $return_string;
}
add_shortcode('test_sc', 'test_sc');


//////////////////////////////////////////////////////////////////
// Remove extra P tags
//////////////////////////////////////////////////////////////////
function shortcodes_formatter($content) {
	$block = join("|",array("button", "tabs", "tab", "alt_tabs", "one_half", "one_third", "two_third", "one_fourth", "three_fourth", "cta_box", "cta_box_image", "cta_box_content", "multi_box", "multi_box_image", "multi_box_content", "content_boxes", "content_box", "advanced_content_box", "post_slider", "directory_list", "title", "description", "spacer", "related_image", "columns", "get_object", "list_posts", "press_center", "featured_posts_slider", "featured_shop_items_slider", "section_slider", "related_news", "related_news_chrono", "hero_slider_content_box", "googlemap", "iframe", "mobile_section_button", "gallery", "faculty_list", "ob-home-slider", "ob-act-block", "contact-form-7", "brand_box", "event_calendar" ));

	// opening tag
	$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);

	// closing tag
	$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)/","[/$2]",$rep);

	return $rep;
}

add_filter('the_content', 'shortcodes_formatter');
add_filter('widget_text', 'shortcodes_formatter');



//////////////////////////////////////////////////////////////////
// Button shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('button', 'shortcode_button');
	function shortcode_button($atts, $content = null) {
	
	   extract(shortcode_atts(array(
		'color' => 'default',
		'link' => '#',
		'new_window' => '',
		'custom_button_color' => '',
		'custom_text_color' => ''
	  ), $atts));
	  
		$new_window = ($new_window == 'yes') ? ' target="_blank"' : '';

			
			if( ($custom_button_color) || ($custom_text_color) ) {
				$arr = array();
				
				$custom_class=" custom-btn";
				
				if( $custom_button_color ) {
					$arr[]='background-color:'. $custom_button_color . ';';
				}
				
				if( $custom_text_color ) {
					$arr[]='color:'. $custom_text_color . ';';
				}

				$styles = implode(" ", $arr);
				
				$style = 'style="'. $styles . '"';
			
			} else {
				$custom_class='';
				$style='';
			}
			
			return '<a class="btn' . ' ' . $color . $custom_class . '" href="' . $link . '"' . $new_window . ' ' . $style .'>' .do_shortcode($content). '</a>';
	}

//////////////////////////////////////////////////////////////////
// Tabs shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('tabs', 'shortcode_tabs');
	function shortcode_tabs( $atts, $content = null ) {
    
    $out = '';

    $out .= '<div class="tab-holder shortcode-tabs">';

	$out .= '<div class="tab-hold tabs-wrapper">';
	
	$out .= '<ul id="tabs" class="tabset tabs">';
	foreach ($atts as $key => $tab) {
		$out .= '<li><a href="#' . $key . '">' . $tab . '</a></li>';
	}
	$out .= '</ul>';
	
	$out .= '<div class="tab-box tabs-container">';

	$out .= do_shortcode($content) .'</div></div></div>';
	
	return $out;
}

add_shortcode('tab', 'shortcode_tab');
	function shortcode_tab( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'id'=>''
    ), $atts));
    
	$out = '';
	$out .= '<div id="tab' . $id . '" class="tab tab_content">' . do_shortcode($content) .'</div>';
	
	return $out;
}

//////////////////////////////////////////////////////////////////
// Alternative Tabs shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('alt_tabs', 'shortcode_alt_tabs');
	function shortcode_alt_tabs( $atts, $content = null ) {
	extract(shortcode_atts(array(
    ), $atts));
    
    $out = '';

    $out .= '<div class="tab-holder shortcode-tabs alt-tabs">';

	$out .= '<div class="tab-hold tabs-wrapper">';
	
	$out .= '<ul id="tabs" class="tabset tabs">';
	foreach ($atts as $key => $tab) {
		$out .= '<li><a href="#' . $key . '">' . $tab . '</a></li>';
	}
	$out .= '</ul>';
	
	$out .= '<div class="tab-box tabs-container">';

	$out .= do_shortcode($content) .'</div></div></div>';
	
	return $out;
}
	
//////////////////////////////////////////////////////////////////
// Column one_half shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_half', 'shortcode_one_half');
	function shortcode_one_half($atts, $content = null) {
		extract( shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts) );
			
			$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'one_half';
		
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
			return '<div class="col-md-6' . $responsive_width . '">' .do_shortcode($content). '</div>';
	}
	


//////////////////////////////////////////////////////////////////
// Column one_half_wide shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_half_wide', 'shortcode_one_half_wide');
	function shortcode_one_half_wide($atts, $content = null) {
		extract( shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts) );
			
			$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'one_half_wide';
		
			switch($tablet_width) {	
				case 'one_half_wide':
					$responsive_width = ' col-sm-12';
					break;	
				case 'one_half':
					$responsive_width = ' col-sm-12';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
			return '<div class="col-md-6' . $responsive_width . '">' .do_shortcode($content). '</div>';
	}
	
//////////////////////////////////////////////////////////////////
// Column one_third shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_third', 'shortcode_one_third');
	function shortcode_one_third($atts, $content = null) {
		extract(shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts));
			
			$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'one_third';
		
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
							
			return '<div class="col-md-4' . $responsive_width . '">' .do_shortcode($content). '</div>';

	}
	
//////////////////////////////////////////////////////////////////
// Column two_third shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('two_third', 'shortcode_two_third');
	function shortcode_two_third($atts, $content = null) {
		extract(shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts));
			
		$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'two_third';
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
		return '<div class="col-md-8' . $responsive_width . '">' .do_shortcode($content). '</div>';

	}
	
//////////////////////////////////////////////////////////////////
// Column one_fourth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_fourth', 'shortcode_one_fourth');
	function shortcode_one_fourth($atts, $content = null) {
		extract(shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts));
			
		$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'one_fourth';
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
		return '<div class="col-md-3' . $responsive_width . '">' .do_shortcode($content). '</div>';

	}

//////////////////////////////////////////////////////////////////
// Column one_fourth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_fourth_wide', 'shortcode_one_fourth_wide');
	function shortcode_one_fourth_wide($atts, $content = null) {
		extract(shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts));
			
		$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'one_fourth_wide';
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-6';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
		return '<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3' . $responsive_width . '">' .do_shortcode($content). '</div>';

	}
	
//////////////////////////////////////////////////////////////////
// Column three_fourth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('three_fourth', 'shortcode_three_fourth');
	function shortcode_three_fourth($atts, $content = null) {
		extract(shortcode_atts(
			array(
				'tablet_width' => ''
			), $atts));
			
		$tablet_width = ($tablet_width !== '') ?  $tablet_width : 'three_fourth';
			switch($tablet_width) {	
				case 'one_half':
					$responsive_width = ' col-sm-6';
					break;	
				case 'one_fourth':
					$responsive_width = ' col-sm-3';
					break;	
				case 'three_fourth':
					$responsive_width = ' col-sm-9';
					break;	
				case 'one_third':
					$responsive_width = ' col-sm-4';
					break;	
				case 'two_third':
					$responsive_width = ' col-sm-8';
					break;
			}
			
		return '<div class="col-md-9' . $responsive_width . '">' .do_shortcode($content). '</div>';

	}

//////////////////////////////////////////////////////////////////
// Brand Box
//////////////////////////////////////////////////////////////////
add_shortcode('brand_box', 'shortcode_brand_box');
	function shortcode_brand_box($atts, $content = null) {
	
		extract(shortcode_atts(
		array(
			'link' => '#',
			'linktarget' => '',

		), $atts));
	
		$str .= '<div class="brand-box"><a href="' .$link . '" target="' . $linktarget . '">';
		$str .= do_shortcode($content);
		$str .= '</a></div>';
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Brand Box Title shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('brand_box_title', 'shortcode_brand_box_title');
	function shortcode_brand_box_title($atts, $content = null) {

		
		  extract(shortcode_atts(array(
 
		    'bgcolor' => '',

		    ), $atts));
		 
		    $bgcolor = ($bgcolor) ? ' '.$bgcolor : '';


		$str = '';
		$str .= '<div class="brand-box-title' . $bgcolor . '"><h2>';
		$str .= do_shortcode($content);
		$str .= '</h2></div><!--col-sm-3 cta-box-img-->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Brand Box Text shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('brand_box_text', 'shortcode_brand_box_text');
	function shortcode_brand_box_text($atts, $content = null) {

		  extract(shortcode_atts(array(
 
		    'bgcolor' => '',

		    ), $atts));
		 
		    $bgcolor = ($bgcolor) ? ' '.$bgcolor : '';

		$str = '';
		$str .= '<div class="brand-box-text' . $bgcolor . '"">';
		$str .= do_shortcode($content) . " &#187;";
		$str .= '</div><!--brand-box-text-->';
		
		return $str;
	}



//////////////////////////////////////////////////////////////////
// Brand Box Image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('brand_box_image', 'shortcode_brand_box_image');
	function shortcode_brand_box_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="brand-box-image">';
		$str .= do_shortcode($content);
		$str .= '</div><!--brand-box-text-->';
		
		return $str;
	}

	
//////////////////////////////////////////////////////////////////
// Call to Action box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('cta_box', 'shortcode_cta_box');
	function shortcode_cta_box($atts, $content = null) {
	
		extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}
		
		$str = '';
		$str .= '<section class="cta-box">';
		$str .= '<div class="row">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</section>';
		
			if($jumplink && $jumplinktext):
			$str .= '<div class="jumplink-wrapper clearfix"><a href="'.$jumplink .'" class="jumplink" target="' . $jumplinktarget . '">' . $jumplinktext . '</a></div>';
			endif;

		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Call to Action box image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('cta_box_image', 'shortcode_cta_box_image');
	function shortcode_cta_box_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-sm-3"><div class="cta-box-img">';
		$str .= do_shortcode($content);
		$str .= '</div></div><!--col-sm-3 cta-box-img-->';
		
		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Call to Action box content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('cta_box_content', 'shortcode_cta_box_content');
	function shortcode_cta_box_content($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-sm-9 cta-box-content"><div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-sm-9 cta-box-content-->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Collection box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('collection_box', 'shortcode_collection_box');
	function shortcode_collection_box($atts, $content = null) {
	
		extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}
		
		$str = '';
		$str .= '<section class="col-box">';
		$str .= '<div class="row">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</section>';
		
			if($jumplink && $jumplinktext):
			$str .= '<div class="jumplink-wrapper clearfix"><a href="'.$jumplink .'" class="jumplink" target="' . $jumplinktarget . '">' . $jumplinktext . '</a></div>';
			endif;

		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Collection Box Image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('collection_box_image', 'shortcode_collection_box_image');
	function shortcode_collection_box_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-lg-3 col-sm-12"><div class="col-box-img">';
		$str .= do_shortcode($content);
		$str .= '</div></div><!--col-sm-3 col-box-img-->';
		
		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Collection box content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('col_box_content', 'shortcode_col_box_content');
	function shortcode_col_box_content($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-sm-9 col-box-content"><div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-sm-9 cta-box-content-->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Featured box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_main', 'shortcode_feature_box_main');
	function shortcode_feature_box_main($atts, $content = null) {
	
		$str = '';
		$str .= '<section class="feature-box-main">';
		$str .= '<div class="row">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</section>';

		return $str;
	}

//////////////////////////////////////////////////////////////////
// Featured Collection Main box content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_content_main', 'shortcode_feature_box_content_main');
	function shortcode_feature_box_content_main($atts, $content = null) {

			extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}

		$str = '';
		$str .= '<div class="col-lg-6 col-sm-7 feature-box-content"><div class="inner-content">';
		$str .= do_shortcode($content);
		if($jumplink && $jumplinktext):
			$str .= '<a href="'.$jumplink .'" class="jumplink-da" target="' . $jumplinktarget . '">' . $jumplinktext . '</a>';
			endif;
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-sm-7 feature-box-content-->';
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Featured box image text shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_img_text', 'shortcode_feature_box_img_text');
	function shortcode_feature_box_img_text($atts, $content = null) { 
		 extract(shortcode_atts(array(
 
		    'bgcolor' => '',

		    ), $atts));
		 
		    $bgcolor = ($bgcolor) ? ' '.$bgcolor : ''; 

		$str = '';
		$str .= '<div class="feature-box-img-text' .$bgcolor . '">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Featured box image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_image_main', 'shortcode_feature_box_image_main');
	function shortcode_feature_box_image_main($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-lg-6 col-sm-5 fc-image-main">';
		$str .= do_shortcode($content);
		$str .= '</div><!--col-sm-5 feature-box-img-->';
		return $str;
	}




//////////////////////////////////////////////////////////////////
// Featured box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box', 'shortcode_feature_box');
	function shortcode_feature_box($atts, $content = null) {

		$str = '';
		$str .= '<section class="feature-box">';
		$str .= '<div class="row">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</section>';

		return $str;
	}

//////////////////////////////////////////////////////////////////
// Featured Collection box content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_content', 'shortcode_feature_box_content');
	function shortcode_feature_box_content($atts, $content = null) {

			extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}

		$str = '';
		$str .= '<div class="col-lg-6 col-sm-7 feature-box-content"><div class="inner-content">';
		$str .= do_shortcode($content);
		if($jumplink && $jumplinktext):
			$str .= '<a href="'.$jumplink .'" class="jumplink-da" target="' . $jumplinktarget . '">' . $jumplinktext . '</a>';
			endif;
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-sm-7 feature-box-content-->';
		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Featured box image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('feature_box_image', 'shortcode_feature_box_image');
	function shortcode_feature_box_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-lg-6 col-sm-5 fc-image">';
		$str .= do_shortcode($content);
		$str .= '</div><!--col-sm-5 feature-box-img-->';
		return $str;
	}




//////////////////////////////////////////////////////////////////
// Digital Archive box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('da_box', 'shortcode_da_box');
	function shortcode_da_box($atts, $content = null) {
	
		extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}
		
		$str = '';
		$str .= '<section class="da-box col-lg-12">';
		$str .= '<div class="row" >';
		$str .= do_shortcode($content);
		if($jumplink && $jumplinktext):
			$str .= '<div class="jumplink-wrapper clearfix"><a href="'.$jumplink .'" class="jumplink-da" target="' . $jumplinktarget . '">' . $jumplinktext . '</a></div>';
			endif;
		$str .= '</div>';
		$str .= '</section>';

		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Digital Archive Box Image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('da_box_image', 'shortcode_da_box_image');
	function shortcode_da_box_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-lg-4 col-sm-3 da-box-img">';
		$str .= do_shortcode($content);
		$str .= '</div><!--col-lg-4 da-box-img-->';
		
		return $str;
	}


//////////////////////////////////////////////////////////////////
// Digital Archive content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('da_box_content', 'shortcode_da_box_content');
	function shortcode_da_box_content($atts, $content = null) {
		$str = '';
		$str .= '<div class="col-lg-8 col-sm-9 da-content"><div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-lg-8 da-content-->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Stay Connected Box
//////////////////////////////////////////////////////////////////
add_shortcode('stay_connected', 'shortcode_stay_connected');
	function shortcode_stay_connected($atts, $content = null) {
		$str = '';
		$str .=	'<section class="stay-connected"><h2>Stay Connected &#187;</h2>';
		$str .= '<div class="col-lg-6 da-content">';
		$str .= '<div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div><!--inner-content-->';
		$str .= '</div><!--col-lg-6 da-content-->';
		$str .= '<div class="col-lg-6"><ul class="connect-icons">';
		$str .= '<li><a href="("url_facebook")"><img src="' . get_template_directory_uri() . '/img/social-icons/facebook.png"></a></li>';
		$str .= '<li><a href="("url_linkedin")"><img src="' . get_template_directory_uri() . '/img/social-icons/linked-in.png"></a></li>';
		$str .= '<li><a href="("url_youtube")"><img src="' . get_template_directory_uri() . '/img/social-icons/youtube.png"></a></li>';
		$str .= '<li><a href="("url_twitter")"><img src="' . get_template_directory_uri() . '/img/social-icons/twitter.png"></a></li>';
		$str .= '<li><a href="("url_rss")"><img src="' . get_template_directory_uri() . '/img/social-icons/rss.png"></a></li>';
		$str .= '<li><a href="("url_itunes")"><img src="' . get_template_directory_uri() . '/img/social-icons/itunes.png"></a></li>';
		$str .= '</ul></div><!--end social icons>';
		$str .= '</section>';
		
		return $str;
	}


//////////////////////////////////////////////////////////////////
// Multi-Box
//////////////////////////////////////////////////////////////////
add_shortcode('multi_box', 'shortcode_multi_box');
function shortcode_multi_box($atts, $content = null) {
	extract(shortcode_atts(array(
		'jumplink' => '#',
		'heading_location' => '',
		'heading_text' => '',
		
	
	), $atts));

	//required to split out multibox components into separate shortcodes
	global $multibox_parent_link;
	$multibox_parent_link = $jumplink;

	//Determine if the multibox has a top or bottom heading, if not, it gets the default top rule.
	if ($heading_location) {
		$has_heading = ' mb-has-heading';
	} else {
		$has_heading = '';
	}
	
    ob_start();?>
	<div class="multi-box-wrapper <?php echo $has_heading; ?>">
		<article class="multi-box">
		<?php if( !empty($heading_text) && ($heading_location == 'top') ) :
			if( !empty($jumplink) ) :
		?>
			<header>
				<h2><a href="<?php echo $jumplink; ?>"><?php echo $heading_text; ?></a></h2>
			</header>
		<?php	else: ?>
			<header class="no-link">
				<h2><?php echo $heading_text; ?></h2>
			</header>
			<?php endif; ?>
		<?php endif; ?>
			<div class="mb-content">
				<?php echo do_shortcode($content); ?>
			</div>

		<?php if( !empty($heading_text) && ($heading_location == 'bottom') ) :
			if( !empty($jumplink) ) :
		?>
			<footer>
				<h2><a href="<?php echo $jumplink; ?>"><?php echo $heading_text; ?></a></h2>
			</footer>
		<?php	else: ?>
			<footer class="no-link">
				<h2><?php echo $heading_text; ?></h2>
			</footer>
			<?php endif; ?>
		<?php endif; ?>
		</article>
	</div>
	<?php if(!empty($jumplink) && !empty($jumplinktext)) : ?>
	<div class="mb-jumplink-wrapper clearfix">
		<a class="jumplink" href="<?php echo $jumplink; ?>"><?php echo $jumplinktext; ?></a>
	</div>
	<?php endif; ?>
	<?php $html = ob_get_clean();
	return $html;
}

//////////////////////////////////////////////////////////////////
// Multi-Box Image
//////////////////////////////////////////////////////////////////
add_shortcode('multi_box_image', 'shortcode_multi_box_image');
function shortcode_multi_box_image($atts, $content = null) {
	global $multibox_parent_link;
	$str = '';
	$str = '<div class="mb-image"><a href="'. $multibox_parent_link .'">';
	$str .= do_shortcode($content);
	$str .= '</a></div>';
	
	return $str;
}

//////////////////////////////////////////////////////////////////
// Multi-Box content
//////////////////////////////////////////////////////////////////
add_shortcode('multi_box_content', 'shortcode_multi_box_content');
function shortcode_multi_box_content($atts, $content = null) {
	$str = '';
	$str = '<div class="mb-inner-content">';
	$str .= do_shortcode($content);
	$str .= '</div>';
	
	return $str;
}

//////////////////////////////////////////////////////////////////
// Content boxes shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('content_boxes', 'shortcode_content_boxes');
	function shortcode_content_boxes($atts, $content = null) {
		$str = '';
		$str .= '<section class="columns content-boxes">';
		$str .= do_shortcode($content);
		$str .= '</section>';

		return $str;
	}

//////////////////////////////////////////////////////////////////
// Content box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('content_box', 'shortcode_content_box');
	function shortcode_content_box($atts, $content = null) {
		extract(shortcode_atts(array(
			'heading_size'=>'3',
			'linktarget'=>'_self',
			'new_window'=>'',
			'heading'=>'',
			'link'=>'',
			'linktext'=>'',
			'linktype'=>'jumplink',
			'text'=>''
		),$atts));
		
		if($new_window == 'yes') {
			$new_window = ' target="_blank"';
		} 
		
		if($heading_size == '') {
			$heading_size='3';
		}
		
		$str = '';
		$str .= '<article class="content-box">';

		$str .= do_shortcode($content);
		
		$str .= '<div class="inner-content">';

		if(($heading) && ($link)):
		$str .= '<h'. $heading_size . ' class="heading"><a href="' . $link . '" target="' . $linktarget . '">' . $heading . '</a></h'.  $heading_size . '>';
		else:
		$str .= '<h'.$heading_size . ' class="heading">'.$heading . '</h' . $heading_size . '>';
		endif;
		
		if($text):
		$str .= '<p>' . $text . '</p>';
		endif;
		
		if($link && $linktext):
			if($linktype == 'button') {
				$linktype = 'btn';
			} else {
				$linktype = 'jumplink';
			}
			
		$str .= '<a href="' . $link . '" class="'. $linktype .'" target="' . $linktarget . '">' . $linktext . '</a>';
		endif;
		
		$str .= '</div>';
		
		$str .= '</article>';

		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Advanced Content box shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('advanced_content_box', 'shortcode_advanced_content_box');
	function shortcode_advanced_content_box($atts, $content = null) {
	
		extract(shortcode_atts(array(
			'img_url'=>'',
			'img_link'=>'',
			'img_alt_text'=>'',
			'jumplink'=>'',
			'jumplinktext'=>''
		),$atts));
		
		$str = '';
		$str .= '<article class="content-box">';
		if( $img_url ) {
		$str .= '<a href="' . $img_link . '">';
		$str .= '<img src="'. $img_url .'" alt="' . $img_alt_text . '">';
		$str .= '</a>';
		}		
		$str .= '<div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</article>';
		if(!empty($jumplink) && !empty($jumplinktext)) :
		$str .= '<div class="jumplink-wrapper after-box clearfix">';
		$str .= '<a class="jumplink" href="' . $jumplink . '">' . $jumplinktext . '</a></div>';
		endif;

		return $str;
	}

//////////////////////////////////////////////////////////////////
// Post Slider by Parameter
//////////////////////////////////////////////////////////////////
add_shortcode( 'post_slider', 'post_slider_shortcode' );
function post_slider_shortcode( $atts ) {
     // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'post',
		'category' => '',
        'order' => 'date',
        'orderby' => 'title',
		'taxonomy' => 'category',
        'posts' => -1,
		'jumplink'=>'',
		'jumplinktext'=>'',
		'jumplinktarget'=>''
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
		$taxonomy => $category
    );
	$query = new WP_Query( $options );
	if ( $query->have_posts() ) {
		$html = '';
		$html = '<div class="content-slider-wrapper">';
		$html .= '<ul class="content-slider list-unstlyled">';
			while ( $query->have_posts() ) : $query->the_post();
		$html .= '<li id="post-' .  get_the_ID() . '" ' . shortcode_post_class() .'>';
		$html .= '<div class="content-slide">';
			$image_id = get_post_thumbnail_id();
			$image_url = wp_get_attachment_image_src($image_id,'content-box', true);
		$html .= '<a href="' . get_permalink() . '"><img src="' . $image_url[0] . '" ' . 'class="cs-img img-responsive" alt="'. get_the_title() .'" /></a>';
		$html .= '<a href="' . get_permalink() . '" class="cs-title">' . get_the_title() . '</a>';
		$html .= '<p class="cs-subtitle">' . get_post_meta(get_the_ID(),'wpcf-partner-city',TRUE) . ', ' . get_post_meta(get_the_ID(),'wpcf-partner-state',TRUE) . '</p>';
		$html .= '<ul class="cs-links list-unstyled list-float">';
		$html .= '<li><a href="' . get_post_meta(get_the_ID(),'wpcf-partner-website-url',TRUE) . '">Visit Website</a></li>';
		//$html .= '<li><a href="#">Traveling Exhibits</a></li>';
		$html .= '</ul>';
		$html .= '</div>';
		$html .= '</li>';
			endwhile;
			wp_reset_postdata();
		$html .= '</ul>';
		$html .= '</div><!--.content-slider-wrapper-->';
			if($jumplink && $jumplinktext):
		$html .= '<div class="jumplink-wrapper clearfix"><a href="' . $jumplink . '" class="jumplink" target="' . $jumplinktarget . '">' . $jumplinktext . '</a></div>';
			endif;
			
	return $html;
	}	
}

//////////////////////////////////////////////////////////////////
// Directory Post List
//////////////////////////////////////////////////////////////////
add_shortcode( 'directory_list', 'directory_list_shortcode' );
function directory_list_shortcode( $atts ) {
     // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'post',
		'category' => '',
        'order' => 'ASC',
        'orderby' => 'title',
		'taxonomy' => 'category',
        'posts' => -1,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
		$taxonomy => $category
    );
	$html = '';
	$query = new WP_Query( $options );
	if ( $query->have_posts() ) {
		$html .= '<ul class="partner-directory-list list-unstyled">';
			while ( $query->have_posts() ) : $query->the_post();
			$html .= '<li id="post-' . get_the_ID() . '"' . shortcode_post_class() . '>';
			$html .= '<p>' . get_the_title() . '</p>';
				$url = get_field('wpcf-partner-website-url');
				$url = preg_replace('#^https?://#', '', $url);
			$html .= '<a href="' . get_field('wpcf-partner-website-url') . '">' . $url . '</a>';
			$html .= '</li>';
			endwhile;
			wp_reset_postdata();
		$html .= '</ul>';
	return $html;
	}	
}

//////////////////////////////////////////////////////////////////
// Title
//////////////////////////////////////////////////////////////////
add_shortcode('title', 'shortcode_title');
function shortcode_title($atts, $content = null) {
	$html = '';
	$html .= '<div class="title">'.do_shortcode($content).'</div>';
	return $html;
}

//////////////////////////////////////////////////////////////////
// Event Type
//////////////////////////////////////////////////////////////////
add_shortcode('event_type', 'shortcode_event_type');
function shortcode_event_type($atts, $content = null) {
	$html = '';
	$html .= '<span class="event-type">'.do_shortcode($content).'</span>';
	return $html;
}


//////////////////////////////////////////////////////////////////
// Featured Header
//////////////////////////////////////////////////////////////////
add_shortcode('featured_header', 'shortcode_featured_header');
function shortcode_featured_header($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
			'jumplinktext' => ''
		), $atts));
	
		if ( empty($jumplinktarget) || !isset($jumplinktarget) ) {
			$jumplinktarget = '_self';
		}

	$str .= '<div class="featured-header clearfix"><div class="col-lg-8 col-md-8 col-sm-8"><h2>';
	$str .= do_shortcode($content);
	$str .= '</h2></div>';
		if($jumplink && $jumplinktext):
			$str .= '<div class="col-lg-4 col-md-4 pull-right"><a href="'.$jumplink .'" class="jumplink" target="' . $jumplinktarget . '">' . $jumplinktext . '</a></div></div>';
			endif;

		return $str;
	}

//////////////////////////////////////////////////////////////////
// Top Head
//////////////////////////////////////////////////////////////////

add_shortcode('top_head', 'shortcode_top_head');
	function shortcode_top_head($atts, $content = null) {
		$str = '';
		$str .= '<span class="top-head">';
		$str .= do_shortcode($content);
		$str .= '</span><!--col-sm-3 col-box-img-->';
		
		return $str;
	}


//////////////////////////////////////////////////////////////////
// Holdings shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('holdings_item', 'shortcode_holdings_item');
	function shortcode_holdings_item($atts, $content = null) {

		$str = '';
		$str .= '<div class="col-lg-4 holdings-item">';
		$str .= do_shortcode($content);
		$str .= '</div><!---->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Holdings Text
//////////////////////////////////////////////////////////////////
add_shortcode('holdings_text', 'shortcode_holdings_text');
	function shortcode_holdings_text($atts, $content = null) {

		$str = '';
		$str .= '<div class="holdings-text">';
		$str .= do_shortcode($content);
		$str .= '</div>';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Holdings Title
//////////////////////////////////////////////////////////////////
add_shortcode('holdings_title', 'shortcode_holdings_title');
	function shortcode_holdings_title($atts, $content = null) {

		extract(shortcode_atts(
		array(
			'link' => '#',
			'linktarget' => '',

		), $atts));

		$str = '';
		$str .= '<h4 class ="holdings-title"><a href="' .$link . '" target="' . $linktarget . '">';
		$str .= do_shortcode($content);
		$str .= '</a></h4>';
		
		return $str;
	}


//////////////////////////////////////////////////////////////////
// Holdings Image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('holdings_icon', 'shortcode_holdings_icon');
	function shortcode_holdings_icon($atts, $content = null) {
		$str = '';
		$str .= '<div class="holdings-icon">';
		$str .= do_shortcode($content);
		$str .= '</div><!--col-lg-4-->';
		
		return $str;
	}



//////////////////////////////////////////////////////////////////
// Homepage Calendar
//////////////////////////////////////////////////////////////////
add_shortcode('event_calendar', 'gcmf_shortcode_event_calendar'); 
function gcmf_shortcode_event_calendar( $atts, $content = null) {
	
		extract(shortcode_atts(
		array(
			'jumplink' => '#',
			'jumplinktarget' => '',
		), $atts));
	
		$str .= '<section class="calendar-item col-lg-6 col-md-6 col-sm-6"><a href="' .$jumplink . '" target="' . $jumplinktarget . '">';
		$str .= '<div class="row" >';
		$str .= do_shortcode($content);
		$str .= '</div>';
		$str .= '</a></section>';
		return $str;
	}



//////////////////////////////////////////////////////////////////
// Calendar Date
//////////////////////////////////////////////////////////////////


add_shortcode('event_date', 'shortcode_event_date');
	function shortcode_event_date($atts, $content = null) {
		$str = '';
		$str .= '<h4 class="event-date">';
		$str .= do_shortcode($content);
		$str .= '</h4><!--col-sm-3 col-box-img-->';
		
		return $str;
	}

//////////////////////////////////////////////////////////////////
// Calendar Image shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('event_image', 'shortcode_event_image');
	function shortcode_event_image($atts, $content = null) {
		$str = '';
		$str .= '<div class="calendar-img">';
		$str .= do_shortcode($content);
		$str .= '</div><!--col-sm-3 col-box-img-->';
		
		return $str;
	}
	
//////////////////////////////////////////////////////////////////
// Calendar Content shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('event_content', 'shortcode_event_content');
	function shortcode_event_content($atts, $content = null) {
		$str = '';
		$str .= '<div class="inner-content">';
		$str .= do_shortcode($content);
		$str .= '</div><!--inner-content-->';
		
		return $str;
	}
		
//////////////////////////////////////////////////////////////////
// Description
//////////////////////////////////////////////////////////////////
add_shortcode('description', 'shortcode_description');
function shortcode_description($atts, $content = null) {
	$html = '';
	$html .= '<div class="description"><p>'.do_shortcode($content).'</p></div>';
	return $html;
}

//////////////////////////////////////////////////////////////////
// Spacer
//////////////////////////////////////////////////////////////////
add_shortcode('spacer', 'shortcode_spacer');
function shortcode_spacer($atts, $content = null) {
	extract(shortcode_atts(array(
		'divider' => 'no',
		'top' => '0',
		'bottom' => '0'
	), $atts));

	$divider = ($divider == 'yes') ? ' show-divider' : '' ;
	$html = '';
	$html .= '<div class="spacer' . $divider . '" style="margin-top: ' . $top . 'px; margin-bottom: ' . $bottom . 'px;"></div>';
	return $html;
}

//////////////////////////////////////////////////////////////////
// Related Image
//////////////////////////////////////////////////////////////////
add_shortcode('related_image', 'shortcode_related_image');
function shortcode_related_image($atts, $content = null) {
	extract(shortcode_atts(array(
		'id'=>''
	),$atts));
	$img_thumb = wp_get_attachment_image_src( $id, 'thumbnail' );
	$img = wp_get_attachment_image_src( $id, 'full' );
	$img_meta = wp_get_attachment( $id );
	ob_start();
	?>
	<div class="row related-image">
		<div class="col-md-5 col-xs-3">
		<a href='<?php echo esc_url($img[0]); ?>' title='<?php echo esc_attr($img_meta['caption']); ?>'>
			<img src='<?php echo esc_url($img_thumb[0]); ?>' class='img-responsive' alt='<?php echo esc_attr($img_meta['alt']); ?>' />
		</a>
		</div>
			<div class="col-md-7 col-xs-9">
			<p class="related-caption"><?php echo $img_meta['caption']; ?></p>
		</div>
	</div><!-- .row -->
	<?php
	$html = ob_get_clean();
	return $html;
}

//////////////////////////////////////////////////////////////////
// Columns shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('columns', 'shortcode_columns');
	function shortcode_columns( $atts, $content = null ) {  
		
		return '<div class="row column-wrapper">' .do_shortcode($content). '</div>';  
		
}


//////////////////////////////////////////////////////////////////
// List Posts by Parameter
//////////////////////////////////////////////////////////////////
add_shortcode( 'list_posts', 'list_posts_shortcode' );
function list_posts_shortcode( $atts ) {
    ob_start();
     // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'post',
		'category' => '',
        'order' => 'date',
        'orderby' => 'title',
		'taxonomy' => 'category',
        'posts' => 10,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
		$taxonomy => $category
    );
	$query = new WP_Query( $options );
	if ( $query->have_posts() ) { ?>
		<ul class="list-unstlyled">
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div>
					<?php $image_id = get_post_thumbnail_id(); ?>
					<?php $image_url = wp_get_attachment_image_src($image_id,'full', true);?>
					<img src="<?php echo $image_url[0]; ?>" class="img-responsive" />
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</div>
			</li>
			<?php endwhile;
			wp_reset_postdata(); ?>
		</ul>
	<?php $html = ob_get_clean();
	return $html;
	}	
}



//////////////////////////////////////////////////////////////////
// Featured Posts Slider
//////////////////////////////////////////////////////////////////
add_shortcode('featured_posts_slider', 'shortcode_featured_posts_slider');
function shortcode_featured_posts_slider($atts, $content = null) {

	//Get the ID of the current blog
	$current_blog_id = get_current_blog_id();
	
	// Define shortcode attributes and their defaults
    extract( shortcode_atts( array (
        'post_type' => 'post',
		'category' => '',
        'order' => 'ASC',
        'orderby' => 'date',
        'posts' => -1,
		'skin_color'=>'',
		'title_placement' => '',
		'title_text' => '',
		'jumplink' => '',
		'jumplinktext' => ''
    ), $atts ) );
		
	//Use queried post type to determine ID of required blog
	$queried_blog=$post_type;
	switch ($queried_blog) {
		case 'featured-products':
			$queried_blog_id=6;
			break;
		case 'exhibitions':
			$queried_blog_id=7;
			break;
		case 'art':
			$queried_blog_id=9;
			break;
		case 'special-programs':
			$queried_blog_id=10;
			break;
		case 'events':
			$queried_blog_id=11;
			break;
	}
	
	if( !($skin_color == '') ) {
		$skin = $skin_color;
		switch($skin) {
			case 'blue':
			$skin = ' skin-blue';
			break;
			case 'green':
			$skin = ' skin-green';
			break;
			case 'seafoam':
			$skin = ' skin-seafoam';
			break;
			case 'slate':
			$skin = ' skin-slate';
			break;
			case 'red':
			$skin = ' skin-red';
			break;
			case 'dark-blue':
			$skin = ' skin-dark-blue';
			break;
			case 'sky-blue':
			$skin = ' skin-sky-blue';
			break;
			case 'orange':
			$skin = ' skin-orange';
			break;
			case 'purple':
			$skin = ' skin-purple';
			break;
			case 'pink':
			$skin = ' skin-pink';
			break;
		}
	}
	
	//If ID of current blog is not the same as ID of queried blog, switch to queried blog to run loop
	if($current_blog_id !==	$queried_blog_id) {
		switch_to_blog($queried_blog_id);
	}
	
	//Get ID of Featured category from queried blog and get ID of required category
	$featured_obj = get_category_by_slug('featured');
	$featured_id = $featured_obj->term_id;
	if( !($category == '') ) {
		$cat_obj = get_category_by_slug($category);
		$cat_id = $cat_obj->term_id;
	}
	//Determine image size variant based on category
	$cat = $category;
	$pt = $post_type;
	
	if( isset($featured_id) && isset($cat_id) ) {
		switch($cat) {
			case 'at-the-museum':
			$image_variant = 'half-slider';
			break;
			case 'upcoming-exhibitions':
			$image_variant = 'quarter-slider';
			break;
			case 'traveling-exhibitions':
			$image_variant = 'quarter-slider';
			break;
			case 'special-events':
			$image_variant = 'half-slider';
			break;
			case 'wine-and-dine':
			$image_variant = 'quarter-slider';
			break;
			case 'gcmf-after-dark':
			$image_variant = 'quarter-slider';
			break;
		}

	}	else {
	
		switch($pt) {
			case 'featured-products':
			$image_variant = 'half-slider';
			break;
			case 'events':
			$image_variant = 'quarter-slider';
			break;
			case 'art':
			$image_variant = 'quarter-slider';
			break;
		}
	}
	
	// define query parameters for featured posts based on supplied shortcode attributes
	if( isset($featured_id) && isset($cat_id) ) {
		$options = array(
			'post_type' => $post_type,
			'order' => $order,
			'orderby' => $orderby,
			'posts_per_page' => $posts,
			'category_name' => $category,
			'category__and' => array( $featured_id, $cat_id )
		);
	} else {
		$options = array(
			'post_type' => $post_type,
			'order' => $order,
			'orderby' => $orderby,
			'posts_per_page' => $posts,
			'category_name' => 'featured'
		);
	}
		ob_start();?>
		<div class="multi-box-wrapper mb-has-heading <?php echo $skin ?>">
		<?php if($title_placement == 'top') :?>
				<header class="no-link">
					<h2><?php echo $title_text; ?></h2>
				</header>
		<?php endif; ?>
		<article <?php post_class('multi-box featured-slider gcmf-slider-skin'); ?>>
			
			<?php //Start the loop
				global $post;
				$query = new WP_Query( $options );
				if ( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
								
				//Set up post variables
				
				if(!(get_post_meta($post->ID,'wpcf-gallery-location',TRUE) == '')) {
					$location = get_post_meta($post->ID,'wpcf-gallery-location',TRUE);
					$loc = implode(', ', $location);
				} else {
					$loc = '';
				}
				
				if(!(get_post_meta($post->ID,'wpcf-collection',TRUE) == '')) {
					$collection = get_post_meta($post->ID,'wpcf-collection',TRUE);
					$col = implode(', ', $collection);
				} else {
					$col = '';
				}
					
				//See if artist-short custom field has a value. If not, set $artist to artist custom field
				if( get_post_meta($post->ID,'wpcf-artist-short',TRUE ) == '' ) {
					$artist = get_post_meta($post->ID,'wpcf-artist',TRUE);
				} else {
					$artist = get_post_meta($post->ID,'wpcf-artist-short',TRUE);
				}

				?>
			<div class="slide-content-wrapper">
			<div class="mb-image">
				<?php if ( has_post_thumbnail()) : ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
					
					<?php 
					//If one of the custom crop custom fields has a value
					if(!(get_post_meta($post->ID,'wpcf-featured-slider-lg-img',TRUE) == '') || !(get_post_meta($post->ID,'wpcf-featured-slider-sm-img',TRUE) == '')) {
					//Check to see which image variant it wants
						$src = ($image_variant == 'half-slider') ? get_post_meta($post->ID,'wpcf-featured-slider-lg-img',TRUE) : get_post_meta($post->ID,'wpcf-featured-slider-sm-img',TRUE);
					//output appropriate image crop
						echo '<img src="' . $src . '" class="img-responsive">';
						
					} else {
					
					the_post_thumbnail( $image_variant, array('class' => 'img-responsive')); 
					
					}?>
					<span class="countdown-overlay"></span>
					</a>
				<?php endif; ?>
			</div>
			<div class="mb-content-wrapper-outer">
				<div class="mb-content-wrapper-inner is-table">
					<div class="table-cell">
						<div class="mb-content inner-content">
							<?php $title = ($post_type == 'art') ? $artist : get_the_title(); ?>
							<p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo $title;?></a></p>
							<?php $secondary = ($post_type == 'art') ? $col : $loc; ?>
							<p class="details below"><?php echo $secondary;?></p>
						</div>
					</div>
				</div><!-- .mb-content-wrapper-inner -->
			</div><!-- .mb-content-wrapper-outer -->
			</div><!-- .slide-content-wrapper -->
		<?php endwhile;
		wp_reset_postdata();
		endif;
		restore_current_blog();
			if($title_placement == 'bottom') :?>
			<footer class="no-link">
				<h2><?php echo $title_text; ?></h2>
			</footer>
			<?php endif; ?>
		</article>
		<?php if(!empty($jumplink) && !empty($jumplinktext)) : ?>
			<div class="jumplink-wrapper clearfix">
				<a class="jumplink" href="http://"><?php echo $jumplinktext; ?></a>
			</div>
		<?php endif; ?>
		</div> <?php
	$html = ob_get_clean();
	return $html;
}



//////////////////////////////////////////////////////////////////
// Section Slider
//////////////////////////////////////////////////////////////////
add_shortcode('section_slider', 'shortcode_section_slider');
function shortcode_section_slider($atts, $content = null) {
	extract(shortcode_atts(array(
		'id'=>''
	),$atts));
	$html = '';
	$html .= '<div class="row">';
	$html .= '<div class="section-slider clearfix">';
	$html .= '<div class="ss-content">' . do_shortcode($content) . '</div>';
	$html .= '<div class="col-lg-12 col-md-12 ss-image">';
	$html .= '<div class="no-gutter">' . get_new_royalslider($id) . '</div>';
	$html .= '</div>';
	$html .= '</div>';
	$html .= '</div>';
	return $html;
}

//////////////////////////////////////////////////////////////////
// Related News
//////////////////////////////////////////////////////////////////
add_shortcode('related_news', 'gcmf_shortcode_related_news'); 
function gcmf_shortcode_related_news( $atts, $content = null) {
 	extract( shortcode_atts( array (
     'posts' => -1,
    ), $atts ) );

	switch_to_blog(3); //switch to pressroom to get articles
	// Get Related Posts
	$cat = get_category_by_slug($atts['slug']);
	$args = array(
	'post_type' => 'news',
	'category' => $cat->term_id,
	'posts_per_page' => $posts
	);
	
	$related_news  = get_posts( $args );
	$html = '';
	global $post;
	$html .= '<ul class="list-unstyled entry-list">';
	foreach ($related_news as $post) : setup_postdata( $post ); {
		//print_r($post);
		$html .= '<li class="entry-list-item">';
		$html .= '<div class="row">';
		$html .= '<div class="col-xs-2 entry-list-thumb"><a href="' . get_permalink() . '">';
		$html .= ( '' != get_the_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail') : '<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/thumbnail-default.png" alt="" />';
		$html .= '</a></div>';
		$html .= '<div class="col-xs-10 entry-list-content"><p class="entry-date">'. get_the_date() . '</p><p class="entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
		$html .= '<p class="excerpt">' . the_excerpt_max_charlength(120) . ' <a href="'. get_permalink() .'" class="jumplink">Read more</a></p>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</li>';
	}
	endforeach;
	$html .= '</ul>';
	wp_reset_postdata();
	restore_current_blog(); //switch back to main site
	return $html;
	
}



//////////////////////////////////////////////////////////////////
// Related Events
//////////////////////////////////////////////////////////////////
add_shortcode('related_events', 'gcmf_shortcode_related_events'); 
function gcmf_shortcode_related_events( $atts, $content = null) {
 	extract( shortcode_atts( array (
     'posts' => -1,
    ), $atts ) );

	switch_to_blog(3); //switch to pressroom to get articles
	// Get Related Posts
	$cat = get_category_by_slug($atts['slug']);
	$args = array(
	'post_type' => 'news',
	'category' => $cat->term_id,
	'posts_per_page' => $posts
	);
	
	$related_events  = get_posts( $args );
	$html = '';
	global $post;
	$html .= '<ul class="list-unstyled entry-list">';
	foreach ($related_events as $post) : setup_postdata( $post ); {
		//print_r($post);
		$html .= '<li class="entry-list-item">';
		$html .= '<div class="row">';
		$html .= '<div class="col-xs-2 entry-list-thumb"><a href="' . get_permalink() . '">';
		$html .= ( '' != get_the_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail') : '<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/thumbnail-default.png" alt="" />';
		$html .= '</a></div>';
		$html .= '<div class="col-xs-10 entry-list-content"><p class="entry-date">'. get_the_date() . '</p><p class="entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
		$html .= '<p class="excerpt">' . the_excerpt_max_charlength(120) . ' <a href="'. get_permalink() .'" class="jumplink">Read more</a></p>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</li>';
	}
	endforeach;
	$html .= '</ul>';
	wp_reset_postdata();
	restore_current_blog(); //switch back to main site
	return $html;
	
}


//////////////////////////////////////////////////////////////////
// Related Events Chronological
//////////////////////////////////////////////////////////////////
add_shortcode('related_events_chrono', 'gcmf_shortcode_related_events_chrono'); 
function gcmf_shortcode_related_events_chrono( $atts, $content = null) {
 	extract( shortcode_atts( array (
     'posts' => -1,
    ), $atts ) );

	switch_to_blog(3); //switch to pressroom to get articles
	// Get Related Posts
	$cat = get_category_by_slug($atts['slug']);
	$args = array(
	'post_type' => 'events',
	'category' => $cat->term_id,
	'posts_per_page' => $posts,
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'meta_key' => 'wpcf-start-date'
	);
	
	$related_events  = get_posts( $args );
	$html = '';
	global $post;
	$html .= '<ul class="list-unstyled entry-list">';
	
	
	

	foreach ($related_events as $post) : setup_postdata( $post ); {
		

		$startdate = get_post_meta(get_the_ID(),'wpcf-start-date',TRUE);
		$startdateformat = date_i18n('Ymd', $startdate);
		$enddate = get_post_meta(get_the_ID(),'wpcf-end-date',TRUE);
		$enddateformat = date_i18n('Ymd', $startdate);
		$location = get_post_meta(get_the_ID(),'wpcf-event-location',TRUE);
		
		date_default_timezone_set('America/New_York');
		$currentdate = date('Ymd');
		
		if ($startdateformat === $enddateformat) {
			$printdate = date_i18n('F j, Y', $startdate);
			$comparedate = date_i18n('Ymd', $startdate);
		}
		else if($enddateformat == null){
			$printdate = date_i18n('F j, Y', $startdate);
			$comparedate = date_i18n('Ymd', $startdate);
		}
		else{
			$printdate = date_i18n('F j', $startdate) . " - " . date_i18n('F j, Y', $enddate);
			$comparedate = date_i18n('Ymd', $enddate);
		}

		if ($currentdate <= $comparedate){
			//print_r($post);
			$html .= '<li class="entry-list-item">';
			$html .= '<div class="row">';
			$html .= '<div class="col-xs-2 entry-list-thumb"><a href="' . get_permalink() . '">';
			$html .= ( '' != get_the_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail') : '<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/thumbnail-default.png" alt="" />';
			$html .= '</a></div>';
			$html .= '<div class="col-xs-10 entry-list-content"><p class="entry-date">' . $printdate .'</p><p class="entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
			$html .= '<p style="margin-bottom: 5px;">' . $location . '</p>';
			$html .= '<p class="excerpt">' . the_excerpt_max_charlength(120) . ' <a href="'. get_permalink() .'" class="jumplink">Read more</a></p>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</li>';
		}
	}
	endforeach;
	$html .= '</ul>';
	wp_reset_postdata();
	restore_current_blog(); //switch back to main site
	return $html;
	
}


//////////////////////////////////////////////////////////////////
// Hero slider content block
//////////////////////////////////////////////////////////////////
add_shortcode('hero_slider_content_box', 'shortcode_hero_slider_content_box');
function shortcode_hero_slider_content_box($atts, $content = null) {

// Define shortcode attributes and their defaults
    extract(shortcode_atts( array (
        'text_align' => 'left',
		'block_align' => 'left',
		'move_up' => '0',
        'move_right' => '0',
        'move_left' => '0',
		'text_color' => '#fff',
		'background' => 'no',
		'background_color' => '#00aee7'
    ), $atts ));
	
	//If background is requested, add padding and set shade and opacity
	if ( $block_align == 'right' ) {
			$block_align = 'right:0px; ';	
		} else {		
			$block_align = '';
		}
	
	//If background is requested, add padding and set shade and opacity
	if ( $background == 'yes' ) {
		$padding = 'padding:30px; ';
		
		$bg = 'color:'. $text_color .'; background-color:'. $background_color .';';
		
	} else {
		$padding = '';
		$bg = '';
	}
	
$str = '';
$str .= '<div class="container">';
$str .= '<div class="slider-block" style="' . $bg . ' ' . $padding . $block_align . ' text-align:'. $text_align .'; margin-left:'. $move_right .'px; margin-right:'. $move_left .'px; margin-bottom:'. $move_up .'px;">';
$str .= do_shortcode($content);
$str .= '</div></div>';

return $str;
}

//////////////////////////////////////////////////////////////////
// Google map shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('googlemap', 'googlemap_shortcode');
function googlemap_shortcode($atts, $content = null) {
   extract(shortcode_atts(array(
               "width" => '940',
               "height" => '300',
               "src" => ''
   ), $atts));
 
return '<div>
         <iframe src="'.$src.'&output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="'.$width.'" height="'.$height.'"></iframe>
        </div>';
}

//////////////////////////////////////////////////////////////////
// iframe shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('iframe', 'iframe_shortcode');
function iframe_shortcode($atts, $content = null) {
   extract(shortcode_atts(array(
               'width' => '',
               'height' => '',
               'src' => '',
			   'name' => ''
   ), $atts));
 
return '<iframe src="'.$src.'" name="'.$name.'" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="'.$width.'" height="'.$height.'"></iframe>';
}

//////////////////////////////////////////////////////////////////
// Mobile Home Page Section Button
//////////////////////////////////////////////////////////////////
add_shortcode('mobile_section_button', 'mobile_section_button_shortcode');
function mobile_section_button_shortcode($atts, $content = null) {
   extract(shortcode_atts(array(
               "color" => '',
			   "url" => '',
			   "text" => ''
   ), $atts));
 
$html = '';
$html .= '<h2 class="btn '. $color .'">';
$html .= '<div class="container">';
$html .= '<a href="'.esc_url($url).'">' . $text . '</a>';
$html .= '</div>';
$html .= '</h2>';
return $html;
}

//////////////////////////////////////////////////////////////////
// Custom Gallery shortcode
//////////////////////////////////////////////////////////////////
remove_shortcode('gallery', 'gallery_shortcode'); // removes the original shortcode
add_shortcode('gallery', 'custom_gallery_shortcode'); // add new shortcode
function custom_gallery_shortcode($attr) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) )
			$attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => 'div',
		'icontag'    => 'div',
		'captiontag' => 'p',
		'columns'    => 1,
		'size'       => 'thumbnail',
		'include'    => '',
		'link'       => 'none'
	), $attr, 'gallery'));

	$id = intval($id);

	if ( !empty($include) ) {
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$icontag = tag_escape($icontag);
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) )
		$itemtag = 'div';
	if ( ! isset( $valid_tags[ $captiontag ] ) )
		$captiontag = 'p';
	if ( ! isset( $valid_tags[ $icontag ] ) )
		$icontag = 'div';

	$selector = "gallery-{$instance}";

	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='row gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
			$image_output = wp_get_attachment_image( $id, $size, false, array('class' => 'img-responsive') );
			$image_url = wp_get_attachment_image_src( $id, 'full' );

		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) )
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';

		$output .= "<{$itemtag} class='gallery-item col-md-3'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
				<a href='{$image_url[0]}' title='". wptexturize($attachment->post_excerpt). " " .wptexturize($attachment->post_content) ."'>$image_output</a>
			</{$icontag}>";
		if ( $captiontag && ( trim($attachment->post_excerpt) || trim($attachment->post_content) ) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . " " . wptexturize($attachment->post_content) .  "
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
	}

	$output .= "
			<br style='clear: both;' />
		</div>\n";

	return $output;
}


//////////////////////////////////////////////////////////////////
// Faculty List Shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('faculty_list', 'shortcode_faculty_list');
function shortcode_faculty_list($atts, $content = null) {
    $terms = get_terms('faculty-member', array('hide_empty' => false) );
	$str = '';
	$str .= '<div class="row">';
	foreach ($terms as $term) {
		//Always check if it's an error before continuing. get_term_link() can be finicky sometimes
		$term_link = get_term_link( $term, 'faculty-member' );
		if( is_wp_error( $term_link ) )
        continue;
		$str .= '<div class="col-lg-6 profile-tile">';
		$str .= '<article class="content-box">';
		$str .= '<div class="container"><div class="row">';		
		$str .= '<div class="col-lg-6 col-md-3 col-sm-3 col-xs-4 pt-image no-gutter"><a href="' . $term_link . '">';
				
			$fac_image = get_field('faculty_image', 'faculty-member_' . $term->term_id  );
			if( !empty($fac_image) ):
				$str.='<img src="' . $fac_image['url'] . '" alt="' . $fac_image['alt'] . '" class="img-responsive" />';		 
			else:		
				$str.='<img src="' . get_stylesheet_directory_uri() . '/img/placeholders/headshot.png" class="img-responsive" />';		
			endif;
		
		$str .= '</a></div>';
		$str .= '<div class="col-lg-6 col-md-9 col-sm-9 col-xs-8 pt-content">';
        $str .= '<h3><a href="' . $term_link . '">' . $term->name . '</a></h3>';
		$str .= '<p>' . get_field('faculty_title', 'faculty-member_' . $term->term_id ) . '</p>';
		$args = array(
			'post_type' => 'programs',
			 'tax_query' => array(
				array(
					'taxonomy' => 'faculty-member',
					'field' => 'id',
					'terms' => $term->term_id
				)
			)
		);
			$results = get_posts( $args );
			$has_courses = ((!is_wp_error($results) && count($results)>0) ? ' Programs and' : '');
		$str .= '</div></div><!--.row .container-->';
		$str .= '<a href="' . $term_link . '" class="jumplink">'. $has_courses .' Full Profile</a>';
		$str .= '</article></div>';
    }
	$str .= '</div>';
	return $str;
}



//////////////////////////////////////////////////////////////////
// Add buttons to tinyMCE
//////////////////////////////////////////////////////////////////
add_action('init', 'add_button');
//add_action('init', 'add_button_2');

function add_button() {  
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )  
   {  
     add_filter('mce_external_plugins', 'add_plugin');  
     add_filter('mce_buttons_3', 'register_button');  
   }  
}  

function register_button($buttons) {
   array_push($buttons, "button", "columns", "one_third", "two_third", "one_fourth", "one_half", "three_fourth", "title", "cta_box", "content_box", "advanced_content_box", "hero_slider_content_box", "section_slider", "tabs", "alt_tabs", "spacer");  
   return $buttons;
}  

function add_plugin($plugin_array) {  
   $plugin_array['button'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['columns'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['one_third'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['two_third'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['one_fourth'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['one_half'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['three_fourth'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['title'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['cta_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['da_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['content_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['feature_box_main'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['feature_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['hero_slider_content_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['advanced_content_box'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['section_slider'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['tabs'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['alt_tabs'] = get_template_directory_uri().'/tinymce/customcodes.js';
   $plugin_array['spacer'] = get_template_directory_uri().'/tinymce/customcodes.js';

   return $plugin_array;  
}